<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'author_id',
            //'udk_index',
            //'pages',
            // 'created_at',
            // 'updated_at',
            'status',
            // 'payment_status',
            // 'tags',
            // 'digest_id',
            // 'rus_annotation:ntext',
            // 'eng_annotation:ntext',
            'coauthor',
            'science_id',
            'organization',
            // 'grnti',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>