<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
?>

<div class="container">
    <div class="site-page form-padding">
        <?php if (isset($page)) { ?>

                    <?php 

                        $this->title = $page->title;
                        $this->params['breadcrumbs'][] = $this->title;

                    ?>

                    <?= 

                        Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) 
                        
                    ?>

                <!--renderView-->
                <?php
                    if ($page->layout && file_exists($page->layout)) { ?>

                        <?= 
                            $this->renderFile($page->layout, [
                                'page' => $page
                            ]) 
                        ?>

                <?php } else { ?>

                        <?= Html::decode($page->page_body) ?>

                <?php } ?>

        <?php } ?>
    </div>	
</div>

