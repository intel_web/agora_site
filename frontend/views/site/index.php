<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;

$this->title = \Yii::$app->params['siteTtitle'];
?>

<section class="top-section">
    <div class="container"> 
        <div class="row">
             <div class="col-md-3">
                    <div class="col-md-12">
            <a class="btn btn-social-icon btn-vk">
             <span class="fa fa-vk"></span>
            </a>
            <a class="btn btn-social-icon btn-facebook">
             <span class="fa fa-facebook"></span>
            </a>
            <a class="btn btn-social-icon btn-twitter">
             <span class="fa fa-twitter"></span>
            </a>
            <a class="btn btn-social-icon btn-google">
             <span class="fa fa-google"></span>
            </a>
            
            </div>
            </div>
             <div class="col-md-6">
                    <div class="logo text-center">
                    <img src="/mediafiles/files/its.png" />
                 </div>
            </div>
            <div class="col-md-3">
                <div class="pull-right apply-block">

                    <?= Html::beginTag('a', ['href' => Url::to(['control-panel/author'], true)]) ?>

                        <button class="btn btn-default btn-lg apply-button well btn-block" type="button"  aria-expanded="false">Опубликовать статью в сборнике «IT&nbsp;&&nbsp;Транспорт»</button>

                    <?= Html::endTag('a') ?>

                    <?= Html::beginTag('a', ['href' => Url::to(['control-panel/author'], true)]) ?>

                        <button class="btn btn-default btn-lg apply-button well btn-block" type="button"  aria-expanded="false">Принять участие в конкурсе «IT&nbsp;&&nbsp;Транспорт»</button>

                    <?= Html::endTag('a') ?>

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                <?php if (isset($slide_news) && count($slide_news) !== 0) { ?>

                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">

                                    <?php foreach ($slide_news as $key=>$slide) { ?>

                                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $key; ?>" class="<?php if ($key == 0) echo 'active'; ?>"></li>

                                    <?php } ?>

                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">

                                    <?php foreach ($slide_news as $key => $slide) { ?>

                                        <div class="item  <?php if ($key == 0) echo 'active';  ?> ">
                                            <div class="main-article">
                                                <div class="article-header text-center">

                                                    <?= Html::a('<h1>'.Html::decode($slide->title).'</h1>', ['site/post', 'id' => $slide->id]) ?>

                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>

                                </div>
                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only"><<</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">>></span>
                                </a>
                            </div>

                <?php } ?>        
                            
            </div>
        </div>
    </div>
</section>
<section class="middle-section">
        <div class="container"> 
            <div class="row row-eq-height">
                <?php
                    if (isset($news)) {
                        foreach ($news as $post) { ?>

                            <div class="col-xs-6 col-md-4 thumbnail">

                                <?= Html::beginTag('a', ['href' => Url::to(['site/post', 'id' => $post->id], true)]) ?>

                                    <?= Html::img(Html::encode($post->thumbnail), ['alt' => 'Preview']) ?>

                                <?= Html::endTag('a') ?>

                              <div class="caption">

                                <?= Html::tag('time', Html::encode($post->publish_date)) ?>

                                <?= Html::tag('h3', Html::encode(strip_tags($post->title))) ?>

                                <?= Html::beginTag('article', ['class' => 'post-preview']) ?>

                                    <?= Html::tag('p', Html::decode($post->preview)) ?>

                                <?= Html::endTag('article') ?>

                                <?= Html::beginTag('p') ?>
                                    
                                    <?= Html::a('Подробнее...', ['site/post', 'id' => $post->id], ['class' => 'btn btn-default thumb-bottom-button', 'role' => 'button']) ?>

                                <?= Html::endTag('p') ?>

                              </div>
                    
                            </div>

                    <?php }
                    }
                ?>
            </div>
        </div>
</section>
<section class="contact-section" id="contacts">
    <?php
        $this->registerJs(
            '$("document").ready(function(){
                $("#contact-form").on("pjax:end", function() {
                    $.pjax.reload({container:"#feedback-result"});
                });
            $(".contact-toggle").click(function(){
                    $(".contact-toggled").slideToggle("linear");
                });
            });'
        );
    ?>

    <div class="container text-center"> 
        <div class="section-header contact-toggle">
            <span class="white-text">Связаться с нами</span>
        </div>
        <div class="contact-toggled">
        <div class="row">
            <div id="feedback-result" class="col-md-12">
            </div>
        </div>
        <div class="row">

            <?php Pjax::begin(['id' => 'feedBack']) ?>

                <?php $form = ActiveForm::begin(['action' => ['site/contact'], 'id' => 'contact-form', 'options' => ['data-pjax' => true]]); ?>

                        <div class="col-lg-4 col-sm-4" >

                            <?= $form->field($feedBack, 'name') ?>

                        </div>
                        <div class="col-lg-4 col-sm-4 " >

                            <?= $form->field($feedBack, 'email') ?>

                        </div>
                        <div class="col-lg-4 col-sm-4">

                            <?= $form->field($feedBack, 'subject') ?>

                        </div>
                        <div class="col-lg-12 col-sm-12" >
                        
                            <?= $form->field($feedBack, 'body')->textArea(['rows' => 6]) ?>

                        </div>
                        <div class="col-lg-12 col-sm-12" >
                        
                            <?= $form->field($feedBack, 'verifyCode')->widget(Captcha::className(), [
                                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                            ]) ?>

                        </div>
                        <div class="col-lg-12 col-sm-12">
                            
                            <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn btn-primary custom-button', 'name' => 'contact-button']) ?>

                        </div>


                <?php ActiveForm::end(); ?>

            <?php Pjax::end(); ?>

        </div>
    </div> </div>
</section>
<!-- <section class="info-section">
    <div class="container text-center"> 
        <div class="section-header">
            <h2 class="black-text">Контактная информация</h2></div>
      
        <div class="contacts-info-table text-left">
            <ul class="list-group">
              <li class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> ИнтелТранс</li>
              <li class="list-group-item"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Россия, г.Самара, ул. Мичурина, 126</li>
              <li class="list-group-item"><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> (846) 922-79-78</li>
              <li class="list-group-item"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> info@its-spc.ru</li>   
            </ul>  
        </div>
        <div class="row">
            <div class="col-md-12">
            <a class="btn btn-social-icon btn-vk">
             <span class="fa fa-vk"></span>
            </a>
            <a class="btn btn-social-icon btn-facebook">
             <span class="fa fa-facebook"></span>
            </a>
            <a class="btn btn-social-icon btn-twitter">
             <span class="fa fa-twitter"></span>
            </a>
            <a class="btn btn-social-icon btn-google">
             <span class="fa fa-google"></span>
            </a>
            <a class="btn btn-social-icon btn-bitbucket">
             <span class="fa fa-bitbucket"></span>
            </a>
            </div>
        </div>
    </div>
</section> -->