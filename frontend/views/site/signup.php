<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\assets\PhoneMaskAsset;

PhoneMaskAsset::register($this);
$this->registerJs(
   '$("document").ready(function(){ 
        $("#signupform-phone").mask("+7(999)999-9999", { completed: function () { $(".hidden").slideDown("slow"); } });
    });'
);

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-signup form-padding">
        <div class="panel panel-default">
            <div class="panel-heading text-center">
                <h1 style="padding:0"><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body">
                            
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend>Данные для входа</legend>
                                
                                            <?= $form->field($model, 'username') ?>

                                            <?= $form->field($model, 'email') ?>

                                            <?= $form->field($model, 'password')->passwordInput() ?>

                                            <?= $form->field($model, 'passwordConfirm')->passwordInput() ?>

                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend>Профиль</legend>

                                            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($model, 'organization')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($model, 'office_position')->textInput(['maxlength' => true]) ?>

                                    </fieldset>
                                </div>
                            </div>
                            <div class="row text-right">
                                <div class="col-md-3"></div>
                                <div class="col-md-3"></div>
                                <div class="col-md-3"></div>
                                <div class="col-md-3">

                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className()) ?>

                                </div>
                            </div>

                            <div class="form-group text-right">

                                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-default', 'name' => 'signup-button']) ?>
                            
                            </div>

                        <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
