<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-contact form-padding">
        <div class="panel panel-default">
            <div class="panel-heading text-center">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Если у Вас есть вопрос или предложение, свяжитесь с нами заполнив форму:</p>
                        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                            
                            <?= $form->field($model, 'name') ?>

                            <?= $form->field($model, 'email') ?>

                            <?= $form->field($model, 'subject') ?>

                            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

                            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                            ]) ?>

                            <div class="form-group">
                                <?= Html::submitButton('Отправить', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>	
</div>

