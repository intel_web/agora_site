<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php echo $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'digest_id') ?>

    <?php // echo $form->field($model, 'rus_annotation') ?>

    <?php // echo $form->field($model, 'eng_annotation') ?>

    <?php echo $form->field($model, 'coauthor') ?>

    <?php // echo $form->field($model, 'science_id') ?>

    <?php echo $form->field($model, 'organization') ?>

    <?php // echo $form->field($model, 'grnti') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>