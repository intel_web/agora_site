<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;



//$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-contact form-padding">
        <div class="panel panel-default">
            <?php if (isset($current)) {  ?>
                <div class="panel-heading text-center">

                    <?= Html::tag('h3', Html::encode($current->title)) ?>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= Html::tag('article', Html::decode($current->content), ['class' => 'post-preview']) ?>

                        </div>
                    </div>
                </div>
            <?php } ?>   
        </div>
    </div>	
</div>

