<div class="row">
	<div class="col-md-4">
		<h2>Sidebar</h2>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras finibus a ante eu bibendum. Aenean auctor, quam ut eleifend sodales, odio mauris pretium leo, a tempus turpis nulla non erat. Pellentesque in efficitur quam. Aenean porttitor diam ut efficitur suscipit. Aenean porttitor eu ante at gravida. Donec semper pulvinar ante, vitae tincidunt augue pretium ut. Nulla placerat velit sit amet aliquam aliquam. Vivamus ut convallis neque. Mauris et massa tincidunt, iaculis augue et, fermentum enim. Nulla varius ante sed sapien pharetra, et commodo erat posuere. Integer ligula tellus, efficitur ac arcu eu, dignissim dignissim ligula. Curabitur nec lectus vehicula, eleifend magna porttitor, eleifend mauris. Integer id odio aliquam mi venenatis congue iaculis et lacus. Aliquam cursus congue purus, rhoncus tristique metus accumsan vehicula. Curabitur auctor pharetra hendrerit.
	</div>
	<div class="col-md-8">
		
		<h1><?= $page->title ?></h1>

		<?= $page->page_body ?>

	</div>
</div>