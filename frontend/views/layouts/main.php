<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\helpers\MenuHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name='yandex-verification' content='4dd6921a032492f9' />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">
    <header class="header" id="top">
        <?php
        NavBar::begin([
            'brandLabel' => 'IT & Транспорт',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-default navbar-fixed-top',
            ],
        ]);
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-home"></span> Главная', 'url' => ['/site/index']];
        $menuItems = array_merge($menuItems, MenuHelper::getMenuItems());

        if (Yii::$app->user->isGuest) {
            $menuLoginItems[] = ['label' => '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Регистрация', 'url' => ['/site/signup']];
            $menuLoginItems[] = ['label' => '<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Вход', 'url' => ['/site/login']];
        } else {
            $menuLoginItems[] = ['label' => '<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Личный кабинет', 'url' => ['/control-panel']];
            $menuLoginItems[] = [
                'label' => '<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Выйти',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post'],
            ];
        }
        echo Nav::widget([
            'options' => ['class' => 'nav navbar-nav'],
            'items' => $menuItems,
            'encodeLabels' => false,
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuLoginItems,
            'encodeLabels' => false,
        ]);
        NavBar::end();
        ?>
    </header>

    <!--<div class="container">-->
        <?php /* Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])*/ ?>
        <?php //Alert::widget() ?>
        <?= $content ?>
    <!--</div>-->
    <footer>
        <div class="container text-center white-text">  
            <div class="row">
                <div class="col-lg-12 credentials" >
                    <span><a href="http://its-spc.ru"  target="_blank">ИнтелТранс</a>, <?php echo date("Y") ?></span>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>

    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38347940 = new Ya.Metrika({
                    id:38347940,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38347940" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
<?php $this->endPage() ?>
