<?php
namespace frontend\controllers;

use Yii;

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

use common\models\LoginForm;
use common\models\Post;
use common\models\Page;
use common\models\Author;
use common\models\User;
use common\models\ArticleSearch;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Contact;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'frontend\components\MathCaptchaAction',
                'minLength' => 0,
                'maxLength' => 10,
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $feedBack = new ContactForm();
        $news = Post::find()
            ->where('status = 1 AND publish_date <= CURDATE()')
            ->limit(3)
            ->orderBy(['publish_date' => SORT_DESC])
            ->all();
        $slideNews = Post::find()
            ->where('is_presentation = 1 AND status = 1 AND publish_date <= CURDATE()')
            ->orderBy(['publish_date' => SORT_DESC])
            ->all();
		return $this->render('index', [
            'news' => $news,
            'slide_news' => $slideNews,
            'feedBack' => $feedBack,
        ]);
    }
	/**
    * Renders single post
    *
    * @return mixed
    */
    public function actionPost($id){   
        $post = Post::findOne($id);

        return $this->render('post', [
            'current' => $post
        ]);
    }
    /**
    * Renders single page
    *
    * @return mixed
    */
    public function actionPage($id) {
        $page = Page::find()
            ->where('id = :id AND status = 1', [':id' => $id])
            ->one();
        if ($page !== null) {
            return $this->render('page', [
                'page' => $page
            ]);
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //redirect to user module
            foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID) as $user_role) {
                switch ($user_role->name) {
                    case 'administrator': {
                        $this->redirect(Url::home().'control-panel/administrator');
                    }break;
                    case 'author': {
                        $this->redirect(Url::home().'control-panel/author');
                    }break;
                    case 'editor': {
                        $this->redirect(Url::home().'control-panel/editor');
                    }break;
                    /*default: {
                        return $this->goBack();
                    }break;*/
                }
            }
            //return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate() && Yii::$app->request->isPjax) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                return $this->renderPartial('message', [
                    'type' => 'success',
                    'message' => 'Спасибо за обращение к нам. Мы ответим как можно быстрее.'
                ]);
            } else {
                return $this->renderPartial('message', [
                    'type' => 'danger',
                    'message' => 'Ошибка при отправке сообщения.'
                ]);
            }
            if (!Yii::$app->request->isPjax) {
                return $this->refresh();
            }
        } else {
            $this->redirect(Url::home());
        }

    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
    	$model = new SignupForm();
    	
        if ($model->load(Yii::$app->request->post())) {
    		if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    if(mkdir("./".Yii::$app->params['uploads'].Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/")){
                        $this->sendEmail($user, $model->password);
                        return $this->redirect(Url::home().'control-panel');
                    }
                }
            }
    	}
    	return $this->render('signup', [
            'model' => $model,
        ]);
    }

    protected function sendEmail($user, $password)
    {
        return \Yii::$app->mailer->compose(['html' => 'afterRegistrationHelloMail-html', 
            'text' => 'afterRegistrationHelloMail-text'], 
            ['user' => $user, 'password' => $password])
            ->setFrom([\Yii::$app->params['adminEmail'] => 'IT&Transport'])
            ->setTo($user->email)
            ->setSubject(Html::encode('Регистрация на IT&Транспорт'))
            ->send();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSearch()
    {
        $this->layout = 'search';
        
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('article', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }	
}
