<?php
 
namespace frontend\controllers;
 
use common\models\Post;
use common\models\Page;
use common\models\SiteMenu;
use yii\web\Controller;
use yii\db\Query;
use Yii;
 
class SitemapController extends Controller
{
 
    public function actionIndex()
    {
 
        if (!$xml_sitemap = Yii::$app->cache->get('sitemap')) {  // проверяем есть ли закэшированная версия sitemap
            $urls = array();
 
            // Выбираем страницы сайта
            $pages = Page::find()->where('status = 1')->all();
            foreach ($pages as $page) {
                $urls[] = array(
                    Yii::$app->urlManager->createUrl(['/site/page?id=/' . $page->id]) // создаем ссылки на выбранные категории
                , 'weekly'                                                           // вероятная частота изменения категории
                );
            }
 
            // Записи Блога
            $posts = Post::find()->where('status = 1')->all();
            foreach ($posts as $post) {
                $urls[] = array(
                    Yii::$app->urlManager->createUrl(['site/post?id=' . $post->id]) // строим ссылки на записи блога
                , 'weekly'
                );
            }
 
            $xml_sitemap = $this->renderPartial('index', [ // записываем view на переменную для последующего кэширования
                'host' => Yii::$app->request->hostInfo,         // текущий домен сайта
                'urls' => $urls,                                // с генерированные ссылки для sitemap
            ]);
 
            Yii::$app->cache->set('sitemap', $xml_sitemap, 3600*12); // кэшируем результат, чтобы не нагружать сервер и не выполнять код при каждом запросе карты сайта.
        }
 
        Yii::$app->response->format = \yii\web\Response::FORMAT_XML; // устанавливаем формат отдачи контента
 
        echo $xml_sitemap;
    }
}