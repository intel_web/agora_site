<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Добрый день, <?= Html::encode($user->username) ?>,</p>

    <p>Это письмо бы сгенерировано автоматически, по запросу на сброс Вашего пароля от аккаунта на сайте IT&TRANSPORT. Если Вы не делали запрос 
    	на сброс пароля - просто проигнорируйте это письмо. Иначе пройдите по ссылке:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
