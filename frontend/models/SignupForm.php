<?php
namespace frontend\models;

use common\models\User;
use common\models\Author;
use common\models\MailList;
use common\helpers\Translit;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $passwordConfirm;
    
    public $name;
    public $surname;
    public $lastname;
    public $organization;
    public $office_position;

    public $eng_name;
    public $eng_surname;
    public $eng_lastname;
   
    public $verifyCode;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //user fields
            ['username', 'required'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Логин уже занят.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'validateUsername'],

            ['password', 'required'],
            ['password', 'filter', 'filter' => 'trim'],
            ['password', 'string', 'min' => 6],

            ['email', 'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'E-mail уже занят.'],

            ['passwordConfirm', 'required'],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],

            //profile fields
            [['surname', 'name', 'lastname', 'organization', 'office_position'], 'required'],
            [['surname', 'name', 'lastname', 'organization', 'office_position'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'lastname',  'organization', 'office_position'], 'string', 'min' => 2, 'max' => 200],

            [['surname', 'name', 'lastname'], 'filter', 'filter' => function ($value) {
                // нормализация значения происходит тут
                $newValue = mb_strtoupper(mb_substr($value, 0, 1, 'UTF-8'), 'UTF-8');
                $newValue .= mb_substr($value, 1, null, 'UTF-8');
                return $newValue;
            }],

            ['verifyCode', 'required'],
            ['verifyCode', 'captcha'],

        ];
    }

    public function validateUsername() {
        if (!preg_match("/^[a-zA-Z0-9_.]+$/", $this->username)) {
            $this->addError('username', 'Допустимы буквы английского алфавита, цифры, символы нижнего подчеркивания.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя(логин)',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'passwordConfirm' => 'Повторите пароль',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'lastname' => 'Отчество',
            'organization' => 'Учреждение',
            'office_position' => 'Должность',
            'verifyCode' => 'Код',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $userRole = Yii::$app->authManager->getRole('author');

            //Добавляем нового адресата в список рассылки
            /*if(!MailList::find()->where('email = :email',[':email' => $this->email])->exists()){
                $mailList = new MailList();
                $mailList->email = $this->email;
                $mailList->name = $this->name . " " . $this->lastname;
                $mailList->status = 1;
                $mailList->save();
            }*/

            if($user->save()) {
                Yii::$app->authManager->assign($userRole, $user->getId());
                $author = new Author();
                $author->user_id = $user->id;
                $author->name = $this->name;
                $author->surname = $this->surname;
                $author->lastname = $this->lastname;
                $author->organization = $this->organization;
                $author->office_position = $this->office_position;

                $author->eng_name = Translit::transliteration($this->name);
                $author->eng_surname = Translit::transliteration($this->surname);
                $author->eng_lastname = Translit::transliteration($this->lastname);
                
                if($author->save()){
                    return $user;
                }
            }
        }
        return null;
    }
}
