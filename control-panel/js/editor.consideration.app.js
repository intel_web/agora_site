angular.module('editorConsiderationApp', ['ngRoute', 'editorApp', 'initialValue', 'dndLists']);

angular.module('editorConsiderationApp').config(['$routeProvider', function($routeProvider) {
	$routeProvider
		//articles
		.when('/articles/:status', {
			templateUrl: function ($routeParams) {
				return 'editor/article/article-list?status=' + $routeParams.status;
			},
			controller: 'ArticlesController'
		})
		.when('/article/:status/:articleId', {
			templateUrl: function ($routeParams) {
				return 'editor/article/article?status=' + $routeParams.status + '&id=' + $routeParams.articleId;
			},
			controller: 'ArticleController'
		})
		//digests
		.when('/digests', {
			templateUrl: function ($routeParams) {
				return 'editor/digest/digest-list';
			},
			controller: 'DigestListController'
		})
		.when('/create-digest', {
			templateUrl: function ($routeParams) {
				return 'editor/digest/create-digest';
			},
			controller: 'DigestController'
		})
		.when('/update-digest/:digestId', {
			templateUrl: function($routeParams) {
				return 'editor/digest/update-digest?id=' + $routeParams.digestId;
			},
			controller: 'DigestController'
		})
		.when('/digest/:digestId/articles', {
			templateUrl: function($routeParams) {
				return 'editor/digest/get-digest-articles-template?id=' + $routeParams.digestId;
			},
			controller: 'DigestArticlesController',
		})
		//contests articles
		.when('/contest-articles/:status', {
			templateUrl: function ($routeParams) {
				return 'editor/contest-article/article-list?status=' + $routeParams.status;
			},
			controller: 'ContestArticleListController'
		})
		.when('/contest-article/:status/:articleId', {
			templateUrl: function ($routeParams) {
				return 'editor/contest-article/article?status=' + $routeParams.status + '&id=' + $routeParams.articleId;
			},
			controller: 'ContestArticleController'
		})
		//contests
		.when('/contests', {
			templateUrl: function ($routeParams) {
				return 'editor/contest/contest-list';
			},
			controller: 'ContestListController'
		})
		.when('/create-contest', {
			templateUrl: function ($routeParams) {
				return 'editor/contest/create-contest';
			},
			controller: 'ContestController'
		})
		.when('/update-contest/:contestId', {
			templateUrl: function($routeParams) {
				return 'editor/contest/update-contest?id=' + $routeParams.contestId;
			},
			controller: 'ContestController'
		})
		.when('/mail', {
			templateUrl: function ($routeParams) {
				return 'editor/default/mail';
			},
			controller: 'MailController'
		})
		.otherwise({
    		redirectTo: '/articles/2'
		});
}]);

//directives
angular.module('editorConsiderationApp').directive('fileModel', ['$parse', function($parse){
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	}
}]);
angular.module('editorConsiderationApp').directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
                    //scope.date = date;
                    ngModelCtrl.$setViewValue(date);
                    scope.$apply();
                }
            });
        }
    };
});

angular.module('editorConsiderationApp').factory('ShareFactory', function () {
    var mem = {};
 
    return {
        store: function (key, value) {
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});

angular.module('editorConsiderationApp').controller('ErrorController', function($scope, ShareFactory) {

	ShareFactory.store('errorControllerScope', $scope);

	$scope.alerts = {};

	$scope.addAlert = function(message, type) {
		$scope.alerts[type] = $scope.alerts[type]||[];
		$scope.alerts[type].push(message);
	};

	$scope.clearAlerts = function() {
		for(var x in $scope.alerts) {
           delete $scope.alerts[x];
        }
	};
});

angular.module('editorConsiderationApp').controller('EditorInbox', function($scope, $http, $interval, ShareFactory) {

	ShareFactory.store('inboxControllerScope', $scope);

	$scope.inbox = {};

	$scope.updateInbox = function() {
		$http.post('editor/default/inbox', {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(inbox) {
			$scope.inbox = inbox;
		}).error(function(error) {
		}).finally(function() {
		});
	};

	$scope.updateInbox();
	$interval($scope.updateInbox, 10000);
});

angular.module('editorConsiderationApp').directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {                
            ngModel.$parsers.push(function(val) {                    
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {                    
                return '' + val;
            });
        }
    };
});

angular.module('editorConsiderationApp').controller('ArticlesController', function($scope, $http, $routeParams) {
	$scope.articles = [];
	$scope.sciences = [];
	$scope.digests = [];

	$scope.getConsiderationArticles = function() {
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/article/get-article-list', {
			data: {
				articleStatus: $routeParams.status
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(articles) {
			console.log(articles);
			$scope.articles = articles;
			$scope.sciences = $scope.getUniqueSciences($scope.articles);
			$scope.digests = $scope.getUniqueDigests($scope.articles);
		}).error(function(error) {
			$scope.addAlert(error.message, 'alert-danger');
		}).finally(function() {
			//$('.saving-order').hide();
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.getUniqueSciences = function(articles) {
		var sciences = [];
		articles.forEach(function(article) {
			if(sciences.indexOf(article.science.science_name) === -1) {
				sciences.push(article.science.science_name);
			}
		});
		return sciences;
	};

	$scope.getUniqueDigests = function(articles) {
		var digests = [];
		articles.forEach(function(article) {
			if (article.digest) {
				if(digests.indexOf(article.digest.name) === -1) {
					digests.push(article.digest.name);
				}
			}
		});
		return digests;
	};

	$scope.getConsiderationArticles();
});

angular.module('editorConsiderationApp').controller('ArticleController', function($scope, $routeParams, $http, ShareFactory) {
	$scope.articleObject = {};
	$scope.currentComment = "";
	$scope.commentOrderByDate = '-created_at';

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getArticle = function() {
		$scope.errorControllerScope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/article/get-article', {
			data: {
				articleId: $routeParams.articleId,
				articleStatus: $routeParams.status,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(article) {
			$scope.articleObject = article;
			console.log(article);
			$scope.errorControllerScope.addAlert('Статья загружена', 'alert-success');
		}).error(function(error) {
			$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.updateArticleStatus = function() {
		$scope.errorControllerScope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/article/change-article-status', {
			data: {
				articleId: $scope.articleObject.article.id,
				articleStatus: $scope.articleObject.article.status,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(response) {
			$scope.errorControllerScope.addAlert('Статус успешно изменен', 'alert-success');
			$scope.inboxControllerScope.updateInbox();
		}).error(function(error) {
			$scope.errorControllerScope.addAlert(error, 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.sendComment = function() {
		$scope.errorControllerScope.clearAlerts();
		if ($scope.currentComment) {
			$http.post('editor/article/comment-article', {
				data: {
					articleId: $scope.articleObject.article.id,
					comment: $scope.currentComment
				},
				_csrf: yii.getCsrfToken(),
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).success(function(comment) {
					$scope.articleObject.article.comments.unshift(comment);
			}).error(function(error) {
					$scope.errorControllerScope.addAlert(error, 'alert-danger');
			}).finally(function() {
					$scope.currentComment = "";
			});
		}
	};

	$scope.getArticle();

});

angular.module('editorConsiderationApp').controller('DigestController', function($scope, $http, $routeParams, $route, ShareFactory) {
	$scope.currentDigest = {};

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.create = function(currentDigest) {
		$scope.errorControllerScope.clearAlerts();
		var formData = new FormData();
		for (var key in currentDigest) {
			formData.append(key, currentDigest[key]);
		}
		$http(
			{
				method: 'POST',
				url: 'editor/digest/create-digest',
				data: formData,
				headers: {
					'Content-Type': undefined,
					'X-CSRF-Token': yii.getCsrfToken(),
				}
			}).success(function(result) {
				$scope.errorControllerScope.addAlert('Сборник создан!', 'alert-success');
				$scope.currentDigest = {};
				$scope.inboxControllerScope.updateInbox();
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
		});
	};
	$scope.update = function(currentDigest) {
		$scope.errorControllerScope.clearAlerts();
		var formData = new FormData();
		for (var key in currentDigest) {
			formData.append(key, currentDigest[key]);
		}
		$http(
			{
				method: 'POST',
                    url: 'editor/digest/update-digest?id=' + $routeParams.digestId,
                    data: formData,
                    headers: {
                        'Content-Type': undefined,
                        'X-CSRF-Token': yii.getCsrfToken(),
                    }
			}).success(function(result) {
				console.log($scope.currentDigest);
				$scope.errorControllerScope.addAlert('Сборник обновлен!', 'alert-success');
				$scope.inboxControllerScope.updateInbox();
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
		});
	};
	console.log($scope.currentDigest);
});

angular.module('editorConsiderationApp').controller('DigestArticlesController', function($scope, $http, $routeParams, $route, ShareFactory) {
	$scope.currentDigest = {};
	$scope.freeArticles = [];

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getCurrentDigest = function(digestId) {
		$scope.errorControllerScope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/digest/get-digest?id=' + digestId, {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(digest) {
			$scope.currentDigest = digest;
			console.log($scope.currentDigest);
			$scope.getFreeArticles();
			$scope.errorControllerScope.addAlert('Сборник загружен!', 'alert-success');
		}).error(function(error) {
			$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.getFreeArticles = function() {
		$http.post('editor/article/get-free-articles', {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(articles) {
			$scope.freeArticles = articles;
			//$scope.models.lists['A'] = articles;
			console.log($scope.freeArticles);
		}).error(function(error) {
			$scope.errorControllerScope.addAlert('Ошибка при загрузке свободных статей!', 'alert-danger');
		}).finally(function() {
		});
	};

	$scope.updateDigestArticles = function() {
		$scope.errorControllerScope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/digest/update-digest-articles?id=' + $routeParams.digestId, {
			data: {
				freeArticles: $scope.freeArticles,
				digestArticles: $scope.currentDigest.articles
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(response) {
			console.log(response);
			$scope.inboxControllerScope.updateInbox();
			$scope.errorControllerScope.addAlert('Сборник изменен!', 'alert-success');
		}).error(function(error) {
			$scope.errorControllerScope.addAlert(error, 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.moveArticlesFromDigest = function() {
		$scope.currentDigest.articles.forEach(function(article, index) {
			$scope.freeArticles.push(article);
		});
		$scope.currentDigest.articles = [];
	};

	$scope.moveFreeArticlesToDigest = function() {
		$scope.freeArticles.forEach(function(article, index) {
			$scope.currentDigest.articles.push(article);
		});
		$scope.freeArticles = [];
	};

	$scope.getCurrentDigest($routeParams.digestId);
});

angular.module('editorConsiderationApp').controller('DigestListController', function($scope, $http, $routeParams, $route, ShareFactory) {
	$scope.digests = [];

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getDigests = function() {
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/digest/get-digest-list', {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(digests) {
			$scope.digests = digests;
			console.log($scope.digests);
			//$scope.addAlert('Порядок успешно изменен', 'alert-success');
		}).error(function(error) {
			//$scope.addAlert('Ошибка сервера', 'alert-danger');
		}).finally(function() {
			//$('.saving-order').hide();
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.removeDigest = function(event) {
		if (confirm('Вы уверены, что хотите удалить сборник?')) {
			$('#progress-bar').show();
			$('#progress-bar .progress-bar').css('width', '50%');
			$scope.errorControllerScope.clearAlerts();
			$http.post('editor/digest/remove-digest', {
				data: {
					digestId: event.currentTarget.id
				},
				_csrf: yii.getCsrfToken(),
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).success(function() {
				$scope.errorControllerScope.addAlert('Сборник удален!', 'alert-success');
				$('#progress-bar .progress-bar').css('width', '90%');
				$scope.getDigests();
				$scope.inboxControllerScope.updateInbox();
			}).error(function(error) {
				$scope.errorControllerScope.addAlert('Ошибка сервера', 'alert-danger');
			}).finally(function() {
				$('#progress-bar .progress-bar').css('width', '100%');
				$('#progress-bar').hide();
			});
		}
	};

	$scope.getDigests();

});

angular.module('editorConsiderationApp').controller('ContestArticleListController', function($scope, $http, $routeParams, $route, ShareFactory) {
	$scope.articles = [];
	$scope.contests = [];
	$scope.nominations = [];

	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getArticles = function() {
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/contest-article/get-contest-article-list', {
			data: {
				articleStatus: $routeParams.status
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(articles) {
			$scope.articles = articles;
			$scope.contests = $scope.getUniqueContests($scope.articles);
			$scope.nominations = $scope.getUniqueNominations($scope.articles);
			console.log(articles);
			//$scope.addAlert('Порядок успешно изменен', 'alert-success');
		}).error(function(error) {
			//$scope.addAlert('Ошибка сервера', 'alert-danger');
		}).finally(function() {
			//$('.saving-order').hide();
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.getUniqueContests = function(articles) {
		var contests = [];
		articles.forEach(function(article) {
			if(contests.indexOf(article.contest.name) === -1) {
				contests.push(article.contest.name);
			}
		});
		return contests;
	};

	$scope.getUniqueNominations = function(articles) {
		var nominations = [];
		articles.forEach(function(article) {
			if(nominations.indexOf(article.contestNomination.nomination_name) === -1) {
				nominations.push(article.contestNomination.nomination_name);
			}
		});
		return nominations;
	};

	$scope.updateContestArticleScore = function(article) {
		console.log(article);
		if (article) {
			$scope.errorControllerScope.clearAlerts();
			$('#progress-bar').show();
			$('#progress-bar .progress-bar').css('width', '50%');
			$http.post('editor/contest-article/update-article-score', {
				data: {
					articleId: article.id,
					articleScore: article.score,
				},
				_csrf: yii.getCsrfToken(),
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).success(function(response) {
				//console.log(response);
				$scope.errorControllerScope.addAlert('Место работы изменено', 'alert-success');
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
				$scope.getArticles();
			}).finally(function() {
				$('#progress-bar .progress-bar').css('width', '100%');
				$('#progress-bar').hide();
			});
		}
	};

	$scope.getArticles();
});

angular.module('editorConsiderationApp').controller('ContestArticleController', function($scope, $routeParams, $http, ShareFactory) {
	$scope.articleObject = {};
	$scope.currentComment = "";
	$scope.commentOrderByDate = '-created_at';

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getArticle = function() {
		$scope.errorControllerScope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/contest-article/get-article', {
			data: {
				articleId: $routeParams.articleId,
				articleStatus: $routeParams.status,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(article) {
			$scope.articleObject = article;
			console.log(article);
			$scope.errorControllerScope.addAlert('Заявка загружена', 'alert-success');
		}).error(function(error) {
			$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.updateArticleStatus = function() {
		$scope.errorControllerScope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/contest-article/change-article-status', {
			data: {
				articleId: $scope.articleObject.article.id,
				articleStatus: $scope.articleObject.article.status,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(response) {
			$scope.errorControllerScope.addAlert('Статус успешно изменен', 'alert-success');
			$scope.inboxControllerScope.updateInbox();
		}).error(function(error) {
			$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.sendComment = function() {
		$scope.errorControllerScope.clearAlerts();
		if ($scope.currentComment) {
			$http.post('editor/contest-article/comment-article', {
				data: {
					articleId: $scope.articleObject.article.id,
					comment: $scope.currentComment
				},
				_csrf: yii.getCsrfToken(),
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).success(function(comment) {
					$scope.articleObject.article.contestComments.unshift(comment);
			}).error(function(error) {
					$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
					$scope.currentComment = "";
			});
		}
	};

	$scope.getArticle();

});

angular.module('editorConsiderationApp').controller('ContestListController', function($scope, $http, $routeParams, $route, ShareFactory) {
	$scope.contests = [];

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getContests = function() {
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('editor/contest/get-contest-list', {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(contests) {
			$scope.contests = contests;
			console.log($scope.contests);
			//$scope.addAlert('Порядок успешно изменен', 'alert-success');
		}).error(function(error) {
			//$scope.addAlert('Ошибка сервера', 'alert-danger');
		}).finally(function() {
			//$('.saving-order').hide();
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};

	$scope.removeContest = function(event) {
		if (confirm('Вы уверены, что хотите удалить конкурс?')) {
			$('#progress-bar').show();
			$('#progress-bar .progress-bar').css('width', '50%');
			$scope.errorControllerScope.clearAlerts();
			$http.post('editor/contest/remove-contest', {
				data: {
					contestId: event.currentTarget.id
				},
				_csrf: yii.getCsrfToken(),
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).success(function() {
				$scope.errorControllerScope.addAlert('Конкурс удален!', 'alert-success');
				$('#progress-bar .progress-bar').css('width', '90%');
				$scope.getContests();
				$scope.inboxControllerScope.updateInbox();
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
				$('#progress-bar .progress-bar').css('width', '100%');
				$('#progress-bar').hide();
			});
		}
	};

	$scope.getSheet = function(contestId) {
		if (confirm('Вы уверены, что хотите сформировать ведомость по конкурсу?')) {
			$('#progress-bar').show();
			$('#progress-bar .progress-bar').css('width', '50%');
			$scope.errorControllerScope.clearAlerts();
			$http.post('editor/contest/get-sheet', {
				data: {
					contestId: contestId
				},
				_csrf: yii.getCsrfToken(),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).success(function(response) {
				//console.log(response);
				window.location.href = response;
				//$scope.errorControllerScope.addAlert('Конкурс удален!', 'alert-success');
				$('#progress-bar .progress-bar').css('width', '90%');
				/*$scope.getContests();
				$scope.inboxControllerScope.updateInbox();*/
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
				$('#progress-bar .progress-bar').css('width', '100%');
				$('#progress-bar').hide();
			});
		}
	};

	$scope.getParticipantsSheet = function(contestId) {
		if (confirm('Вы уверены, что хотите сформировать ведомость участников конкурса?')) {
			$('#progress-bar').show();
			$('#progress-bar .progress-bar').css('width', '50%');
			$scope.errorControllerScope.clearAlerts();
			$http.post('editor/contest/get-participants-sheet', {
				data: {
					contestId: contestId
				},
				_csrf: yii.getCsrfToken(),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).success(function(response) {
				//console.log(response);
				window.location.href = response;
				//$scope.errorControllerScope.addAlert('Конкурс удален!', 'alert-success');
				$('#progress-bar .progress-bar').css('width', '90%');
				/*$scope.getContests();
				$scope.inboxControllerScope.updateInbox();*/
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
				$('#progress-bar .progress-bar').css('width', '100%');
				$('#progress-bar').hide();
			});
		}
	};

	$scope.getDiplom = function(contestId){
		if (confirm('Вы уверены, что хотите сформировать дипломы?')) {
			$('#progress-bar').show();
			$('#progress-bar .progress-bar').css('width', '50%');
			$scope.errorControllerScope.clearAlerts();
			$http.post('editor/contest/get-diplom', {
				data: {
					contestId: contestId
				},
				_csrf: yii.getCsrfToken(),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).success(function(response) {
				$('#progress-bar .progress-bar').css('width', '90%');
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
				$('#progress-bar .progress-bar').css('width', '100%');
				$('#progress-bar').hide();
			});
		}
	};

	$scope.getContests();

});

angular.module('editorConsiderationApp').controller('ContestController', function($scope, $http, $routeParams, $route, ShareFactory) {
	$scope.currentContest = {};

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.create = function(currentContest) {
		$scope.errorControllerScope.clearAlerts();
		$http(
			{
				method: 'POST',
                    url: 'editor/contest/create-contest',
                    data: $.param($scope.currentContest),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-CSRF-Token': yii.getCsrfToken(),
                    }
			}).success(function(result) {
				$scope.errorControllerScope.addAlert('Конкурс создан!', 'alert-success');
				$scope.currentContest = {};
				$scope.inboxControllerScope.updateInbox();
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
		});
	};
	$scope.update = function(currentContest) {
		$scope.errorControllerScope.clearAlerts();
		var formData = new FormData();
		for (var key in currentContest) {
			formData.append(key, currentContest[key]);
		}
		$http(
			{
				method: 'POST',
                    url: 'editor/contest/update-contest?id=' + $routeParams.contestId,
                    data: formData,
                    headers: {
                        'Content-Type': undefined,
                        'X-CSRF-Token': yii.getCsrfToken(),
                    }
			}).success(function(result) {
				console.log($scope.currentContest);
				$scope.errorControllerScope.addAlert('Конкурс обновлен!', 'alert-success');
				$scope.inboxControllerScope.updateInbox();
			}).error(function(error) {
				$scope.errorControllerScope.addAlert(error.message, 'alert-danger');
			}).finally(function() {
		});
	};
	console.log($scope.currentContest);
});

//on run
angular.module('editorConsiderationApp').run(function($rootScope, $templateCache, ShareFactory) {
    $rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
      ShareFactory.get('errorControllerScope').clearAlerts();
   });
});

angular.module('editorConsiderationApp').controller('MailController', function($scope, $routeParams, $http, ShareFactory) {

	$scope.inboxControllerScope = ShareFactory.get('inboxControllerScope');
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

		/*$http.post('/control-panel/editor/default/mass-mailing', {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(inbox) {
			$scope.inbox = inbox;
		}).error(function(error) {
		}).finally(function() {
		});*/
	//};
});
//bootstraping module
//angular.bootstrap(document.getElementById("considerations-page"), ['editorConsiderationApp']);