angular.module('editorApp', []);

angular.module('editorApp').controller('EditorInbox', function($scope, $http) {
	$scope.inbox = {
		consideration: 0,
		approved: 0,
		published: 0,
	};

	$scope.updateInbox = function() {
		$http.post('/control-panel/editor/default/inbox', {
			data: {},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(inbox) {
			$scope.inbox = inbox;
		}).error(function(error) {
		}).finally(function() {
		});
	};

	$scope.updateInbox();

});