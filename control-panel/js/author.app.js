angular.module('authorApp', ['initialValue']);

angular.module('authorApp').controller('OrganizationsController', function($scope) {
    $scope.organizations = [];
    $scope.organization = "";

    $scope.addOrganization = function(event) {
        event.preventDefault();
        var organization = $scope.organization;
        if (organization !== "") {
            $scope.organizations.push(organization.replace(/\s+/g, ' ').replace(/\.+/g, '.').trim());
            $scope.organization = "";
            document.getElementById("article-organization").value = $scope.createOrganizationString($scope.organizations);
        }
    };

    $scope.parseOrganizationsString = function(string) {
        if (string !== "") {
            var organizations = string.split(';');
            for (var i = 0, maxi = organizations.length - 1; i < maxi; i++) {
                $scope.organizations.push(organizations[i]);
            }
        }
    };

    $scope.removeOrganization = function(index) {
        $scope.organizations.splice(index, 1);
        document.getElementById("article-organization").value = $scope.createOrganizationString($scope.organizations);
    };

    $scope.$watch('organization', function(newValue, oldValue) {
        var organizationInput = document.getElementById("newOrganization");
        if (/[^а-яА-Яa-zA-Z0-9\.\,\-\(\)\'\"\s]/.test($scope.organization)) {
            organizationInput.value = organizationInput.value.substring(0, organizationInput.value.length - 1);
            $scope.organization = $scope.organization.substring(0, $scope.organization.length - 1);
        }
    });

    $scope.createOrganizationString = function(organizationsArray) {
        var organizationsString = "";
        for (var i = 0, maxi = organizationsArray.length; i < maxi; i++) {
            organizationsString += organizationsArray[i] + ';';
        }
        return organizationsString;
    };

    $scope.parseOrganizationsString(document.getElementById("article-organization").value);
});

angular.module('authorApp').controller('TagsController', function($scope) {
    $scope.tags = [];
    $scope.tag = "";

    $scope.addTag = function(event) {
        event.preventDefault();
        var tag = $scope.tag;
        if (tag !== "") {
            $scope.tags.push(tag.replace(/\s+/g, ' ').replace(/\.+/g, '.').trim());
            $scope.tag = "";
            document.getElementById("article-tags").value = $scope.createTagString($scope.tags);
        }
    };

    $scope.parseTagsString = function(string) {
        if (string !== "") {
            var tags = string.split(';');
            for (var i = 0, maxi = tags.length - 1; i < maxi; i++) {
                $scope.tags.push(tags[i]);
            }
        }
    };

    $scope.removeTag = function(index) {
        $scope.tags.splice(index, 1);
        document.getElementById("article-tags").value = $scope.createTagString($scope.tags);
    };

    $scope.$watch('tag', function(newValue, oldValue) {
        var tagInput = document.getElementById("newTag");
        if (/[^а-яА-Яa-zA-Z0-9\.\,\-\(\)\'\"\s]/.test($scope.tag)) {
            tagInput.value = tagInput.value.substring(0, tagInput.value.length - 1);
            $scope.tag = $scope.tag.substring(0, $scope.tag.length - 1);
        }
    });

    $scope.createTagString = function(tagsArray) {
        var tagsString = "";
        for (var i = 0, maxi = tagsArray.length; i < maxi; i++) {
            tagsString += tagsArray[i] + ';';
        }
        return tagsString;
    };

    $scope.parseTagsString(document.getElementById("article-tags").value);
});

angular.module('authorApp').controller('CoauthorsController', function($scope) {
    $scope.coauthors = [];
    $scope.coathor = "";

    $scope.addCoathor = function(event) {
        event.preventDefault();
        var coathor = $scope.coathor;
        if (coathor !== "") {
            $scope.coauthors.push(coathor.replace(/\s+/g, ' ').replace(/\.+/g, '.').trim());
            $scope.coathor = "";
            document.getElementById("article-coauthor").value = $scope.createCoauthorString($scope.coauthors);
        }
    };

    $scope.parseCoathorsString = function(string) {
        if (string !== "") {
            var authors = string.split(';');
            for (var i = 0, maxi = authors.length - 1; i < maxi; i++) {
                $scope.coauthors.push(authors[i]);
            }
        }
    };

    $scope.removeCoathor = function(index) {
        $scope.coauthors.splice(index, 1);
        document.getElementById("article-coauthor").value = $scope.createCoauthorString($scope.coauthors);
    };

    $scope.$watch('coathor', function(newValue, oldValue) {
        var coathorInput = document.getElementById("newCoauthor");
        if (/[^а-яА-Я\.\s]/.test($scope.coathor)) {
            coathorInput.value = coathorInput.value.substring(0, coathorInput.value.length - 1);
            $scope.coathor = $scope.coathor.substring(0, $scope.coathor.length - 1);
        }
    });

    $scope.createCoauthorString = function(authorsArray) {
        var coathorsString = "";
        for (var i = 0, maxi = authorsArray.length; i < maxi; i++) {
            coathorsString += authorsArray[i] + ';';
        }
        return coathorsString;
    };

    $scope.parseCoathorsString(document.getElementById("article-coauthor").value);
});

angular.module('authorApp').controller('FileController', function($scope, $http, $location) {

    $scope.removeFile = function() {
        if (confirm('Вы действительно хотите удалить файл?')) {
            $http.post('remove-file', {
                data: {
                    cn_article_id: $location.absUrl().split('=')[1],
                },
                _csrf: yii.getCsrfToken(),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
            }).success(function(data) {
                document.getElementById("file").style.display = "none";
                document.getElementById("file-form").style.display = "block";
            }).error(function(error) {
                alert("Не удалось удалить файл");
                console.log(error);
            });
        }
    }
});

angular.module('authorApp').controller('ArticleFileController', function($scope, $http, $location) {

    $scope.removeFile = function(fileName) {
        if (confirm('Вы действительно хотите удалить этот файл?')) {
            var fileNameParam = "";
            switch(fileName) {
                case 'article-file': 
                    fileNameParam = 'article';
                    break;
                case 'first-review': 
                    fileNameParam = 'first_review';
                    break;
                case 'second-review': 
                    fileNameParam = 'second_review';
                    break;
                case 'expert-opinion': 
                    fileNameParam = 'expert_opinion';
                    break;
                default:
                    console.log('this option does not exist');
                    return;
            }
            if(fileNameParam !== ""){
                $http.post('remove-file', {
                    data: {
                        article_id: $location.absUrl().split('=')[1],
                        fileName: fileNameParam,
                    },
                    _csrf: yii.getCsrfToken(),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
                }).success(function(data) {
                    document.getElementById(fileName).style.display = "none";
                    document.getElementById(fileName + "-file-form").style.display = "block";
                }).error(function(error) {
                    alert("Не удалось удалить файл");
                    console.log(error);
                });
            }
        }
    }
});

angular.module('authorApp').controller('AuthorStatistic', function($scope, $http, $interval, $location) {

    $scope.authorStatistic = {};

    $scope.updateAuthorStatistic = function() {
        $http.post('author-statistic', {
            data: {},
            _csrf: yii.getCsrfToken(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success(function(data) {
            //console.log(data);
            $scope.authorStatistic = data;
        }).error(function(error) {
        }).finally(function() {
        });
    };

    $scope.updateAuthorStatistic();
    $interval($scope.updateAuthorStatistic, 10000);
});