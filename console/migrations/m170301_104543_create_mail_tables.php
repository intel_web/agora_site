<?php

use yii\db\Schema;
use yii\db\Migration;

class m170301_104543_create_mail_tables extends Migration
{
    public function up()
    {
        $this->createTable('mail_list', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
        ]);

        $this->createTable('category', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createTable('mail_list_category', [
            'id' => Schema::TYPE_PK,
            'mail_list_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createIndex('FK_mail_list_id', 'mail_list_category', 'mail_list_id');

        $this->addForeignKey(
            'FK_mail_list_id', 'mail_list_category', 'mail_list_id', 'mail_list', 'id', 'RESTRICT', 'RESTRICT'
        );        

        $this->createIndex('FK_category_id', 'mail_list_category', 'category_id');

        $this->addForeignKey(
            'FK_category_id', 'mail_list_category', 'category_id', 'category', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->insert('category', [
            'name' => 'Разослать всем',
        ]);

        $this->createTable('mail_history', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'mail_subject' => Schema::TYPE_STRING . ' NOT NULL',
            'mail_body' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'updated_at' => Schema::TYPE_DATETIME . ' NOT NULL',
        ]);

    }

    public function down()
    {
        $this->dropTable('mail_history');
        $this->dropTable('mail_list_category');
        $this->dropTable('mail_list');
        $this->dropTable('category');
    }

}
