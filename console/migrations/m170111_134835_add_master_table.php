<?php

use yii\db\Schema;
use yii\db\Migration;

class m170111_134835_add_master_table extends Migration
{
    public function up()
    {
        $this->createTable('master', [
            'id' => Schema::TYPE_PK,
            'surname' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
            'sex' => Schema::TYPE_STRING . ' NOT NULL',
            'organization' => Schema::TYPE_STRING . ' NOT NULL',
            'office_position' => Schema::TYPE_STRING . ' NOT NULL',
            'academic_degree_id' => Schema::TYPE_INTEGER,
        ]);

        $this->createIndex('FK_master_academic_degree_id', 'master', 'academic_degree_id');

        $this->addForeignKey(
            'FK_master_academic_degree_id', 'master', 'academic_degree_id', 'academic_degree', 'id', 'RESTRICT'
        );

        $this->createTable('cn_article_master', [
            'id' => Schema::TYPE_PK,
            'master_id' => Schema::TYPE_INTEGER,
            'cn_article_id' => Schema::TYPE_INTEGER,
        ]);

        $this->createIndex('FK_master_id', 'cn_article_master', 'master_id');

        $this->addForeignKey(
            'FK_master_id', 'cn_article_master', 'master_id', 'master', 'id', 'RESTRICT', 'RESTRICT'
        );        

        $this->createIndex('FK_cn_article_id', 'cn_article_master', 'cn_article_id');

        $this->addForeignKey(
            'FK_cn_article_id', 'cn_article_master', 'cn_article_id', 'cn_article', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->dropColumn('author', 'science');
    }

    public function down()
    {
        $this->dropTable('cn_article_master');
        $this->dropTable('master');
    }
}
