<?php

use yii\db\Schema;
use yii\db\Migration;

class m160327_183256_agora extends Migration
{
    public function safeUp()
    {
        $this->createTable('editor', [
            'user_id' => Schema::TYPE_PK,
            'surname' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createIndex('FK_editor_user_id', 'editor', 'user_id');

        $this->addForeignKey(
            'FK_editor_user_id', 'editor', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createTable('administrator',[
            'user_id' => Schema::TYPE_PK,
            'surname' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createIndex('FK_administrator_user_id', 'administrator', 'user_id');

        $this->addForeignKey(
            'FK_administrator_user_id', 'administrator', 'user_id', 'user', 'id', 'RESTRICT','RESTRICT'
        );

        $this->createTable('academic_status', [
            'id' => Schema::TYPE_PK . ' NOT NULL',
            'academic_status' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Младший научный сотрудник',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Научный сотрудник',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Старший научный сотрудник',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Ведущий научный сотрудник',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Главный научный сотрудник',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Доцент',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Профессор',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Член-корреспондент',
        ]);

        $this->insert('academic_status', [
            'academic_status' => 'Академик',
        ]);

        $this->createTable('academic_degree', [
            'id' => Schema::TYPE_PK . ' NOT NULL',
            'academic_degree' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'Бакалавр',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'Специалист',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'Магистр',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'к.б.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.б.н.',
        ]);        
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.воен.н..',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.воен.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'к.г.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.г.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'к.г.-м.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.г.-м.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'к.и.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.и.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'к.иск.',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'д.иск.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.м.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.м.н.',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'к.пед.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.пед.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.полит.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.полит.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.п.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.п.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.с.-х.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.с.-х.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'к.социол.н.',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'д.социол.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.т.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.т.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.фарм.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'д.фарм.н.',
        ]);        
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.ф.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.ф.н.',
        ]);

        $this->insert('academic_degree', [
            'academic_degree' => 'к.ф.-м.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.ф.-м.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.филос.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'д.филос.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.х.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'д.х.н.',
        ]);        
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.э.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.э.н.',
        ]);
        
        $this->insert('academic_degree', [
            'academic_degree' => 'к.ю.н.',
        ]);        

        $this->insert('academic_degree', [
            'academic_degree' => 'д.ю.н.',
        ]);


        $this->createTable('author', [
            'user_id' => Schema::TYPE_PK,
            'surname' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
            'birth_date' => Schema::TYPE_DATE,
            'sex' => Schema::TYPE_STRING,
            'science' => Schema::TYPE_STRING,
            'organization' => Schema::TYPE_STRING . ' NOT NULL',
            'office_position' => Schema::TYPE_STRING . ' NOT NULL',
            'address' => Schema::TYPE_STRING,
            'phone_number' => Schema::TYPE_STRING,
            'academic_degree_id' => Schema::TYPE_INTEGER,
            'academic_status_id' => Schema::TYPE_INTEGER,
            'eng_surname' => Schema::TYPE_STRING . ' NOT NULL',
            'eng_name' => Schema::TYPE_STRING . ' NOT NULL',
            'eng_lastname' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createIndex('FK_author_user_id', 'author', 'user_id');

        $this->addForeignKey(
            'FK_author_user_id', 'author', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createIndex('FK_academic_degree_id', 'author', 'academic_degree_id');

        $this->addForeignKey(
            'FK_academic_degree_id', 'author', 'academic_degree_id', 'academic_degree', 'id', 'RESTRICT'
        );

        $this->createIndex('FK_academic_status_id', 'author', 'academic_status_id');

        $this->addForeignKey(
            'FK_academic_status_id', 'author', 'academic_status_id', 'academic_status', 'id', 'RESTRICT'
        );

        $this->createTable('digest', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_DATE,
            'is_published' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
        ]);


        $this->createTable('science', [
            'id' => Schema::TYPE_PK,
            'science_name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->insert('science', [
            'science_name' => 'Интеллектуальные транспортные системы',
        ]);

        $this->insert('science', [
            'science_name' => 'Информационные системы и технологии',
        ]);

        $this->createTable('article',[
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'author_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'udk_index' => Schema::TYPE_STRING,
            'pages' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'updated_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . ' NOT NULL',
            'payment_status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'tags' => Schema::TYPE_STRING,
            'digest_id' => Schema::TYPE_INTEGER,
            'rus_annotation' => Schema::TYPE_TEXT . ' NOT NULL',
            'eng_annotation' => Schema::TYPE_TEXT . ' NOT NULL',
            'coauthor' => Schema::TYPE_STRING,
            'science_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'organization' => Schema::TYPE_STRING .' NOT NULL',
            'grnti' => Schema::TYPE_STRING,
        ]);
        $this->createIndex('FK_author_id', 'article', 'author_id');

        $this->addForeignKey(
            'FK_author_id', 'article', 'author_id', 'author', 'user_id', 'RESTRICT', 'RESTRICT'
        );

        $this->createIndex('FK_digest_id', 'article', 'digest_id');

        $this->addForeignKey(
            'FK_digest_id', 'article', 'digest_id', 'digest', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createIndex('FK_science_id', 'article', 'science_id');

        $this->addForeignKey(
            'FK_science_id', 'article', 'science_id', 'science', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createTable('comment',[
            'id' => Schema::TYPE_PK,
            'comment' => Schema::TYPE_TEXT . ' NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'article_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createIndex('FK_comment_user_id', 'comment', 'user_id');

        $this->addForeignKey(
            'FK_comment_user_id', 'comment', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createIndex('FK_comment_article_id', 'comment', 'article_id');

        $this->addForeignKey(
            'FK_comment_article_id', 'comment', 'article_id', 'article', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createTable('article_author',[
            'id' => Schema::TYPE_PK,
            'author_user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'article_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createIndex('FK_article_author_user_id', 'article_author', 'author_user_id');

        $this->addForeignKey(
            'FK_article_author_user_id', 'article_author', 'author_user_id', 'author', 'user_id', 'RESTRICT', 'RESTRICT'
        );

        $this->createIndex('FK_article_author_article_id', 'article_author', 'article_id');

        $this->addForeignKey(
            'FK_article_author_article_id', 'article_author', 'article_id', 'article', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createTable('rate',[
            'id' => Schema::TYPE_PK,
            'minimum' => Schema::TYPE_INTEGER,
            'maximum' => Schema::TYPE_INTEGER,
            'value' => Schema::TYPE_DOUBLE . ' NOT NULL',
        ]);

        $this->createTable('post', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . " NOT NULL",
            'content' => Schema::TYPE_TEXT . " NOT NULL",
            'preview' => Schema::TYPE_TEXT . " NOT NULL",
            'publish_date' => Schema::TYPE_DATE,
            'status' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT 0",
            'is_presentation' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'thumbnail' => Schema::TYPE_STRING,
            'username' => Schema::TYPE_STRING,
        ]);

        $this->createTable('page', [
        	'id' => Schema::TYPE_PK,
        	'title' => Schema::TYPE_STRING . " NOT NULL",
        	'page_body' => Schema::TYPE_TEXT,
        	'status' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
        	'layout' => Schema::TYPE_STRING,
        ]);

        $this->createTable('site_menu', [
        	'id' => Schema::TYPE_PK,
        	'label' => Schema::TYPE_STRING . ' NOT NULL',
        	'url' => Schema::TYPE_STRING . ' NOT NULL',
        	'page_id' => Schema::TYPE_INTEGER . ' DEFAULT 0',
        	'parent_id' => Schema::TYPE_INTEGER . ' DEFAULT 0',
        ]);

        $this->createTable('contest', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'start_date' => Schema::TYPE_DATE . ' NOT NULL',
            'end_date' => Schema::TYPE_DATE . ' NOT NULL',
            'requirements' => Schema::TYPE_TEXT,
        ]);

        $this->createTable('cn_nomination', [
            'id' => Schema::TYPE_PK,
            'nomination_name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->insert('cn_nomination', [
            'nomination_name' => 'Интеллектуальные транспортные системы',
        ]);

        $this->insert('cn_nomination', [
            'nomination_name' => 'Инновационное управление дорожным движением',
        ]);

        $this->insert('cn_nomination', [
            'nomination_name' => 'Системы и технологии моделирования потоков в транспортных сетях',
        ]);

        $this->insert('cn_nomination', [
            'nomination_name' => 'Инновации в сфере технических средств организации дорожного движения',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Мониторинг состояния объектов транспортной инфраструктуры',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Космомониторинг развития транспортной инфраструктуры',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Автоматизированные информационные системы и информационные технологии',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Программное обеспечение поддержки принятия решений',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Интеллектуальные модели, методы, технологии и системы',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Нейросетевые технологии',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Нечеткие модели и методы',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Распределенные информационные системы',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Облачные технологии',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Геоинформационные системы и технологии',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Методы, модели и информационные технологии в управлении социально-экономическими системами',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Обучающие системы',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Социальные сети',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Охрана окружающей среды',
        ]);
        
        $this->insert('cn_nomination', [
            'nomination_name' => 'Экология и урбанизация, экологическая инфраструктура',
        ]);

        $this->createTable('cn_article',[
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'annotation' => Schema::TYPE_TEXT . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'cn_nomination_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'contest_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'auhtor' => Schema::TYPE_STRING . ' NOT NULL',
            'organization' => Schema::TYPE_STRING .' NOT NULL',
            'coauthor' => Schema::TYPE_STRING . ' NOT NULL',
            'score' => Schema::TYPE_INTEGER,
        ]);

        $this->createIndex('FK_contest_id', 'cn_article', 'contest_id');

        $this->addForeignKey(
            'FK_contest_id', 'cn_article', 'contest_id', 'contest', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createIndex('FK_cn_article_user_id', 'cn_article', 'user_id');

        $this->addForeignKey(
            'FK_cn_article_user_id', 'cn_article', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT'
        );
        
        $this->createIndex('FK_cn_nomination_id', 'cn_article', 'cn_nomination_id');

        $this->addForeignKey(
            'FK_cn_nomination_id', 'cn_article', 'cn_nomination_id', 'cn_nomination', 'id', 'RESTRICT', 'RESTRICT'
        );
        
        
        $this->createTable('cn_comment',[
            'id' => Schema::TYPE_PK,
            'comment' => Schema::TYPE_TEXT . ' NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'cn_article_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createIndex('FK_cn_comment_user_id', 'cn_comment', 'user_id');

        $this->addForeignKey(
            'FK_cn_comment_user_id', 'cn_comment', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT'
        );
        
        $this->createIndex('FK_cn_comment_cn_article_id', 'cn_comment', 'cn_article_id');

        $this->addForeignKey(
            'FK_cn_comment_cn_aticle_id', 'cn_comment', 'cn_article_id', 'cn_article', 'id', 'RESTRICT', 'RESTRICT'
        );

        $this->createTable('coauthor_request',[
            'id' => Schema::TYPE_PK,
            'article_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'article_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'requested_user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createIndex('FK_requested_user_id', 'coauthor_request', 'requested_user_id');

        $this->addForeignKey(
            'FK_requested_user_id', 'coauthor_request', 'requested_user_id', 'author', 'user_id', 'RESTRICT', 'RESTRICT'
        );

    }

    public function safeDown()
    {
        $this->dropTable('editor');
        $this->dropTable('administrator');
        $this->dropTable('article_author');
        $this->dropTable('coauthor_request');
        $this->dropTable('comment');
        $this->dropTable('article');
        $this->dropTable('author');
        $this->dropTable('academic_degree');
        $this->dropTable('academic_status');
        $this->dropTable('digest');
        $this->dropTable('rate');
        $this->dropTable('post');
        $this->dropTable('page');
        $this->dropTable('site_menu');
        $this->dropTable('cn_comment');
        $this->dropTable('cn_article');
        $this->dropTable('contest');
        $this->dropTable('cn_nomination');
        $this->dropTable('science');
        
    }
}
