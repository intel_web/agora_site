<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\MailHistory;
use yii\helpers\Html;

class MailController extends Controller {

	public function actionSend() {
		$mailList = MailHistory::find()->where('status = 0')->all();
		if(!empty($mailList)){
			foreach ($mailList as $value) {
			if(\Yii::$app->mailer->compose()
	            ->setFrom([\Yii::$app->params['adminEmail'] => 'IT&Transport'])
	            ->setTo($value->email)
	            ->setSubject(Html::encode($value->mail_subject))
	            ->setTextBody(Html::encode($value->mail_body))
	            ->send()) {
					$model = MailHistory::findOne($value->id);
					$model->updateAttributes(['status' => 1]);
	            }
			}
		}
	}
}