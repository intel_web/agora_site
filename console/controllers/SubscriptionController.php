<?php
namespace console\controllers;


use yii\console\Controller;
use console\models\ParentSubscription;
use console\models\Message;
use \yii\db\Query;
use linslin\yii2\curl;
//MAILTO=""
//*/10 * * * * php /var/www/schooljournal.its-spc.ru/yii subscription/send
///var/www/schooljournal.its-spc.ru/console/runtime/logs/sms.log
class SubscriptionController extends Controller {
	public function actionSend() {
		//curent date
        $currentDate = date("Y-m-d");
        $twoDays = strtotime('-1 days');
        $twoDaysBackDate = date('Y-m-d', $twoDays);
        
        //get all subscriptions by current date
        foreach (ParentSubscription::find()->where('subscription_start <= :currentDate AND subscription_end >= :currentDate AND payment_status = 1', ['currentDate' => $currentDate])->batch(2) as $subscriptions) {
            
            foreach ($subscriptions as $subscription) {
            	$message = new Message();

                $studentSendArray = array();
                $studentSendArray['subjects'] = array();

                $message->phoneNumber = str_replace(['(', ')', '+', '-', ' '], '', $subscription->parentPhone->phone_number);
                //get student marks by subscription
                $studentMarks = (new Query())
                    ->select(['student.name', 'student_mark.mark', 'subject.abbreviation'])
                    ->from('parent_student')
                    ->join('LEFT JOIN', 'student', 'student.id = parent_student.student_id')
                    ->join('LEFT JOIN', 'school_student', 'school_student.personal_number = student.id')
                    ->join('LEFT JOIN', 'student_mark', 'student_mark.student_id = school_student.id')
                    ->join('LEFT JOIN', 'subject', 'subject.id = student_mark.subject_id')
                    ->where('student_mark.mark_date <= :currentDate AND student_mark.mark_date >= :twoDaysBackDate AND parent_student.id = :parent_student_id', ['currentDate' => $currentDate, 'twoDaysBackDate' => $twoDaysBackDate, 'parent_student_id' => $subscription->parent_student_id])
                    ->orderBy('student.name')
                    ->all();
                //prepare subjects
                for ($i = 0, $maxi = count($studentMarks); $i < $maxi; $i++) {
                    $studentSendArray['name'] = $studentMarks[$i]['name'];
                    $studentSendArray['subjects'][$studentMarks[$i]['abbreviation']] = array();
                }
                for ($i = 0, $maxi = count($studentMarks); $i < $maxi; $i++) {
                    array_push($studentSendArray['subjects'][$studentMarks[$i]['abbreviation']], $studentMarks[$i]['mark']);
                }
                $message->messageText .= iconv("UTF-8", "CP1251", $studentSendArray['name']."\n");
                foreach ($studentSendArray['subjects'] as $key => $subject) {
                    $message->messageText .= iconv("UTF-8", "CP1251", $key).": ";
                    $message->messageText .= iconv("UTF-8", "CP1251", str_replace([',']," ",implode(' ', $subject)));
                    $message->messageText .= "\n";
                }
                if ($message->validate()) {
                    $curl = new curl\Curl();
                    $curl->setOption(CURLOPT_HEADER, 0);
                    $curl->setOption(CURLOPT_TIMEOUT, 10);
                    $curl->setOption(CURLOPT_RETURNTRANSFER, 1);
                    $curl->setOption(CURLOPT_POST, 1);
                    $response = $curl->setOption(
                        CURLOPT_POSTFIELDS, 
                        http_build_query(array(
                            'Http_username' => urlencode(\Yii::$app->params['websms']['login']),
                            'Http_password' => urlencode(\Yii::$app->params['websms']['password']),
                            'Phone_list' => $message->phoneNumber,
                            'Message' => $message->messageText
                        )
                    ))->post('http://www.websms.ru/http_in5.asp');
                    //check post
                    switch ($curl->responseCode) {
                    	//if http request was successful -> check sms send status
                    	case '200':{
                    		//if sms wasn't send -> log it
                    		$trimedResponse = "";
                    		$errorMessage = "";
                    		$trimedResponse = trim($response);
                    		$parsedResponse = [];
                    		preg_match("/error_num\s*=\s*(.*)+/i", $trimedResponse, $parsedResponse);
                    		$errorMessage = preg_replace("/error_num\s*=\s*/i", "", strval($parsedResponse[0]));
                    		if ($errorMessage != "OK\r") {
                    			file_put_contents(\Yii::$app->params['websms']['logFilePath'], $this->getLog('WebSmsError', $response), FILE_APPEND | LOCK_EX);
                    		}
                    	}break;
                    	case 'timeout':{
                    		file_put_contents(\Yii::$app->params['websms']['logFilePath'], $this->getLog('Timeout', $message->toString()), FILE_APPEND | LOCK_EX);
                    	}break;
                    	case '404': {
                    		file_put_contents(\Yii::$app->params['websms']['logFilePath'], $this->getLog('Not found error(404)', $message->toString()), FILE_APPEND | LOCK_EX);
                    	}break;
                    	case '500': {
                    		file_put_contents(\Yii::$app->params['websms']['logFilePath'], $this->getLog('Internal WebSms server error', $message->toString()), FILE_APPEND | LOCK_EX);
                    	}break;
                    }
                }
            }
        }
	}

	protected function getLog($header, $body) {
		$log = "";
		$log .= date('D M j G:i:s T Y').": ";
		$log .= $header."\n";
		$log .= $body."\n\n";
		return $log;
	}
}