<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EditorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/editor.css',
        'css/jquery-ui.css'
    ];
    public $js = [
        'js/jquery-ui.min.js',
        'js/phone.mask.js',
        'js/angular.min.js',
        'js/angular-route.min.js',
        'js/editor.app.js',
        'js/angular-initial-value.js',
        'js/angular-drag-and-drop-lists.min.js',
        'js/editor.consideration.app.js',
        'js/modal.window.module.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
