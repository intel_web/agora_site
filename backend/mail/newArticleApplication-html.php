<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="new-application-mail">
    <p>Доброго времени суток, уважаемый модератор ресурса IT&Транспорт!</p>

    <p>Сообщаем Вам, что на ресурсе IT&Транспорт появилась новая заявка на публикацию в журнале,</p>
    <p>требующая Вашего рассмотрения</p>
    <p>Автор: <?= Html::encode($article->author->surname) . ' ' 
    	. Html::encode($article->author->name) . ' ' 
    	. Html::encode($article->author->lastname)?></p>
    <p>Организация: <?= Html::encode($article->organization) ?></p>
    <p>Статья: <?= Html::encode($article->title) ?></p>


    <p>С уважением,<br> email-бот ресурса IT&Транспорт</p>


</div>
