<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\AcademicDegree;
use yii\helpers\ArrayHelper;

?>
<div id="master-form">

	<?php
		$form = ActiveForm::begin([
			'id' => 'modal-form'
		]);
	?>

		<div class="alert alert-danger" role="alert" style="display: none"></div>

		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-6">
				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			</div>			
		</div>	

		<div class="row">
			<div class="col-md-8">	
				<?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-4">
				<?= $form->field($model, 'sex')->dropDownList([
                'Мужской' => 'Мужской',
                'Женский' => 'Женский',
                ], ['prompt' => 'Выберите пол...']) ?>
			</div>
		
		</div>
		<div class="row">
			<div class="col-md-12">	
				<?= $form->field($model, 'organization')->textInput(['maxlength' => true]) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'office_position')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-6">
				<?= $form->field($model, 'academic_degree_id')->dropDownList(ArrayHelper::map(
        				AcademicDegree::find()->all(), 'id', 'academic_degree'), 
        			['prompt' => 'Выберите научную степень...'])
    			?>
			</div>
		</div>
			
			

		<div class="form-group">
			
			<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', 
				['class' => $model->isNewRecord ? 'btn btn-success':'btn btn-primary']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>