<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Конкурс', 'url' => ['contest/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           
            'title',
            [
            'attribute' => 'master',
            'value' => $model->getMaster(),
            ],
            'author',
            'coauthor',
            'organization',
            [
            'attribute' => 'status',
            'value' => $model->getStatus($model),
            ],
            [
            'attribute' => 'cn_nomination_id',
            'value' => $model->getNomination($model->cn_nomination_id),
            ],
            [
            'attribute' => 'contest_id',
            'value' => $model->getContestName($model->contest_id),
            ],
            [
            'label' => 'Прикрепленные файлы',
            'value' => $model->getAttachedFiles($model),
            'format' => 'raw',
            ],
            'annotation',
        ],
    ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту работу?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Отправить на проверку', ['submit', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row col-md-12" style="padding:1%">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['comment','id'=>$model->id],
        ]); 
    ?>

    <?= $form->field($comment, 'comment')->textarea(['maxlength' => true]) ?>

    <?= Html::submitButton('Добавить комментарий', ['class' => 'btn btn-primary']) ?>

    <?php  ActiveForm::end();  ?>
    
    </div>

    <div class="row col-md-12">
    <?php   
        if(!empty($comments)){
            $comments=array_reverse($comments);
            foreach ($comments as $v) {
                echo('<div class="panel panel-primary">');
                    echo('<div class="panel-heading">');
                        echo('<div class="row">');
                            echo('<div class="col-md-6" text-left>');
                            echo('<h4>'.$v->user->username.'</h4>');
                            //get array of all user roles and print description of roles
                            echo('<h6>'); 
                            foreach(Yii::$app->authManager->getRolesByUser($v->user_id) as $role){
                                echo(Html::encode($role->description));
                            }
                            echo('</h6>');
                            echo('</div>');
                            echo('<div class="col-md-6 text-right">');
                            echo('<h5>'.Html::encode($v->created_at).'</h5>');
                            echo('</div>');
                        echo('</div>');
                    echo('</div>');
                    echo('<div class="panel-body" style="padding:2%">');
                    echo(Html::encode($v->comment));
                    echo('</div>');
                echo('</div>');
            }
        }
    ?>
    </div>

</div>
