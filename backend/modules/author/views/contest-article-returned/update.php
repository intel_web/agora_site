<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = 'Обновить работу';
$this->params['breadcrumbs'][] = ['label' => 'На доработку', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Работа для конкурса', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contest-article-update">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'contest_article'=>$contest_article,
        'cn_article_masters' => $cn_article_masters,
        'file' => $file,
    ]) ?>

</div>
