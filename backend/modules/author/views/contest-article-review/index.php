<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContestArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Работы на рассмотрении';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout' => "{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'coauthor',
            'master',
            [
            'attribute'=>'cn_nomination_id',
            'value'=>'contestNomination.nomination_name',
            ],
            [
            'attribute'=>'contest_id',
            'value'=>'contest.name',
            ],
        ],
    ]); ?>

</div>
