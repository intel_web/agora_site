<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Текущие конкурсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-index" ng-app="authorApp" ng-controller="AuthorStatistic">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'start_date',
            'end_date',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {take-part}',
                'buttons' => [
                    'take-part' => function ($url,$model) {
                        return Html::a('Участвовать', ['contest-article-draft/create','id'=>$model->id], 
                            ['class' => 'btn btn-success']);
                    },
                    'view' => function($url, $model) {
                        return Html::a('Подробнее', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    },
                ],
            ],
        ],
    ]); ?>

    <h3>
        <?= Html::tag('p', Html::encode("Мои работы"), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Черновик <span class="badge">{{(authorStatistic.contestDraft)}}</span>',
                'content' => $this->render('_article-draft', ['dataProvider' => $draftDataProvider]),
            ],
            [
                'label' => 'На рецензии <span class="badge">{{(authorStatistic.contestReview)}}</span>',
                'content' => $this->render('_article-review', ['dataProvider' => $reviewDataProvider]),
            ],            
            [
                'label' => 'На доработке <span class="badge">{{(authorStatistic.contestReturned)}}</span>',
                'content' => $this->render('_article-returned', ['dataProvider' => $returnedDataProvider]),
            ],            
            [
                'label' => 'Принято <span class="badge">{{(authorStatistic.contestAccepted)}}</span>',
                'content' => $this->render('_article-accepted', ['dataProvider' => $acceptedDataProvider]),
            ],
        ],
        'encodeLabels' => false,
    ]); ?>

</div>
