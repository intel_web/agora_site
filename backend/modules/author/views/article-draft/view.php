<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\author\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Черновик', 'url' => ['article-draft/index']];
$this->params['breadcrumbs'][] = 'Публикация';
?>
<div class="article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3 class="text-center"><?= Html::encode($this->title) ?></h3></div> 
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'title',
                [
                'label'=>'Научное направление',
                'value'=>$model->getArticleScience($model->science_id),
                ],
                [
                'label' => 'Cоавторы',
                'format'=>'raw',
                'value' => $model->getAuthorsToString($model),
                ],
                'created_at',
                'updated_at',
                [
                'attribute' => 'status',
                'value' => $model->getStatus($model),
                ],
                'udk_index',
                'grnti',
                'organization',
                'tags',
                'rus_annotation',
                'eng_annotation',
                [
                'label' => 'Прикрепленые файлы',
                'format'=>'raw',
                'value' => $model->getAttachedFiles($model),
                ],
            ],

        ]) ?>
    </div>
    
    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту публикацию?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('Отправить на проверку', ['submit', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

</div>
