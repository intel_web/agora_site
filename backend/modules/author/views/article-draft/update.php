<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\author\models\Article */

$this->title = 'Обновить публикацию';
$this->params['breadcrumbs'][] = ['label' => 'Черновик', 'url' => ['article-draft/index']];
$this->params['breadcrumbs'][] = ['label' => 'Публикация', 'url' => ['view','id'=>$model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="article-update">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'article' => $article,
        'first_review' => $first_review,
        'second_review' => $second_review,
        'expert_opinion' => $expert_opinion,
        'files' => $files,
    ]) ?>

</div>
