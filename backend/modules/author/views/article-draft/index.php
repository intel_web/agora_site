<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use kartik\sidenav\SideNav;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Черновик';
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="article-draft">
        <h3><?= Html::tag('p', Html::encode('Черновик'), ['class' => 'alert alert-success text-center']) ?></h3>
        
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                'attribute' => 'science_id',
                'value' => 'science.science_name',
                ],
                [
                'attribute' => 'coauthor',
                'value' => function($model) {
                        return $model->getAuthorsToString($model);
                    }
                ],
                
                ['class' => 'yii\grid\ActionColumn',
                    'controller' => 'article-draft',
                    'template' => '{view} {update} {delete}',
                ],
            ],
        ]); ?>

        <div class="addArticle">
            <?= Html::a('', Url::toRoute('article-draft/create'), ['class'=>'glyphicon glyphicon-plus', 'title' => 'Подать публикацию']) ?>
        </div>
    </div>