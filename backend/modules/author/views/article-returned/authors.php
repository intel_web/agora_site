<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Author;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticleAuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = ['label' => 'На доработке', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Публикация', 'url' => ['view','id'=> $model->id]];
$this->title = 'Соавторы публикации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-author-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <?php $form = ActiveForm::begin(); ?>
    <div class ="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3>Поиск автора</h3></div>
            <div class="panel-body" style="padding:2">

            <?= $form->field($article_author, 'author_user_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Author::find()->all(), 'user_id',
                 function($model, $defaultValue) {
                     return $model->user_id." ".$model->surname." ".$model->name." ".$model->lastname;}),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите автора...'],
            'pluginOptions' => [
            'allowClear' => true
                ],
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
            </div>

            </div>
        </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="row">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
            'label'=>'ID',
            'value'=>function($model){
                return $model->authorUser->user_id;
                }
            ],
            [
            'attribute'=>'Фамилия',
            'value'=>function($model){
                return $model->authorUser->surname;
                }
            ],
            [
            'attribute'=>'Имя',
            'value'=>function($model){
                return $model->authorUser->name;
                }
            ],
            [
            'attribute'=>'Отчество',
            'value'=>function($model){
                return $model->authorUser->lastname;
                }
            ],
            [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => '{remove}',
            'buttons' => [
                'remove' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                            'title' => "Удалить",
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?')
                        ]);
                    }],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'remove') {
                        return Url::toRoute(['remove', 'article_author_id' => $model['id']]);
                    }
                },
                'contentOptions' => ['class' => 'text-center']
            ]
        ],
    ]); ?>

    </div>

</div>