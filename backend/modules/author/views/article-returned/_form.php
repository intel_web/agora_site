<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Science;

/* @var $this yii\web\View */
/* @var $model backend\modules\author\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form" ng-app="authorApp">

    <?php $form = ActiveForm::begin((['options' => ['enctype' => 'multipart/form-data']]) ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'science_id')->dropDownList(ArrayHelper::map(Science::find()->all(), 'id',
                    function($model){
                        return $model->science_name;
            }),['prompt' => 'Выберите научное направление...']) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'udk_index')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'grnti')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'rus_annotation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'eng_annotation')->textarea(['rows' => 6]) ?>

    <fieldset ng-controller="OrganizationsController" class="fieldset-style">
        <legend class="legend-style">Добавление организаций</legend>

        <?= $form->field($model, 'organization')->textInput(['maxlength' => true, 'readonly' => 'true', 'style' => 'display: none']) ?>
        
        <div class="row">
            <div class="col-md-10">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-tent" aria-hidden="true"></span></span>
                    <input id="newOrganization" ng-model="organization" ng-keydown="$event.keyCode == 13 ? addOrganization($event) : null" type="text" class="form-control" placeholder="Название организации" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-2 text-right">
                <button type="button" class="btn btn-success" ng-click="addOrganization($event)" ng-disabled="(organization) ? (false) : (true)">Добавить</button>
            </div>
        </div>
        <ul class="list-group row-margin coauthors-list">
            <li class="list-group-item" ng-repeat="organizationItem in organizations track by $index">
                {{organizationItem}}
                <div class="pull-right action-buttons">
                    <a href="" class="trash" ng-click="removeOrganization($index)"><span class="glyphicon glyphicon-trash"></span></a>
                </div>
            </li>
        </ul>
    </fieldset>
    <fieldset ng-controller="CoauthorsController" class="fieldset-style">
        <legend class="legend-style">Добавление соавторов</legend>

        <?= $form->field($model, 'coauthor')->textInput(['maxlength' => true, 'readonly' => 'true', 'style' => 'display: none']) ?>
        
        <div class="row">
            <div class="col-md-10">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                    <input id="newCoauthor" ng-model="coathor" ng-keydown="$event.keyCode == 13 ? addCoathor($event) : null" type="text" class="form-control" placeholder="Фамилия И.О. соавтора" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-2 text-right">
                <button type="button" class="btn btn-success" ng-click="addCoathor($event)" ng-disabled="(coathor) ? (false) : (true)">Добавить</button>
            </div>
        </div>
        <ul class="list-group row-margin coauthors-list">
            <li class="list-group-item" ng-repeat="author in coauthors track by $index">
                {{author}}
                <div class="pull-right action-buttons">
                    <a href="" class="trash" ng-click="removeCoathor($index)"><span class="glyphicon glyphicon-trash"></span></a>
                </div>
            </li>
        </ul>
    </fieldset>
    <fieldset ng-controller="TagsController" class="fieldset-style">
        <legend class="legend-style">Добавление тегов</legend>

        <?= $form->field($model, 'tags')->textInput(['maxlength' => true, 'readonly' => 'true', 'style' => 'display: none']) ?>
        
        <div class="row">
            <div class="col-md-10">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-tags" aria-hidden="true"></span></span>
                    <input id="newTag" ng-model="tag" ng-keydown="$event.keyCode == 13 ? addTag($event) : null" type="text" class="form-control" placeholder="ключевое слово" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-2 text-right">
                <button type="button" class="btn btn-success" ng-click="addTag($event)" ng-disabled="(tag) ? (false) : (true)">Добавить</button>
            </div>
        </div>
        <ul class="list-group row-margin coauthors-list">
            <li class="list-group-item" ng-repeat="tagItem in tags track by $index">
                {{tagItem}}
                <div class="pull-right action-buttons">
                    <a href="" class="trash" ng-click="removeTag($index)"><span class="glyphicon glyphicon-trash"></span></a>
                </div>
            </li>
        </ul>
    </fieldset>

    <fieldset ng-controller="ArticleFileController" class="fieldset-style">
        <legend class="legend-style">Файлы публикации</legend>
            <div id="article-file" style= <?= $files['article'] ? 'display:true' : 'display:none' ?> >
                <div class="col-md-2">
                    <img src="../../img/article-img.jpg" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-1">
                    <div class="pull-left action-buttons">
                        <a href="" class="trash" ng-click="removeFile('article-file')"><span class="glyphicon glyphicon-remove" title="Удалить файл работы"></span></a>
                    </div>
                </div>
            </div> 
            <div id="article-file-file-form" style= <?= $files['article'] ? 'display:none' : 'display:true' ?> >
                <div class="col-md-3">
                    <?= $form->field($article, 'file')->fileInput()->label('Файл статьи') ?>
                </div>
            </div>

            <div id="first-review" style= <?= $files['firstReview'] ? 'display:true' : 'display:none' ?> >
                <div class="col-md-2">
                    <img src="../../img/first-review-img.jpg" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-1">
                    <div class="pull-left action-buttons">
                        <a href="" class="trash" ng-click="removeFile('first-review')"><span class="glyphicon glyphicon-remove" title="Удалить файл рецензии №1"></span></a>
                    </div>
                </div>
            </div>   
            <div id="first-review-file-form" style= <?= $files['firstReview'] ? 'display:none' : 'display:true' ?> >
                <div class="col-md-3">
                    <?= $form->field($first_review, 'file')->fileInput()->label('Рецензия № 1') ?>
                </div>
            </div>

            <div id="second-review" style= <?= $files['secondReview'] ? 'display:true' : 'display:none' ?> >
                <div class="col-md-2">
                    <img src="../../img/second-review-img.jpg" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-1">
                    <div class="pull-left action-buttons">
                        <a href="" class="trash" ng-click="removeFile('second-review')"><span class="glyphicon glyphicon-remove" title="Удалить файл рецензии №2"></span></a>
                    </div>
                </div>
            </div>
            <div id="second-review-file-form" style= <?= $files['secondReview'] ? 'display:none' : 'display:true' ?> >
                <div class="col-md-3">
                    <?= $form->field($second_review, 'file')->fileInput()->label('Рецензия № 2') ?>
                </div>
            </div>

            <div id="expert-opinion" style= <?= $files['expertOpinion'] ? 'display:true' : 'display:none' ?> >
                <div class="col-md-2">
                    <img src="../../img/expert-opinion-img.jpg" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-1">
                    <div class="pull-left action-buttons">
                        <a href="" class="trash" ng-click="removeFile('expert-opinion')"><span class="glyphicon glyphicon-remove" title="Удалить файл экспертного заключения"></span></a>
                    </div>
                </div>
            </div>
            <div id="expert-opinion-file-form" style= <?= $files['expertOpinion'] ? 'display:none' : 'display:true' ?> >
                <div class="col-md-3">
                    <?= $form->field($expert_opinion, 'file')->fileInput()->label('Заключение эксперта') ?>
                </div>
            </div>
    </fieldset>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
