<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\sidenav\SideNav;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\author\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'На доработке';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-returned">
       
    <h3><?= Html::tag('p', Html::encode('На доработке'), ['class' => 'alert alert-success text-center']) ?></h3>
       
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout' => "{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
            'attribute'=>'science_id',
            'value'=>'science.science_name',
            ],
            [
            'attribute'=>'coauthor',
            'value'=>function($model){
                    return $model->getAuthorsToString($model);
                },
            ],      

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
