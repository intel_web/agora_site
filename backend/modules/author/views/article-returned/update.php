<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\author\models\Article */

$this->title = 'Обновить статью';
$this->params['breadcrumbs'][] = ['label' => 'На доработке', 'url' => ['article-returned/index']];
$this->params['breadcrumbs'][] = ['label' => 'Публикация', 'url' => ['view','id'=>$model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="article-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'article' => $article,
        'first_review' => $first_review,
        'second_review' => $second_review,
        'expert_opinion' => $expert_opinion,
        'files' => $files,
    ]) ?>

</div>
