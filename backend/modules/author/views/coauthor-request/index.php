<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\sidenav\SideNav;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CoauthorRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запросы на соавторство';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coauthor-request">
      
    <h3><?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?></h3>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
            'attribute'=>'article_id',
            'value'=>function($model){
                    return $model->getArticleTitle($model->article_id);
                }
            ],
            [
            'attribute'=>'requested_user_id',
            'value'=>function($model){
                return $model->requestedUser->surname." "
                    .$model->requestedUser->name." "
                    .$model->requestedUser->lastname." - "
                    .$model->requestedUser->organization;
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'controller' => 'coauthor-request',
                'template' => '{accept} {delete}',
                'buttons' => [
                    'accept' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-ok">&nbsp</span>', $url,
                            ['title'=>'Подтвердить',
                            'data-confirm' => Yii::t('yii', 'Вы подтверждаете соавторство?'),
                            ]);
                    },
                    'delete' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url,
                            ['title'=>'Отклонить',
                            'data-confirm' => Yii::t('yii', 'Вы отклоняете соавторство?'),
                            ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
