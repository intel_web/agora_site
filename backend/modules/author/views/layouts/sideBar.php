<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AuthorAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\sidenav\SideNav;
use yii\helpers\Url;

AuthorAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body ng-app="authorApp">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'IT&Транспорт',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {


        $menuItems[] = ['label' => 'Личный кабинет', 'url' => ['default/index'],
            'items'=>[
                ['label'=>'Личный кабинет','url' => ['default/index']],
                ['label'=>'Редактировать','url' => ['author/update','id'=>Yii::$app->user->identity->id]],
                ['label'=>'Рассылка','url' => ['author/mail-category']],
            ],
        ];

        $menuItems[] = ['label'=>'Публикации','url' => ['article-draft/index']];

        $menuItems[] = ['label' => 'Конкурс', 'url' => ['contest/index']];
       
        $menuItems[] = [
            'label' => 'Задать вопрос',
            'url' => ['default/contact'],
        ];

        $menuItems[] = [
            'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
            </div>
            <div class="col-md-2" ng-controller="AuthorStatistic">
                <?= SideNav::widget([
                    'type' => SideNav::TYPE_PRIMARY,
                    'encodeLabels' => false,
                    'heading' => 'Публикации',
                    'headingOptions' => ['class' => 'text-center'],
                        'items' => [
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.articleDraft)}}</span> Черновик', 'icon' => 'pencil', 'url' => Url::toRoute('article-draft/index')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.articleReview)}}</span> На рецензии', 'icon' => 'envelope', 'url' => Url::toRoute('article-review/index')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.articleReturned)}}</span> На доработке', 'icon' => 'edit', 'url' => Url::toRoute('article-returned/index')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.articleAccepted)}}</span> Принято', 'icon' => 'ok', 'url' => Url::toRoute('article-accepted/index')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.articlePublished)}}</span> Опубликовано', 'icon' => 'file', 'url' => Url::toRoute('article-published/index')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.articleCoAuthors)}}</span> Совместные работы', 'icon' => 'user', 'url' => Url::toRoute('co-author/index')],
                            //['label' => '<span class="fa fa-user-plus"></span>' . ' Запросы на соавторство' . '<span class="pull-right badge">{{(authorStatistic.coAuthorRequest)}}</span>', 'url' => Url::toRoute('coauthor-request/index')],
                            //['label' => '<span class="fa fa-users"></span>' . ' Возможное соавторство' . '<span class="pull-right badge">{{(authorStatistic.coAuthorRequest)}}', 'url' => Url::toRoute('co-author/probable-articles')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.coAuthorRequest)}}</span><span class="fa fa-user-plus"></span> Запросы на соавторство', 'url' => Url::toRoute('coauthor-request/index')],
                            ['label' => '<span class="pull-right badge">{{(authorStatistic.probableArticles)}}</span><span class="fa fa-users"></span> Возможное соавторство', 'url' => Url::toRoute('co-author/probable-articles')],
                        ],
                    ]) 
                ?>
            </div>
            <div class="article col-md-10">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; IT&Транспорт <?= date('Y') ?></p>

        <p class="pull-right"><a href="http://its-spc.ru/">ООО «НПЦ «ИТС»</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
