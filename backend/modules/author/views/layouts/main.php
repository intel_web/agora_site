<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AuthorAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

AuthorAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'IT&Транспорт',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {


        $menuItems[] = ['label' => 'Личный кабинет', 'url' => ['default/index'],
            'items'=>[
                ['label'=>'Личный кабинет','url' => ['default/index']],
                ['label'=>'Редактировать','url' => ['author/update']],
                ['label'=>'Рассылка','url' => ['author/mail-category']],
            ],
        ];

        $menuItems[] = ['label'=>'Публикации', 'url' => ['article-draft/index']];

        $menuItems[] = ['label' => 'Конкурс', 'url' => ['contest/index']];
       
        $menuItems[] = [
            'label' => 'Задать вопрос',
            'url' => ['default/contact'],
        ];

        $menuItems[] = [
            'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; IT&Транспорт <?= date('Y') ?></p>

        <p class="pull-right"><a href="http://its-spc.ru/">ООО «НПЦ «ИТС»</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
