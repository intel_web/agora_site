<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\sidenav\SideNav;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\author\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Совместные публикации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="co-author">
        <h3><?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?></h3>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                'attribute'=>'author_id',
                'value'=>function($model){
                    return $model->articleAuthor->surname." "
                    .$model->articleAuthor->name." "
                    .$model->articleAuthor->lastname;
                }
                ],
                [
                'attribute'=>'coauthor',
                'format'=>'raw',
                'value' =>function($model){ 
                    return $model->getAuthorsToString($model);
                    }
                ],
                [
                'attribute'=>'science_id',
                'value'=>'science.science_name',
                ],
                [
                'attribute' => 'digest_id',
                'value' => function($model){
                        return $model->getArticleDigest($model->digest_id);
                    },
                'format'=> 'raw',
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ], 
            ],
        ]); ?>
</div>