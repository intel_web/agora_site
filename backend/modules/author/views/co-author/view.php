<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\author\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Совместные публикации', 'url' => ['co-author/index']];
$this->params['breadcrumbs'][] = 'Публикация';
?>
<div class="article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'title',
            [
            'label'=>'Научное направление',
            'value'=>$model->getArticleScience($model->science_id),
            ],
            [
            'attribute'=>'author_id',
            'value'=>$model->getArticleAuthorToString($model->author_id),
            ],
            [
            'label' => 'Cоавторы',
            'format'=>'raw',
            'value' => $model->getAuthorsToString($model),
            ],
            'created_at',
            'udk_index',
            'grnti',
            'organization',
            'tags',
            'rus_annotation',
            'eng_annotation',
        ],

    ]) ?>

    </div>
</div>
