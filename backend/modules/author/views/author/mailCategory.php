<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Category;

$this->title = 'Рассылка';
$this->params['breadcrumbs'][] = 'Обновить';

?>

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>
<div class="mail-category-form">

    <?php $form = ActiveForm::begin(['validateOnSubmit' => true]); ?>

    <fieldset class="fieldset-style">
        <legend class="legend-style">Рассылка</legend>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Category::find()->where('id != 1')->all(), 'id',
                            function($model){
                                return $model->name;
                            }),
                        'toggleAllSettings' => [
                                'selectLabel' => '<i class="glyphicon glyphicon-check">Выбрать всё</i>',
                            ],
                        'maintainOrder' => true,
                        'options' => ['placeholder' => 'Выберите темы...', 'id' => 'categoryList'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => true,
                        ],
                    ])->label('Укажите интересующие Вас темы'); ?>
            </div>
        </div>
    </fieldset>


    <div class="form-group">
        <?= Html::submitButton('Подписаться ', ['class' => 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

    <?= $model->mailList->status ? Html::a('Отписаться от рассылки', ['cancel-subscription'], [ 'class' => 'pull-right', 
        	'data-confirm' => 'Вы действительно хотите отписаться от рассылки?']) : ''?>

</div>