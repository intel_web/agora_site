<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Author */

$this->title = 'Обновить информацию об авторе: ' . ' ' . $model->surname . ' '. $model->name . ' ' . $model->lastname;
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="author-update">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
    ]) ?>

</div>
