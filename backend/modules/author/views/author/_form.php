<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\AcademicDegree;
use common\models\AcademicStatus;
use yii\bootstrap\Modal;
use kartik\select2\Select2;

$this->registerJs(
   '$("document").ready(function(){ 
        $("#author-phone_number").mask("+7(999)999-9999", { completed: function () { $(".hidden").slideDown("slow"); } });
    });'
);

?>
    <?php 
        Modal::begin([
            'header' => 'Сменить пароль',
            'id' => 'editModalId',
            'class' => 'modal',
            'size' => 'modal-md',
        ]);

        echo('<div class="modalContent"></div>');

        Modal::end(); 
    ?>

<div class="author-form">

    <?php $form = ActiveForm::begin(); ?>

    <fieldset class="fieldset-style">
        <legend class="legend-style">Личная информация</legend>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'sex')->dropDownList([
                    'Мужской' => 'Мужской',
                    'Женский' => 'Женский',
                    ], ['prompt' => 'Выберите пол...']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'birth_date')->widget(DatePicker::className(),[
                    'language' => 'ru',
                    'clientOptions'=>[
                        'format'=>'yyyy-mm-dd']
                    ]) ?>
            </div>
        </div>

    </fieldset>
    
    <fieldset class="fieldset-style">
        <legend class="legend-style">Организация</legend>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'organization')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'office_position')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'academic_degree_id')->dropDownList(ArrayHelper::map(
                    AcademicDegree::find()->all(), 'id', 'academic_degree'), 
                    ['prompt' => 'Выберите учёную степень...']) 
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'academic_status_id')->dropDownList(ArrayHelper::map(
                    AcademicStatus::find()->all(), 'id', 'academic_status'), 
                    ['prompt' => 'Выберите учёное звание...'])
                ?>
            </div>
        </div>
    </fieldset>

    <fieldset class="fieldset-style">
        <legend class="legend-style">Контактная информация</legend>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Сменить пароль', ['password-change'], ['class'=>'btn btn-danger modalButton pull-right', 'title' => 'Сменить пароль']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
