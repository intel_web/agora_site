<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\AcademicDegree;
use yii\helpers\ArrayHelper;

?>
<div id="password-change-form">
   
    <?php $form = ActiveForm::begin(['id' => 'modal-form']); ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'passwordConfirm')->passwordInput() ?>

    <div class="form-group text-center">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>