<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = 'Редактировать соавторов статьи';
$this->params['breadcrumbs'][] = ['label' => 'Опубликовано', 'url' => ['article-published/index']];
$this->params['breadcrumbs'][] = ['label' => 'Публикация', 'url' => ['article-published/view', 'id'=>$model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="article-update">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
