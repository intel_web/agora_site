<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главная страница автора';
?>
<div class="author-profile-index">
    <h3>
        <p class="alert alert-success text-center">Добро пожаловать в личный кабинет,
            <?= $authorProfile->name . ' ' . $authorProfile->lastname .'!' ?>
        </p>    
    </h3>

    <div class="row">
    	<div class="col-md-4">
    		
    		<?= $this->render('_profile', [
        		'authorProfile' => $authorProfile,
    		]) ?>

    	</div>

        <div class="col-md-4">
            
            <?= $this->render('_article_statistic', [
                'authorStatistic' => $authorStatistic,
            ]) ?>

        </div>    	

        <div class="col-md-4">
    		
    		<?= $this->render('_contest_statistic', [
        		'authorStatistic' => $authorStatistic,
    		]) ?>

    	</div>

    </div>

</div>
