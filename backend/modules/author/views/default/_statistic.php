<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentStudent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-primary">
  <div class="panel-heading">
  	<h2>Публикации</h2>
  </div>
  <div class="panel-body" style="padding:0">
  	<?= DetailView::widget([
        'model' => $authorStatistic,
        'attributes' => [
            'draft',
            'review',
            'returned',
            'accepted',
            'published',
            'coAuthors',
            'total',
        ],
        'options' => [
        	'class' => 'table table-striped table-bordered detail-view',
        	'style' => ['margin-bottom' => 0]
        ]
    ]) ?>
  </div>
</div>