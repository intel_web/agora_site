<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentStudent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-primary">
  <div class="panel-heading">
  	<h2 class="text-center">Мой профиль</h2>
  </div>
  <div class="panel-body" style="padding:0">
  	<?= DetailView::widget([
        'model' => $authorProfile,
        'attributes' => [
        	'surname',
        	'name',
        	'lastname',
          'organization',
          'office_position',
          [
          'attribute' => 'academic_degree_id',
          'value'=> $authorProfile->getAcademicDegreeToString($authorProfile->academic_degree_id), 
          ],
          [
          'attribute' => 'academic_status_id',
          'value'=> $authorProfile->getAcademicStatusToString($authorProfile->academic_status_id), 
          ],
          'sex',
          'birth_date',
          'address',
          'phone_number',
        ],
        'options' => [
        	'class' => 'table table-striped table-bordered detail-view',
        	'style' => ['margin-bottom' => 0]
        ]
    ]) ?>
  </div>
</div>
