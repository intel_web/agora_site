<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

?>
<div class="contest-article-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout' => "{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
            'attribute'=>'cn_nomination_id',
            'value'=>'contestNomination.nomination_name',
            ],
            [
            'attribute'=>'contest_id',
            'value'=>'contest.name',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>