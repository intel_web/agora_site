<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContestArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Черновики',
                'content' => $this->render('_article-draft', ['dataProvider' => $draftDataProvider]),
                'active' => false,
            ],
            [
                'label' => 'На рассмотрении',
                'content' => $this->render('_article-review', ['dataProvider' => $reviewDataProvider]),
            ],            
            [
                'label' => 'На доработку',
                'content' => $this->render('_article-returned', ['dataProvider' => $returnedDataProvider]),
            ],            
            [
                'label' => 'Принятые',
                'content' => $this->render('_article-accepted', ['dataProvider' => $acceptedDataProvider]),
            ],
        ],
    ]); ?>

</div>
