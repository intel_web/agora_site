<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

?>
<div class="contest-article-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
            'attribute'=>'cn_nomination_id',
            'value'=>'contestNomination.nomination_name',
            ],
            [
            'attribute'=>'contest_id',
            'value'=>'contest.name',
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open">', ['contest-article-draft/view','id'=>$model->id]);
                    },
                    'update' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil">', ['contest-article-draft/update', 'id' => $model->id]);
                    },
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash">', ['contest-article-draft/delete', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>