<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = 'Создать заявку';
$this->params['breadcrumbs'][] = ['label' => 'Конкурс', 'url' => ['contest/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-create">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= $this->render('_create_form', [
        'model' => $model,
        'contest_article'=>$contest_article,
        'cn_article_masters' => $cn_article_masters,
    ]) ?>

</div>
