<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ContestNomination;
use common\models\Master;
use yii\bootstrap\Modal;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contest-article-form" ng-app="authorApp">

    <?php 
        Modal::begin([
            'header' => 'Добавить руководителя',
            'id' => 'editModalId',
            'class' => 'modal',
            'size' => 'modal-md',
        ]);

    ?>

        <div id="time-progressbar" class="progress">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
            <span class="sr-only">45% Complete</span>
          </div>
        </div>
        <div class='modalContent'></div>

    <?php

        Modal::end();

    ?>

    <?php $form = ActiveForm::begin((['options' => ['enctype' => 'multipart/form-data']]) ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <fieldset class="fieldset-style">
        <legend class="legend-style">Руководитель</legend>      

        <?= $form->field($cn_article_masters, 'master_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Master::find()->orderBy('surname')->all(),'id',
                        function($model){
                            return $model->surname . ' ' . $model->name . ' ' . $model->lastname . ' - ' . $model->organization;
                        }),
                    'toggleAllSettings' => [
                            'selectLabel' => '<i></i>',
                        ],
                    'maintainOrder' => true,
                    'options' => ['placeholder' => 'Введите ФИО ...', 'id' => 'masterList'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true,
                        'minimumInputLength' => 2,
                        'maximumSelectionLength' => 2,
                    ],
                ])->label('Укажите руководителя (не более 2-х)'); ?>

            <div class="col-md-12">
                <span>Если Вы не нашли своего научного руководителя в списке - добавьте его</span>
                <?= Html::a('<span class="fa fa-user-plus">', ['master-create'], ['class'=>'btn btn-default modalButton', 'title' => 'Добавить руководителя в базу']) ?>
            </div>          

    </fieldset>
    
    <fieldset ng-controller="CoauthorsController" class="fieldset-style">
        <legend class="legend-style">Автор</legend>

        <?= $form->field($model, 'author')->textInput(['maxlength' => true, 'placeholder' => 'Фамилия И.О.']) ?>

        <?= $form->field($model, 'organization')->textInput(['maxlength' => true, 'placeholder' => 'Название организации']) ?>

        <?= $form->field($model, 'coauthor')->textInput(['maxlength' => true, 'readonly' => 'true', 'style' => 'display: none', 'id' => 'article-coauthor']) ?>
        
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Добавить соавторов
                        <span class="fa fa-user-plus"></span></a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row" style="display:block">
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                    <input id="newCoauthor" ng-model="coathor" ng-keydown="$event.keyCode == 13 ? addCoathor($event) : null" type="text" class="form-control" placeholder="Фамилия И.О. соавтора" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-success" ng-click="addCoathor($event)" ng-disabled="(coathor) ? (false) : (true)">Добавить</button>
                            </div>
                        </div>
                        <ul class="list-group row-margin coauthors-list">
                            <li class="list-group-item" ng-repeat="author in coauthors track by $index">
                                {{author}}
                                <div class="pull-right action-buttons">
                                    <a href="" class="trash" ng-click="removeCoathor($index)"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <?= $form->field($model, 'cn_nomination_id')->dropDownList(ArrayHelper::map(
        ContestNomination::find()->all(), 'id', 'nomination_name'), 
        ['prompt' => 'Выберите номинацию...'])
    ?>

     <?= $form->field($model, 'annotation')->textarea(['rows' => 6]) ?>

     <?= $form->field($contest_article, 'file')->fileInput()->label('Файл работы') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
