<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Конкурс', 'url' => ['contest/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           
            'title',
            [
            'attribute' => 'master',
            'value' => $model->getMaster(),
            ],
            'author',
            'coauthor',
            'organization',
            [
            'attribute' => 'status',
            'value' => $model->getStatus($model),
            ],
            [
            'attribute' => 'cn_nomination_id',
            'value' => $model->getNomination($model->cn_nomination_id),
            ],
            [
            'attribute' => 'contest_id',
            'value' => $model->getContestName($model->contest_id),
            ],
            [
            'label' => 'Прикрепленные файлы',
            'value' => $model->getAttachedFiles($model),
            'format' => 'raw',
            ],
            'annotation',
        ],
    ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Отправить на проверку', ['submit', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

</div>
