<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Конкурс', 'url' => ['contest/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           
            'title',
            [
            'attribute' => 'master',
            'value' => $model->getMaster(),
            ],
            'author',
            'coauthor',
            [
            'attribute' => 'status',
            'value' => $model->getStatus($model),
            ],
            [
            'attribute' => 'cn_nomination_id',
            'value' => $model->getNomination($model->cn_nomination_id),
            ],
            [
            'attribute' => 'contest_id',
            'value' => $model->getContestName($model->contest_id),
            ],
            [
            'label' => 'Прикрепленные файлы',
            'value' => $model->getAttachedFiles($model),
            'format' => 'raw',
            ],
            'score',
            'annotation',
        ],
    ]) ?>

    </div>

</div>
