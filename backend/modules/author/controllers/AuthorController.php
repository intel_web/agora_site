<?php

namespace app\modules\author\controllers;

use Yii;
use backend\models\ResetPasswordForm;
use common\models\Author;
use common\models\User;
use common\models\AuthorSearch;
use common\models\MailListCategory;
use common\models\MailList;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use app\modules\author\controllers\DefaultController;

/**
 * AuthorController implements the CRUD actions for Author model.
 */
class AuthorController extends DefaultController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionUpdate() 
    {
        $model = $this->findModel(Yii::$app->user->identity->id);
        $user = User::findOne(Yii::$app->user->identity->id);

        if(($model->load(Yii::$app->request->post()) && $model->save()) && ($user->load(Yii::$app->request->post()) && $user->save())) {
            return $this->redirect(['default/index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user,
            ]);
        }
    }

    public function actionMailCategory() 
    {
        $model = new MailListCategory();
        $mailList = MailList::find()->where('email = :userEmail', [':userEmail' => Yii::$app->user->identity->email])->one();
        if(empty($mailList)) {
            $author = Author::findOne(Yii::$app->user->identity->id);
            if($author !== null){
                (new MailList(['email' => Yii::$app->user->identity->email, 'name' => $author->name . " " . $author->lastname, 'status' => 1]))->save();
                return $this->redirect(['author/mail-category']);
            } else {
                throw new NotFoundHttpException('Произошла ошибка при добавлении Вас в список рассылки, свяжитесь с администратором.');
            }
        }
        $model->mail_list_id = $mailList->id;

        //Получаем массив категорий закрепленных за email пользователя
        $model->category_id = $this->getMailListCategories($model->mail_list_id);
        if(empty($model->category_id)){
            Yii::$app->getSession()->setFlash('warning', 'Поскольку не указаны интересующие Вас темы, Вы получаете письма для всех категорий');
        }
        //Храним список категорий пользователя до изменения
        $oldCategories = $model->category_id;
        if($model->load(Yii::$app->request->post()))
        {
            $mailList->updateAttributes(['status' => 1]);

            if(!empty($model->category_id)) 
            {
                //Проверяем появились ли новые категории у пользователя
                $newCategories = array_diff($model->category_id, $oldCategories);
                //Если появились то добавляем
                if(!empty($newCategories)) {
                    foreach ($newCategories as $value) {
                        (new MailListCategory(['mail_list_id' => $model->mail_list_id,
                            'category_id' => $value]))->save();
                    }
                }
                //Удаленые категории   
                $removedCategories = array_diff($oldCategories, $model->category_id);
                //Если не пусто - удаляем
                if(!empty($removedCategories)){
                    foreach($removedCategories as $value) {
                        MailListCategory::deleteAll(['mail_list_id' => $model->mail_list_id, 'category_id' => $value]);
                    }
                }           
            }
            return $this->redirect(['default/index']);
        } else {
             return $this->render('mailCategory', [
                'model' => $model,
            ]);
        }
    }


    protected function getMailListCategories($mailListId) 
    {
        $categories = array();

        $emailCategories = MailListCategory::find()->where('mail_list_id = :mailListId', [':mailListId' => $mailListId])->all();
        if(!empty($emailCategories)){
            foreach ($emailCategories as $key => $value) {
                array_push($categories, $value->category_id);
            }
        }

        return $categories;
    }

    public function actionCancelSubscription()
    {
        $mailList = MailList::find()->where('email = :userEmail', [':userEmail' => Yii::$app->user->identity->email])->one();
        $mailList->updateAttributes(['status' => 0]);
        Yii::$app->getSession()->setFlash('warning', 'Вы отписались от рассылки :(');
        $mailListCategories = MailListCategory::find()->where('mail_list_id = :mailListId', [':mailListId' => $mailList->id])->all();
        if(!empty($mailListCategories)){
            foreach ($mailListCategories as $value) {
                $value->delete();
            }
        }
        return $this->redirect(['default/index']);
    }

    public function actionPasswordChange() 
    {
        $model = new ResetPasswordForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            return;
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_passwordChange', [
                    'model' => $model,
                ]);                
            }
        }
    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    /**
     * Finds the Author model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Author the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Author::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
