<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\ContestArticle;
use common\models\ContestArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ContestArticleController implements the CRUD actions for ContestArticle model.
 */
class ContestArticleAcceptedController extends DefaultController
{
    /**
     * Displays a single ContestArticle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);

        if($model->user_id==Yii::$app->user->identity->id && $model->status==Yii::$app->params['accepted']){
            return $this->render('view', [
                'model' =>$model,
            ]);
        }else{
            throw new ForbiddenHttpException('The requested page does not exist for you.');
        }
    }

    protected function findModel($id)
    {
        if (($model = ContestArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
