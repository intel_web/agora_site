<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\CoauthorRequest;
use common\models\CoauthorRequestSearch;
use common\models\ArticleAuthor;
use common\models\Article;
use common\models\Author;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

/**
 * CoauthorRequestController implements the CRUD actions for CoauthorRequest model.
 */
class CoauthorRequestController extends DefaultController
{
    /**
     * Lists all CoauthorRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'sideBar';
        
        $searchModel = new CoauthorRequestSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => CoauthorRequest::find()
                ->where(['article_author_id'=> Yii::$app->user->identity->id])
            ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAccept($id)
    {
        $model=$this->findCoauthorRequest($id);

        if($model->article_author_id==Yii::$app->user->identity->id)
        {
            $article_author = new ArticleAuthor();
            $article_author->author_user_id=$model->requested_user_id;
            $article_author->article_id=$model->article_id;
            if(!$article_author->save()){
                Yii::$app->getSession()->setFlash('error', 'Не удалось подтвердить соавторство, возможно соавтор уже добавлен');
            }else{
                $model->delete();
            }
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }

    public function actionDelete($id)
    {
        $model=$this->findCoauthorRequest($id);

        if($model->article_author_id==Yii::$app->user->identity->id)
        {
            $model->delete();

            return $this->redirect('index');
        }else{
             throw new ForbiddenHttpException('Access denied.');
        }
    }

    protected function findCoauthorRequest($id)
    {
        if (($model = CoauthorRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
