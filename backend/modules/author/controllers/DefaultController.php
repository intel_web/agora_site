<?php

namespace app\modules\author\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\grid\GridView;
use common\models\Author;
use common\models\AuthorStatistic;
use common\models\ContactForm;
use common\models\AuthAssignment;



class DefaultController extends Controller {
	
	public function behaviors() {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['author'],
                    ],
                ],
            ],
        ];
	}


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'minLength'=>3,
                'maxLength'=>4,
                'height'=>50,
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function actionIndex() 
    {
        $authorProfile = Author::find()
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->one();
        
        $authorStatistic = new AuthorStatistic();
        $authorStatistic->getAuthorStatistic();

        return $this->render('index', [
            'authorProfile' => $authorProfile,
            'authorStatistic' => $authorStatistic,
        ]);
	
		return $this->render('index');
    }

    public function actionContact()
    {
       
        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо за обращение к нам. Мы ответим как можно быстрее.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке сообщения.');
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAuthorStatistic() 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $authorStatistic = new AuthorStatistic();
        $authorStatistic->getAuthorStatistic();

        return $authorStatistic;
    }

}
