<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\Article;
use common\models\Author;
use common\models\CoauthorRequest;
use common\models\ArticleSearch;
use common\models\CoauthorRequestSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\Url;
use common\models\ArticleAuthor;
/**
 * ArticleController implements the CRUD actions for Article model.
 */
class CoAuthorController extends DefaultController
{
    public function actionIndex()
    {
        $this->layout = 'sideBar';

        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()->joinWith(['articleAuthors'])
                ->where(['article_author.author_user_id'=> Yii::$app->user->identity->id])
                ->andWhere(['status'=> Yii::$app->params['published']])
            ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionView($id)
    {
        if(($coauthor = ArticleAuthor::find()
            ->where(['article_id'=>$id])
            ->andWhere(['author_user_id'=>Yii::$app->user->identity->id]))!=null)
        {   
            $model=$this->findModel($id); 
            
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }

    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionProbableArticles()
    {
        $this->layout = 'sideBar';
        
        $curent_user_surname = Author::findOne(Yii::$app->user->identity->id)->surname;

        $searchModel = new ArticleSearch();

        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()
                ->where('coauthor LIKE :curent_user_surname AND status = :status 
                    AND article.id NOT IN (SELECT coauthor_request.article_id from coauthor_request where coauthor_request.requested_user_id=:current_user) 
                    AND article.id NOT IN (SELECT article_author.article_id from article_author where article_author.author_user_id=:current_user)', 
                    [':curent_user_surname' => '%'.$curent_user_surname.'%', 
                    ':status' => Yii::$app->params['published'],
                    ':current_user'=>Yii::$app->user->identity->id])
        ]);

        return $this->render('probable-articles', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionCoauthorRequest($id)
    {
        if(($model = Article::findOne($id))!==null)
        {
            $coauthor_request = new CoauthorRequest();
            $coauthor_request->article_id=$id;
            $coauthor_request->article_author_id=$model->author_id;
            $coauthor_request->requested_user_id=Yii::$app->user->identity->id;
            if(!$coauthor_request->save())
            {
                Yii::$app->getSession()->setFlash('error', 'Не удалось отправить запрос на соавторство, пожалуйста, попробуйте позже');
            }
        }
        return $this->redirect('index');
    }


    public function actionAccept($id)
    {
        $model=$this->findCoauthorRequest($id);

        if($model->article_author_id==Yii::$app->user->identity->id)
        {
            $article_author = new ArticleAuthor();
            $article_author->author_user_id=$model->requested_user_id;
            $article_author->article_id=$model->article_id;
            if(!$article_author->save()){
                Yii::$app->getSession()->setFlash('error', 'Не удалось подтвердить соавторство, возможно соавтор уже добавлен');
            }else{
                $model->delete();
            }
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findCoauthorRequest($id);

        if($model->article_author_id==Yii::$app->user->identity->id)
        {
            $model->delete();

            return $this->redirect('index');
        }else{
             throw new ForbiddenHttpException('Access denied.');
        }
    }

    protected function findCoauthorRequest($id)
    {
        if (($model = CoauthorRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
