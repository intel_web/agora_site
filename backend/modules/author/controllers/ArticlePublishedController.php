<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\Article;
use common\models\ArticleSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticlePublishedController extends DefaultController
{
    public function actionIndex()
    {
        $this->layout = 'sideBar';
        
        $searchModel = new ArticleSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()
                ->where(['author_id'=> Yii::$app->user->identity->id])
                ->andWhere(['status'=> Yii::$app->params['published']])
            ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->author_id==Yii::$app->user->identity->id)
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect('index');
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }

    public function actionView($id)
    {
        $model=$this->findModel($id);

        if($model->author_id==Yii::$app->user->identity->id){
            
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }

    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
