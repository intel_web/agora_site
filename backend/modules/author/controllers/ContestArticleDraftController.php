<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\ContestArticle;
use common\models\ContestArticleSearch;
use common\models\ContestArticleUploadForm;
use common\models\ContestComment;
use common\models\Master;
use common\models\Author;
use common\models\ContestArticleMaster;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * ContestArticleController implements the CRUD actions for ContestArticle model.
 */
class ContestArticleDraftController extends DefaultController
{

    /**
     * Displays a single ContestArticle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);

        if($model->user_id==Yii::$app->user->identity->id && $model->status==Yii::$app->params['draft']){
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('The requested page does not exist for you.');
        }
    }

    /**
     * Creates a new ContestArticle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ContestArticle();
        $model->contest_id = $id;
        $model->user_id = Yii::$app->user->identity->id;
        $model->author = Author::getAuthorFIO(Yii::$app->user->identity->id);
        $model->organization = Author::getAuthorOrganization(Yii::$app->user->identity->id);
        $contest_article = new ContestArticleUploadForm();
        //Получаем объект с полем в виде строки с id руководителей с формы
        $cn_article_masters = new ContestArticleMaster();
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$id."/")) {
                $contest_article->file = UploadedFile::getInstance($contest_article, 'file');
                $contest_article->fileName = $model->id;
                $contest_article->upload($id);      
                //получаем данные с формы
                $cn_article_masters->load(Yii::$app->request->post());
                //Если массив руководителей не пуст, добавляем записи в таблицу cn_article_master
                if(!empty($cn_article_masters->master_id)) {
                    $this->addCnArticleMasters($model->id, $cn_article_masters->master_id);
                }          
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }else{
            return $this->render('create', [
                'contest_article' => $contest_article,
                'model' => $model,
                'cn_article_masters' => $cn_article_masters,
            ]);
        }
    }

    /**
     * Updates an existing ContestArticle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->user_id==Yii::$app->user->identity->id && ($model->status == Yii::$app->params['draft'] || $model->status == Yii::$app->params['returned']))
        {
            $contest_article = new ContestArticleUploadForm();
            //Получаем массив id научных руководителей работы
            $cn_article_masters = new ContestArticleMaster();
            $cn_article_masters->master_id = $this->getCnArticleMasters($id);

            $file = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$model->id.".*");

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/")) {
                    $contest_article->file = UploadedFile::getInstance($contest_article, 'file');
                    $contest_article->fileName = $model->id;
                    $contest_article->upload($model->contest_id);
                }
                //получаем данные с формы
                $cn_article_masters->load(Yii::$app->request->post());
                $this->addCnArticleMasters($model->id, $cn_article_masters->master_id, $this->getCnArticleMasters($id)); 

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'contest_article' => $contest_article,
                    'cn_article_masters' => $cn_article_masters,
                    'file' => $file,
                ]);
            }
        }else{
            throw new ForbiddenHttpException('The requested page does not exist for you.');
        }
    }

    protected function getCnArticleMasters($cn_article_id) {
        $masters = array();
        $masterArray = ContestArticleMaster::find()->select('master_id')->where('cn_article_id = :cn_article_id', [':cn_article_id' => $cn_article_id])->all();
        foreach ($masterArray as $key => $value) {
            array_push($masters, $value->master_id);
        }
        return $masters;
    }

    protected function addCnArticleMasters($cn_article_id, $newMasterArray, $oldMasterArray=null) {
        if($oldMasterArray==null && !empty($newMasterArray)) {
            foreach ($newMasterArray as $key => $value) {
                $cn_article_master = new ContestArticleMaster();
                $cn_article_master->cn_article_id = $cn_article_id;
                $cn_article_master->master_id = $value;
                $cn_article_master->save();
            }
        }
        if(empty($newMasterArray)) {
            ContestArticleMaster::deleteAll(['cn_article_id' => $cn_article_id]);
        } else {
            if((!empty($newMasterArray)) && (!empty($oldMasterArray))) {   
                //Тут надо с помощью array_diff($newMasterArray, $oldMasterArray) и array_diff($oldMasterArray, $newMasterArray)
                $newMasters = array_diff($newMasterArray, $oldMasterArray);
                if(!empty($newMasters)) {
                    foreach ($newMasters as $key => $value) {
                        $cn_article_master = new ContestArticleMaster();
                        $cn_article_master->cn_article_id = $cn_article_id;
                        $cn_article_master->master_id = $value;
                        $cn_article_master->save();
                    }
                }
                $oldMasters = array_diff($oldMasterArray, $newMasterArray);
                if(!empty($oldMasters)) {
                    foreach ($oldMasters as $key => $value) {
                        ContestArticleMaster::deleteAll(['master_id' => $value]);
                    }
                }
            }
        }
    }

    /**
     * Deletes an existing ContestArticle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->user_id==Yii::$app->user->identity->id)
        {
            //delete contest article
            $model->delete();
            //delete files
            if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/"))
            {
                $files = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$id.".*");
                //delete all files in article folder
                if(!empty($files)) {
                    foreach ($files as $f){
                        unlink($f);       
                    }
                }
            }
            return $this->redirect(['contest/index']);
        }else{
           throw new ForbiddenHttpException('The requested page does not exist for you.');
        }
    }

    protected function getContestArticleComments($contest_article_id)
    {
        $comments = ContestComment::find()->where(['cn_article_id'=>$contest_article_id])->all(); 
            return $comments;
    }

    public function actionSubmit($id)
    {
        $model=$this->findModel($id);

        if($model->user_id==Yii::$app->user->identity->id) {
            $contest_article_file = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$id.".*");
            
            if((($model->status==Yii::$app->params['draft']) || ($model->status==Yii::$app->params['returned'])) && !empty($contest_article_file))
            {
                $model->status=Yii::$app->params['review'];
                $model->save();
                //Отсылаем письмо редактору о новой заявке
                $this->sendEditorEmail($id);
                return $this->redirect(['contest/index']);
            }else{
                Yii::$app->getSession()->setFlash('error', 'Невозможно отправить статью на проверку! Пожалуйста, убедитесь, 
                    что файл прикреплен');
                return $this->redirect(['view','id'=>$id]);
            }
        }else{
            throw new ForbiddenHttpException('This action not allowed to you.');
        }
    }

    /**
     * Finds the ContestArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContestArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContestArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetMaster() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = json_decode(file_get_contents('php://input'));
        if($params->data->masterFIO) {
                $masterList = Master::find()->where('LOCATE(:masterFIO, surname) OR LOCATE(:masterFIO, name) OR LOCATE(:masterFIO, lastname)', 
                    [':masterFIO' => $params->data->masterFIO])->all();
            return $masterList;
        } else {
            return null;
        }
    }

    public function actionRemoveFile() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = json_decode(file_get_contents('php://input'));
        if($params->data->cn_article_id) {
            $model = $this->findModel($params->data->cn_article_id);
            if($model->contest_id && ($model->user_id == Yii::$app->user->identity->id)) {
                if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/")) {
                    $files = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$params->data->cn_article_id.".*");
                    if(!empty($files)) {
                        foreach ($files as $f) {
                            unlink($f);       
                        }
                    }
                }
            } else {
                return \Yii::$app->response->httpStatuses = [406 => 'Not Acceptable'];
            }
            return true;
        } else {
            return \Yii::$app->response->httpStatuses = [424 => 'Method failure'];
        }
    }

    public function actionMasterCreate() {
        $model = new Master();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return;
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_master-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    protected function sendEditorEmail($cn_article_id)
    {
        $cn_article = $this->findModel($cn_article_id);

        return \Yii::$app->mailer->compose(['html' => 'newContestArticleApplication-html'], ['cn_article' => $cn_article])
            ->setFrom([\Yii::$app->params['adminEmail'] => 'IT&Transport'])
            ->setTo(\Yii::$app->params['editorEmail'])
            ->setSubject(Html::encode('Новая заявка на конкурс'))
            ->send();
    }
}