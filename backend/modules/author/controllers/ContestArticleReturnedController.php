<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\ContestArticle;
use common\models\ContestArticleSearch;
use common\models\ContestArticleUploadForm;
use common\models\ContestComment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * ContestArticleController implements the CRUD actions for ContestArticle model.
 */
class ContestArticleReturnedController extends ContestArticleDraftController
{

    public function actionView($id)
    {
        $model=$this->findModel($id);

        $comments=$this->getArticleComments($id);

        $comment = new ContestComment();

        if($model->user_id==Yii::$app->user->identity->id && $model->status==Yii::$app->params['returned'])
        {
            return $this->render('view', [
                'comment' => $comment,
                'comments' => $comments,
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('The requested page does not exist for you.');
        }
    }


    protected function getArticleComments($article_id)
    {
        $comments = ContestComment::find()->where(['cn_article_id'=>$article_id])->all(); 
            return $comments;
    }

    public function actionComment($id)
    {
        $comment = new ContestComment();
        $comment->comment=Yii::$app->request->post('ContestComment')['comment'];
        $comment->created_at=date('Y-m-d H:i:s');
        $comment->user_id=Yii::$app->user->identity->id;
        $comment->cn_article_id=$id;
        if(!$comment->save()){
            Yii::$app->getSession()->setFlash('error', 'Не удалось добавить комментарий');
        }
        return $this->redirect(['view', 'id' => $id]);
    } 

    protected function findModel($id)
    {
        if (($model = ContestArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
