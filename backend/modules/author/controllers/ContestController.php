<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\Contest;
use common\models\ContestArticle;
use common\models\ContestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * ContestController implements the CRUD actions for Contest model.
 */
class ContestController extends DefaultController
{
    /**
     * Lists all Contest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContestSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Contest::find()
                ->where(['<=','start_date', date('Y-m-d')])
                ->andWhere(['>=','end_date', date('Y-m-d')])
            ]);

        $draftDataProvider = new ActiveDataProvider([
            'query' => ContestArticle::find()
                ->where(['status'=> Yii::$app->params['draft']])
                ->andWhere(['user_id'=>Yii::$app->user->identity->id])
                ->joinWith('contest')
                ->orderBy('contest.end_date DESC'),
            //'sort' => false,
        ]);        

        $reviewDataProvider = new ActiveDataProvider([
            'query' => ContestArticle::find()
                ->where(['status'=> Yii::$app->params['review']])
                ->andWhere(['user_id'=>Yii::$app->user->identity->id])
                ->joinWith('contest')
                ->orderBy('contest.end_date DESC'),
            //'sort' => false,
        ]);

        $returnedDataProvider = new ActiveDataProvider([
            'query' => ContestArticle::find()
                ->where(['status'=> Yii::$app->params['returned']])
                ->andWhere(['user_id'=>Yii::$app->user->identity->id])
                ->joinWith('contest')
                ->orderBy('contest.end_date DESC'),
            //'sort' => false,
        ]);

        $acceptedDataProvider = new ActiveDataProvider([
            'query' => ContestArticle::find()
                ->where(['status'=> Yii::$app->params['accepted']])
                ->andWhere(['user_id'=>Yii::$app->user->identity->id])
                ->joinWith('contest')
                ->orderBy('contest.end_date DESC'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'draftDataProvider' => $draftDataProvider,
            'reviewDataProvider' => $reviewDataProvider,
            'returnedDataProvider' => $returnedDataProvider,
            'acceptedDataProvider' => $acceptedDataProvider,
        ]);
    }

    /**
     * Displays a single Contest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new Contest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    protected function findModel($id)
    {
        if (($model = Contest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}            
