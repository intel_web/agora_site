<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\Article;
use common\models\Author;
use common\models\ArticleSearch;
use common\models\ArticleAuthor;
use common\models\ArticleAuthorSearch;
use common\models\Comment;
use common\models\Search;

use common\models\ArticleUploadForm;
use common\models\FirstReviewUploadForm;
use common\models\SecondReviewUploadForm;
use common\models\ExpertOpinionUploadForm;

use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleDraftController extends DefaultController
{
    public function actionIndex()
    {
        $this->layout = 'sideBar';
        $searchModel = new ArticleSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()
                ->where(['author_id'=> Yii::$app->user->identity->id])
                ->andWhere(['status'=> Yii::$app->params['draft']])
            ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
   
    public function actionView($id)
    {
        $model=$this->findModel($id);

        if($model->author_id==Yii::$app->user->identity->id && $model->status == Yii::$app->params['draft']){
            
            return $this->render('view', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }
    
    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $model->created_at = date('Y-m-d H:i:s');
        $model->updated_at = date('Y-m-d H:i:s');
        $model->status = Yii::$app->params['draft'];
        $model->payment_status = 0;
        $model->author_id = Yii::$app->user->identity->id;

        $article = new ArticleUploadForm();
        $article->fileName = Yii::$app->params['articleFileName'];

        $first_review = new FirstReviewUploadForm();
        $first_review->fileName = Yii::$app->params['firstReviewFileName'];

        $second_review = new SecondReviewUploadForm();
        $second_review->fileName = Yii::$app->params['secondReviewFileName'];

        $expert_opinion = new ExpertOpinionUploadForm();
        $expert_opinion->fileName = Yii::$app->params['expertOpinionFileName'];

        $files = null;

        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            if(!file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id))
            {
                mkdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/");
            }
            if(mkdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/".$model->id."/"))
            {
                $article->file = UploadedFile::getInstance($article, 'file');
                $article->upload($model->author_id, $model->id);
                    
                $first_review->file = UploadedFile::getInstance($first_review, 'file');
                $first_review->upload($model->author_id, $model->id);
                    
                $second_review->file = UploadedFile::getInstance($second_review, 'file');
                $second_review->upload($model->author_id, $model->id);
                    
                $expert_opinion->file = UploadedFile::getInstance($expert_opinion, 'file');
                $expert_opinion->upload($model->author_id, $model->id);
                    
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }else{
            return $this->render('create', [
                'model' => $model,
                'article' => $article,
                'first_review' => $first_review,
                'second_review' => $second_review,
                'expert_opinion' => $expert_opinion,
                'files' => $files,
            ]);
        }
    }
    
    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->author_id==Yii::$app->user->identity->id)
        {
            $model->updated_at = date('Y-m-d H:i:s');

            $article = new ArticleUploadForm();
            $article->fileName = Yii::$app->params['articleFileName'];

            $first_review = new FirstReviewUploadForm();
            $first_review->fileName = Yii::$app->params['firstReviewFileName'];

            $second_review = new SecondReviewUploadForm();
            $second_review->fileName = Yii::$app->params['secondReviewFileName'];

            $expert_opinion = new ExpertOpinionUploadForm();
            $expert_opinion->fileName = Yii::$app->params['expertOpinionFileName'];

            $files = $model->getAttachedFilesArray($model);

            glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$model->author_id."/".$model->id."/"."*.*");

            if($model->load(Yii::$app->request->post()) && $model->save()) 
            {
                $author_user_id=Yii::$app->user->identity->id;

                $article->file = UploadedFile::getInstance($article, 'file');
                $article->upload($author_user_id, $id);
                    
                $first_review->file = UploadedFile::getInstance($first_review, 'file');
                $first_review->upload($author_user_id, $id);
                    
                $second_review->file = UploadedFile::getInstance($second_review, 'file');
                $second_review->upload($author_user_id, $id);
                    
                $expert_opinion->file = UploadedFile::getInstance($expert_opinion, 'file');
                $expert_opinion->upload($author_user_id, $id);

                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('update', [
                    'model' => $model,
                    'article' => $article,
                    'first_review' => $first_review,
                    'second_review' => $second_review,
                    'expert_opinion' => $expert_opinion,
                    'files' => $files,
                ]);
            }
        }else{
            throw new ForbiddenHttpException('The requested page does not exist for you.');
        }
    }

    /*Метод создан для удаления файлов прикрепленных к заявке на публикацию*/
    public function actionRemoveFile() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = json_decode(file_get_contents('php://input'));
        if($params->data->fileName && $params->data->article_id) {
            $model = $this->findModel($params->data->article_id);
            //Yii::$app->getSession()->setFlash('error');
            if($model->author_id == Yii::$app->user->identity->id) {
                $files = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$model->author_id."/".$model->id."/". $params->data->fileName .".*");
                if(!empty($files)) {
                    foreach ($files as $file) {
                        unlink($file);   
                    }
                }
            } else {
                return \Yii::$app->response->httpStatuses = [406 => 'Not Acceptable'];
            }
            return true;

        } else {
            return \Yii::$app->response->httpStatuses = [424 => 'Method failure'];
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $article = $this->findModel($id);

        //checking is current user owner of article
        if($article->author_id!=Yii::$app->user->identity->id)
        {
            return;
        }else{
            if(($article->status!=Yii::$app->params['draft']) && ($article->status!=Yii::$app->params['returned'])){
                Yii::$app->getSession()->setFlash('error', 'Невозможно удалить статью');
                return $this->redirect(['view','id'=>$id]);
            }else{

                $article->delete();

                if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/".$article->id))
                {
                    $files = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/".$article->id."/"."*.*");
                    //delete all files in article folder
                    if(!empty($files)){
                        foreach ($files as $f){
                            unlink($f);       
                        }
                    }
                    //delete article folder
                    rmdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/".$article->id);
                }
                return $this->redirect(['index']);
            }
        }
    }

    public function actionAddAuthor($id)
    {
        if($this->findModel($id)->author_id==Yii::$app->user->identity->id)
        {
            if(isset($_POST['ArticleAuthor']['author_user_id']))
            {
                if($_POST['ArticleAuthor']['author_user_id']==Yii::$app->user->identity->id)
                {
                    Yii::$app->getSession()->setFlash('error', 'Нельзя добавлять самого себя в список соавторов');
                    return $this->redirect(['add-author','id'=>$id]);

                }else if($this->isArticleAuthorExists($id,$_POST['ArticleAuthor']['author_user_id'])){
                    Yii::$app->getSession()->setFlash('error', 'Автор уже добавлен');
                    return $this->redirect(['add-author','id'=>$id]);
                }else{
                    $model = new ArticleAuthor();
                    $model->author_user_id=$_POST['ArticleAuthor']['author_user_id'];
                    $model->article_id = $id;
                    if($model->save()){
                        return $this->redirect(['add-author','id'=>$id]);
                    }else{
                        Yii::$app->getSession()->setFlash('error', 'Невозможно добавить автора');
                        return $this->redirect(['add-author','id'=>$id]); 
                    }
                }
            }else{
                $model=$this->findModel($id);
                $article_author = new ArticleAuthor();
                $searchModel = new ArticleAuthorSearch();
                $dataProvider = new ActiveDataProvider([
                    'query' => ArticleAuthor::find()->where(['article_id' => $id])
                ]);

                return $this->render('authors', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'article_author' => $article_author,
                ]);
            }
        }
    }

    public function actionSubmit($id)
    {
        $model=$this->findModel($id);

        $article_file = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".Yii::$app->user->identity->id."/".$model->id."/".Yii::$app->params['articleFileName'].".*");
        
        if((($model->status==Yii::$app->params['draft']) || ($model->status==Yii::$app->params['returned'])) && !empty($article_file))
        {
            $model->status=Yii::$app->params['review'];
            $model->save();
            $this->sendEditorEmail($id);
            return $this->redirect(['index']);
        }else{
            Yii::$app->getSession()->setFlash('error', 'Невозможно отправить статью на проверку! Пожалуйста, убедитесь, 
                что необходимые файлы прикреплены');
            return $this->redirect(['view','id'=>$id]);
        }
    }

    public function actionRemove($article_author_id)
    {
        $model=$this->findArticleAuthor($article_author_id);
        $article_id=$model->article_id;

        if($model->author_user_id==Yii::$app->user->identity->id){
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить самого себя из авторства');
        }else{
            $model->delete();
        }
        return $this->redirect(['add-author','id'=>$article_id]); 
    }


    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findArticleAuthor($id)
    {
        if (($model = ArticleAuthor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function isArticleAuthorExists($article_id, $author_user_id)
    {
        if(($model=ArticleAuthor::find()->where(['article_id'=>$article_id])
            ->andWhere(['author_user_id'=>$author_user_id])
            ->one())!=null){
            return true;
        }else{
            return false;
        }
    }

    protected function findArticleAuthorsByArticleId($article_id)
    {
        $article_authors = array();
        $article_authors = ArticleAuthor::find()->where(['article_id'=>$article_id])->all();
        return $article_authors;
    }

    protected function getArticleComments($article_id)
    {
        $comments = Comment::find()->where(['article_id'=>$article_id])->all(); 
            return $comments;
    }

    protected function sendEditorEmail($article_id)
    {
        $article = $this->findModel($article_id);

        return \Yii::$app->mailer->compose(['html' => 'newArticleApplication-html', 
            'text' => 'newArticleApplication-text'], 
            ['article' => $article])
            ->setFrom([\Yii::$app->params['adminEmail'] => 'IT&Transport'])
            ->setTo(\Yii::$app->params['editorEmail'])
            ->setSubject(Html::encode('Новая заявка'))
            ->send();
    }
}
