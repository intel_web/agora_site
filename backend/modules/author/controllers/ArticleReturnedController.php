<?php

namespace app\modules\author\controllers;

use Yii;
use common\models\Article;
use common\models\ArticleSearch;
use common\models\Comment;

use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\modules\author\controllers\ArticleDraftController;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleReturnedController extends ArticleDraftController
{

    public function actionIndex()
    {
        $this->layout = 'sideBar';
        
        $searchModel = new ArticleSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()
                ->where(['author_id'=> Yii::$app->user->identity->id])
                ->andWhere(['status'=> Yii::$app->params['returned']])
            ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model=$this->findModel($id);

        $comments=$this->getArticleComments($id);

        $comment = new Comment();

        if($model->author_id==Yii::$app->user->identity->id && $model->status == Yii::$app->params['returned'])
        {
            return $this->render('view', [
                'comment' => $comment,
                'comments' => $comments,
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('Access denied.');
        }
    }

    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getArticleComments($article_id)
    {
        $comments = Comment::find()
            ->where(['article_id'=>$article_id])
            ->orderBy('created_at DESC')
            ->all(); 
        return $comments;
    }

    public function actionComment($id)
    {
        $model = new Comment();
        $model->comment=$_POST['Comment']['comment'];
        $model->created_at=date('Y-m-d H:i:s');
        $model->user_id=Yii::$app->user->identity->id;
        $model->article_id=$id;
        if(!$model->save()){
            Yii::$app->getSession()->setFlash('error', 'Не удалось добавить комментарий');
        } else {
            Yii::$app->getSession()->setFlash('success', 'Комментарий добавлен');
        }
        return $this->redirect(['view', 'id' => $id]);
    } 

}
