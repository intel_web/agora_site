<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "administrator".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property string $phone
 *
 * @property User $id0
 */
class Administrator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['id', 'surname', 'name', 'lastname', 'phone'], 'required'],
            [['id'], 'integer'],

            [['surname', 'name', 'lastname'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'lastname'], 'string', 'min' => 2, 'max' => 200],

            [['phone'], 'string', 'max' => 100],
            [['phone'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
