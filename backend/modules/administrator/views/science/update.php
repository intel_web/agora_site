<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Science */

$this->title = 'Обновить научное направление: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Научные направления', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="science-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
