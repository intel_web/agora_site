<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ScienceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Научные направления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="science-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить научное направление', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'science_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
