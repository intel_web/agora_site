<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                [
                'label' => 'Авторы',
                'format'=>'raw',
                'value' => $model->getAuthorsToString($model),
                ],
                'science_id',
                'udk_index',
                'pages',
                'created_at',
                'updated_at',
                [
                'attribute' => 'status',
                'value' => $model->getStatus($model),
                ],
                'payment_status:boolean',
                'tags',
                [
                'attribute' => 'digest_id',
                'value' => $model->getArticleDigest($model->digest_id),
                'format' => 'raw',
                ],
                [
                'label' => 'Прикрепленые файлы',
                'value' => $model->getAttachedFiles($model),
                'format' => 'raw',
                ],
            ],
        ]) ?>
    
    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
