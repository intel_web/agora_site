<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
            'attribute'=>'science_id',
            'value'=>'science.science_name',
            ],
            [
            'attribute' => 'author_id',
            'value' => function($model){
                return $model->articleAuthor->surname." "
                    .$model->articleAuthor->name." "
                    .$model->articleAuthor->lastname;
            }
            ],
            [
            'attribute'=>'coauthor',
            'value'=>function($model){
                return $model->getAuthorsToString($model);
            },
            ],
            'organization',  
            'created_at',
            [
            'attribute' => 'status',
            'value' => function($model){
                switch($model->status){
                    case Yii::$app->params['draft']:
                        return 'Черновик';
                        break;
                    case Yii::$app->params['review']:
                        return 'На рассмотрении';
                        break;
                    case Yii::$app->params['returned']:
                        return 'На доработку';
                        break;
                    case Yii::$app->params['accepted']:
                        return 'Одобрена';
                        break;
                    case Yii::$app->params['published']:
                        return 'Опубликована';
                        break;        
                }
            }],
            [
            'attribute' => 'digest_id',
            'value' => 'digest.name',
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {comment}',
                'buttons' => [
                    'comment' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>',
                            ['article-comment', 'id'=>$model->id], ['title'=>'Показать комментарии']); 
                    },
                ],
            ],
        ],

    ]); ?>

</div>
