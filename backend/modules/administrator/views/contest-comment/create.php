<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContestComment */

$this->title = 'Добавить комментарий';
$this->params['breadcrumbs'][] = ['label'=>'Работы конкурса', 'url'=>['contest-article/index','id'=>$model->contestArticle->contest_id]];
$this->params['breadcrumbs'][] = ['label' => 'Комметарии', 'url' => ['index', 'id'=>\Yii::$app->getRequest()->getQueryParam('id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
