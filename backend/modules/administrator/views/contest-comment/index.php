<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContestCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комментарии к работе: ' .'"'.$model->title.'"';
$this->params['breadcrumbs'][] = ['label'=>'Работы конкурса', 'url'=>['contest-article/index','id'=>$model->contest_id]];
$this->params['breadcrumbs'][] = ['label'=>'Комментарии'];
?>
<div class="contest-comment-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить комменарий', ['create','id'=> \Yii::$app->getRequest()->getQueryParam('id')], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'comment:ntext',
            'created_at',
            [
            'attribute' => 'user_id',
            'value' => 'user.username',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
