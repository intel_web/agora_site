<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContestComment */

$this->title = 'Обновить комментарий: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label'=>'Работы конкурса', 'url'=>['contest-article/index','id'=>$model->contestArticle->contest_id]];
$this->params['breadcrumbs'][] = ['label' => 'Комметарии', 'url' => ['index', 'id'=>$model->cn_article_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
