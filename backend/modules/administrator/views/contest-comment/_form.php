<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use common\models\ContestArticle;
use common\models\User;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\ContestComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contest-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

      <?= $form->field($model, 'created_at')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(User::find()->all(), 'id',
                 function($model, $defaultValue) {
                     return $model->username;
                 }),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите пользователя...'],
            'pluginOptions' => [
            'allowClear' => true
                ],
            ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
