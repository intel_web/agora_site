<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Работы на конкурс', 'url' => ['index','id'=>$model->contest_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
            'attribute' => 'user_id',
            'value' => $model->getContestArticleAuthor($model->user_id),
            ],
            'master',
            [
            'attribute' => 'status',
            'value' => $model->getStatus($model),
            ],
            [
            'attribute' => 'cn_nomination_id',
            'value' => $model->getNomination($model->cn_nomination_id),
            ],
            [
            'attribute' => 'contest_id',
            'value' => $model->getContestName($model->contest_id),
            ],
            [
            'label' => 'Прикрепленные файлы',
            'value' => $model->getAttachedFiles($model),
            'format' => 'raw',
            ],
            'score',
            'annotation',
        ],
        
    ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
