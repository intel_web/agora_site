<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Contest;
use common\models\User;
use common\models\ContestNomination;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contest-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'master')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(User::find()->all(), 'id',
                 function($model, $defaultValue) {
                     return $model->username;
                 }),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите автора...'],
            'pluginOptions' => [
            'allowClear' => true
                ],
            ]); ?>

    <?= $form->field($model, 'annotation')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'status')->dropDownList([
        '1' => 'Черновик',
        '2' => 'На рассмотрении',
        '3' => 'На доработку',
        '4' => 'Принята',
    ]) ?>

    <?= $form->field($model, 'cn_nomination_id')->dropDownList(ArrayHelper::map(ContestNomination::find()->all(), 'id',
                function($model){
                    return $model->nomination_name;
        }),['prompt' => 'Выберите номинацию...']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
