<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContestArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Работы на конкурс: "'.$model->name.'"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-article-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить работу', ['create','id'=> \Yii::$app->getRequest()->getQueryParam('id')], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
            'attribute' => 'status',
            'value' => function($model){
                switch($model->status){
                    case Yii::$app->params['draft']:
                        return 'Черновик';
                        break;
                    case Yii::$app->params['review']:
                        return 'На рассмотрении';
                        break;
                    case Yii::$app->params['returned']:
                        return 'На доработку';
                        break;
                    case Yii::$app->params['accepted']:
                        return 'Одобрена';
                        break;
                    case Yii::$app->params['published']:
                        return 'Опубликована';
                        break;        
                }
            }],
            [
            'attribute'=>'cn_nomination_id',
            'value'=>'contestNomination.nomination_name',
            ],
            [
            'attribute'=>'user_id',
            'value'=>'user.username',
            ],
            'master',
            'score',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {comment}',
                'buttons' => [
                    'comment' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>',  
                            ['contest-article-comment','id'=>$model->id],
                                ['title'=>'Показать комментарии']);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
