<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ContestArticle */

$this->title = 'Обновить работу: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Работы на конкурс', 'url' => ['index','id'=>$model->contest_id]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contest-article-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
