<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CoauthorRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запросы на соавторство';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coauthor-request-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
            'attribute'=>'article_id',
            'value'=>function($model){
                return $model->getArticleTitle($model->article_id);
            }
            ],
            [
            'attribute'=>'requested_user_id',
            'value'=>function($model){
                return $model->requestedUser->user_id." "
                    .$model->requestedUser->surname." "
                    .$model->requestedUser->name." "
                    .$model->requestedUser->lastname;
            }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'controller' => 'coauthor-request',
                'template' => '{accept} {delete}',
                'buttons' => [
                    'accept' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-ok">&nbsp</span>', $url);
                    },
                    'delete' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
