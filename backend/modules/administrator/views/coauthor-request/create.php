<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CoauthorRequest */

$this->title = 'Create Coauthor Request';
$this->params['breadcrumbs'][] = ['label' => 'Coauthor Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coauthor-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
