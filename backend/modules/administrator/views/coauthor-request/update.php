<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CoauthorRequest */

$this->title = 'Update Coauthor Request: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Coauthor Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coauthor-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
