<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комментарии к статье: ' .'"'.$model->title.'"';
$this->params['breadcrumbs'][] = ['label'=>'Все статьи', 'url'=>['article/index']];
$this->params['breadcrumbs'][] = ['label'=>'Комментарии'];
?>
<div class="comment-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить комментарий', ['create','id'=> \Yii::$app->getRequest()->getQueryParam('id')], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'comment:ntext',
            'created_at',
            [
            'attribute' => 'user_id',
            'value' => 'user.username',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
