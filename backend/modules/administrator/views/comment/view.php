<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Comment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label'=>'Все статьи', 'url'=>['article/index']];
$this->params['breadcrumbs'][] = ['label' => 'Комментарии', 'url' => ['index','id' => $model->article_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

 <div class="panel panel-info">
    <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'comment:ntext',
            'created_at',
            'user_id',
            'article_id',
        ],
    ]) ?>

    </div>
    
    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
