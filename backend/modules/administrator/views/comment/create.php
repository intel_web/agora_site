<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Comment */

$this->title = 'Добавить комментарий';
$this->params['breadcrumbs'][] = ['label'=>'Все статьи', 'url'=>['article/index']];
$this->params['breadcrumbs'][] = ['label' => 'Комметарии', 'url' => ['index', 'id'=>\Yii::$app->getRequest()->getQueryParam('id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
