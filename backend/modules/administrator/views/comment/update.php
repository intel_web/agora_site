<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Comment */

$this->title = 'Обновить комментарий: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label'=>'Все статьи', 'url'=>['article/index']];
$this->params['breadcrumbs'][] = ['label' => 'Комментарии', 'url' => ['index','id' => $model->article_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
