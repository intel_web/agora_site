<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleAuthor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Авторство', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-author-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'author_user_id',
                'article_id',
            ],
        ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
