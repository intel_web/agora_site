<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleAuthor */

$this->title = 'Обновить авторство: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Авторство', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="article-author-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
