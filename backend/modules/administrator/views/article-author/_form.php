<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Article;
use common\models\Author;

/* @var $this yii\web\View */
/* @var $model common\models\ArticleAuthor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-author-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'author_user_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Author::find()->all(), 'user_id',
                 function($model, $defaultValue) {
                     return $model->user_id." ".
                     		$model->surname." ".
                     		$model->name." ".
                     		$model->lastname
                     ;}),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите автора...'],
            'pluginOptions' => [
            'allowClear' => true
                ],
            ]); ?>


    <?= $form->field($model, 'article_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Article::find()->all(), 'id',
                 function($model, $defaultValue) {
                     return $model->title;}),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите статью...'],
            'pluginOptions' => [
            'allowClear' => true
                ],
            ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
