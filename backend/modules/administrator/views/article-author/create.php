<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ArticleAuthor */

$this->title = 'Добавить авторство';
$this->params['breadcrumbs'][] = ['label' => 'Авторство', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-author-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
