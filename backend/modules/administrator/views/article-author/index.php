<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticleAuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Авторство';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-author-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить авторство', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
            'attribute' => 'author_user_id',
            'value' => 'authorUser.user.username',
            ],
            [
            'attribute' => 'article_id',
            'value' => 'article.title',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
