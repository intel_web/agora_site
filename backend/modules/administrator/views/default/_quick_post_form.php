<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="well shadow-0">
    <h4>Быстрая запись</h4>
    <hr class="blue-border-color">
    <div class="post-form">

        <?php $form = ActiveForm::begin(['action' => ['post/create']]); ?>
                    
            <?= $form->field($post, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($post, 'content')->textArea(['rows' => '3', 'style' => 'resize:none']) ?>

            <?= $form->field($post, 'preview')->textArea(['rows' => '2', 'style' => 'resize:none']) ?>

            <div class="form-group">

                <?= Html::submitButton($post->isNewRecord ? 'Создать' : 'Обновить', ['class' => $post->isNewRecord ? 'btn btn-primary' : 'btn btn-success']) ?>
                        
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
