<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="well shadow-0">
        <h4>Последние записи</h4>
        <hr class="blue-border-color">
        <?php foreach ($posts as $post) { ?>

        	<?= Html::beginTag('a', ['href' => Url::to(['post/view', 'id' => $post->id], true)]) ?>

        		<?= Html::beginTag('p') ?>

        			<?= Html::tag('time', Html::decode($post->publish_date)) ?>

        			<?= Html::tag('br') ?>

        			<?= Html::tag('span', Html::decode($post->title)) ?>

        		<?= Html::endTag('p') ?>

        	<?= Html::endTag('a') ?>

                <?= Html::tag('hr') ?>

        <?php } ?>
</div>