<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="administrator-default-index">
	<div class="jumbotron shadow-0" style="background-color: #eee">
	  	<h2>
        	<p class="alert alert-success">Добро пожаловать в административную панель сайта IT & TRANSPORT</p>    
    	</h2>
	</div>
    <div class="row">
    	<div class="col-md-4">
    			
			<?= $this->render('_quick_post_form', [
    			'post' => $post,
			]) ?>

    	</div>
    	<div class="col-md-4">

			<?= $this->render('_latest_posts', [
    			'posts' => $latestPosts,
			]) ?>

    	</div>
    	<div class="col-md-4">

    		<?= $this->render('_statistic', [
    			'users' => $users,
    			'pages' => $pages,
    			'posts' => $posts
    		]) ?>
    		
    	</div>
    </div>
</div>
