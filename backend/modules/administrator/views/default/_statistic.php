<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="well shadow-0">
	<h4>Статистика</h4>
    <hr class="blue-border-color">
    <p>

    	<?= Html::beginTag('a', ['href' => Url::to(['user/index'], true)]) ?>

    		<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
    		<span>Пользователей - <?= ($users) ? ($users) : ('-') ?></span>

    	<?= Html::endTag('a') ?>

    </p>
    <p>

    	<?= Html::beginTag('a', ['href' => Url::to(['page/index'], true)]) ?>

	    	<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
	    	<span>Страниц - <?= ($pages) ? ($pages) : ('-') ?></span>

    	<?= Html::endTag('a') ?>
    </p>
    <p>

    	<?= Html::beginTag('a', ['href' => Url::to(['post/index'], true)]) ?>

	    	<span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span>
	    	<span>Записей - <?= ($posts) ? ($posts) : ('-') ?></span>
    	
    	<?= Html::endTag('a') ?>
    </p>
</div>