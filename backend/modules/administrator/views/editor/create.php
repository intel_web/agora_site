<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Editor */

$this->title = 'Добавить редактора';
$this->params['breadcrumbs'][] = ['label' => 'Редакторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="editor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
