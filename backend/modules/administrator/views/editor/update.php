<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Editor */

$this->title = 'Обновить информацию о редакторе: ' . ' ' . $model->user->username;
$this->params['breadcrumbs'][] = ['label' => 'Редакторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="editor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
