<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rate */

$this->title = 'Обновить тариф: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Тарифы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="rate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
