<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Author */

$this->title = $model->user->username;
$this->params['breadcrumbs'][] = ['label' => 'Авторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'user_id',
                'surname',
                'name',
                'lastname',
                'birth_date',
                'sex',
                'organization',
                'office_position',
                'address',
                'phone_number',
                'academic_degree_id',
                'academic_status_id',
                'eng_surname',
                'eng_name',
                'eng_lastname',
            ],
        ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
