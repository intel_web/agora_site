<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\AcademicDegree;
use common\models\AcademicStatus;
use common\models\User;

?>

<div class="author-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(
        User::find()->all(), 'id', 'username'), ['prompt' => 'Выберите пользователя...']) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'eng_surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eng_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eng_lastname')->textInput(['maxlength' => true]) ?>

    
    <?= $form->field($model, 'birth_date')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'sex')->dropDownList([
    'Мужской' => 'Мужской',
    'Женский' => 'Женский',
    ], ['prompt' => 'Выберите пол...']) ?>

    <?= $form->field($model, 'organization')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'academic_degree_id')->dropDownList(ArrayHelper::map(
        AcademicDegree::find()->all(), 'id', 'academic_degree'), 
        ['prompt' => 'Выберите учёную степень...']) 
    ?>
    
    <?= $form->field($model, 'academic_status_id')->dropDownList(ArrayHelper::map(
        AcademicStatus::find()->all(), 'id', 'academic_status'), 
        ['prompt' => 'Выберите учёное звание...'])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
