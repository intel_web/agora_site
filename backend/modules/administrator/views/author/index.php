<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Авторы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить автора', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
            'attribute'=>'user_id',
            'value'=>'user.username',
            ],
            'surname',
            'name',
            'lastname',
            'birth_date',
            'sex',

            'organization',
            'office_position',
            'address',
            'phone_number',
            [
            'attribute'=>'academic_degree_id',
            'value'=>'academicDegree.academic_degree',
            ],
            [
            'attribute'=>'academic_status_id',
            'value'=>'academicStatus.academic_status',
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>

</div>
