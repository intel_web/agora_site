<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Записи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h3>
        <p class="alert alert-success text-center"><?= Html::encode($this->title) ?></p>    
    </h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'publish_date',
                'value' => 'publish_date',
                'filter' => DatePicker::widget(['model' => $searchModel, 'attribute' => 'publish_date', 'template' => '{addon}{input}', 'clientOptions' => ['autoclose' => true, 'format' => 'yyyy-m-dd']]),
                'format' => 'html',
            ],
            [
                'attribute' => 'status',
                'filter' => array('1' => 'Опубликована', '0' => 'В ожидании'),
                'content' => function($data) {
                    $status = "";
                    ($data->status) ? ($status = 'Опубликована') : ($status = 'В ожидании');
                    return $status;
                }
            ],
            [
                'attribute' => 'is_presentation',
                'filter' => array('1' => 'Да', '0' => 'Нет'),
                'content' => function($data) {
                    $is_presentation = "";
                    ($data->is_presentation) ? ($is_presentation = 'Да') : ($is_presentation = 'Нет');
                    return $is_presentation;
                }
            ],
            // 'thumbnail',
            // 'username',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
