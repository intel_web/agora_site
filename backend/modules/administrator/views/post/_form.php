<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use dosamigos\ckeditor\CKEditor;
use iutbay\yii2kcfinder\KCFinderInputWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-xs-8">
                <div class="well shadow-0">
                    <h4>Основной контент</h4>
                    <hr class="blue-border-color">
                    
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= 
                        $form->field($model, 'content')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'Standard'
                        ]) 
                    ?>  

                </div>
                <div class="well shadow-0">
                    <h4>Превью записи</h4>
                    <hr class="blue-border-color">

                    <?= 
                        $form->field($model, 'preview')->widget(CKEditor::className(), [
                            'options' => ['rows' => 0],
                            'preset' => 'Standard'
                        ]) 
                    ?>  

                </div>
            </div>
            <div class="col-xs-4">
                <div class="well shadow-0">
                    <h4>Публикация</h4>
                    <hr class="blue-border-color">

                    <div class="form-group">

                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-success']) ?>
                    
                    </div>
                    
                    <?= $form->field($model, 'status')->checkBox(['label' => 'Опубликовать']) ?>

                    <?= $form->field($model, 'is_presentation')->checkBox(['label' => 'Поместить в слайдер на главную']) ?>
                
                    <?= 
                        $form->field($model, 'publish_date')->widget(
                            DatePicker::className(),[
                                'language' => 'ru',
                                'template' => '{addon}{input}',
                                'clientOptions'=>[
                                    'autoclose' => true,
                                    'format'=>'yyyy-mm-dd'
                                ]
                            ])
                    ?>

                </div>
                <div class="well shadow-0">
                    <h4>Превью новости</h4>
                    <hr class="blue-border-color">

                    <?= 
                        $form->field($model, 'thumbnail')->widget(KCFinderInputWidget::className(), [
                                'multiple' => false,
                        ]); 
                    ?>

                    <hr class="blue-border-color">
                    <div class="row current-thumbnail">
                        <div class="col-xs-12 text-center">
                            <h4>Текущее превью</h4>
                            <div class="thumbnail">

                                <?= Html::img( $model->thumbnail, $options = [] ) ?>
                                
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
