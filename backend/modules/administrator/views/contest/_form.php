<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Contest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contest-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start_date')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'requirements')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
