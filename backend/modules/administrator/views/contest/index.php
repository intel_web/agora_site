<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Конкурсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Создать конкурс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'start_date',
            'end_date',
            'requirements:ntext',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {works} {delete}',
                'buttons' => [
                    'works' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-log-in"></span>',  
                            ['contest-article','id'=>$model->id], ['title'=>'Работы конкурса']);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
