<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MailList */

$this->title = 'Обновить: ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Список рассылки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="mail-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
