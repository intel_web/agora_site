<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MailList */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Список рассылки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
