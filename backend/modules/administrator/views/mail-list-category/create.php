<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MailListCategory */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Mail List Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-list-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
