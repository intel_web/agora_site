<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MailListCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email по категориям';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-list-category-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
            'attribute' => 'mail_list_id',
            'value' => 'mailList.email',
            ],            
            [
            'attribute' => 'category_id',
            'value' => 'category.name',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
