<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Digest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="digest-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'is_published')->checkbox(['checked' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
