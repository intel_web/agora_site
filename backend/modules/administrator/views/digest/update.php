<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Digest */

$this->title = 'Обновить журнал: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Журналы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="digest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
