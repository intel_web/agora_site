<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MailHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История рассылки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-history-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить письмо', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            'mail_subject',
            //'mail_body',
            [
            'attribute' => 'status',
            'value' => function($model) {
                    return $model->status ? 'Отправлено' : 'В очереди';
                }
            ],
            'created_at',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
