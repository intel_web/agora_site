<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\SiteMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-menu-form">

    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'page_id')->dropDownList(ArrayHelper::map($model->getPagesForMenuItem(), 'id', 'title'), ['prompt' => 'Страница']) ?>

	    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($model->getAllMenuItems(), 'id', 'label'), ['prompt' => 'Родитель']) ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

    <?php ActiveForm::end(); ?>

</div>
