<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SiteMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню сайта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-menu-index">
    <h3>
        <p class="alert alert-success text-center"><?= Html::encode($this->title) ?></p>    
    </h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать пункт меню', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'label:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
