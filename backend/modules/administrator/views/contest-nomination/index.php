<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContestNominationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Номинации конкурса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-nomination-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить номинацию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomination_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
