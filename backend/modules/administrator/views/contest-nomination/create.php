<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContestNomination */

$this->title = 'Добавить номинацию';
$this->params['breadcrumbs'][] = ['label' => 'Номинации конкурса', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-nomination-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
