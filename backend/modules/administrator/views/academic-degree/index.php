<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AcademicDegreeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ученые степени';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-degree-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить ученую степень', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'academic_degree',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
