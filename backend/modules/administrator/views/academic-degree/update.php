<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicDegree */

$this->title = 'Обновить ученую степень: ' . ' ' . $model->academic_degree;
$this->params['breadcrumbs'][] = ['label' => 'Ученые степени', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="academic-degree-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
