<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AcademicDegree */

$this->title = 'Добавить ученую степень';
$this->params['breadcrumbs'][] = ['label' => 'Ученые степени', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-degree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
