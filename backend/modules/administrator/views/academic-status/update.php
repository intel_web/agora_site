<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicStatus */

$this->title = 'Обновить ученое звание: ' . ' ' . $model->academic_status;
$this->params['breadcrumbs'][] = ['label' => 'Ученые звания', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="academic-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
