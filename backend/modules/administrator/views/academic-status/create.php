<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AcademicStatus */

$this->title = 'Ученое звание';
$this->params['breadcrumbs'][] = ['label' => 'Ученое звание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
