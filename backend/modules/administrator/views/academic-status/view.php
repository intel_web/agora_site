<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicStatus */

$this->title = $model->academic_status;
$this->params['breadcrumbs'][] = ['label' => 'Ученые звания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-status-view">

    <div class="panel panel-info">
        <div class="panel-heading"><h3><?= Html::encode($this->title) ?></h3></div> 

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'academic_status',
            ],
        ]) ?>

    </div>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
