<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AcademicStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ученые звания';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-status-index">

    <h3>
        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    </h3>

    <p>
        <?= Html::a('Добавить ученое звание', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'academic_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
