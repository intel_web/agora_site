<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use dosamigos\ckeditor\CKEditor;
use iutbay\yii2kcfinder\KCFinderInputWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    	<div class="row">
    		<div class="col-xs-8">
    			<div class="well shadow-0">
                    <h4>Основной контент</h4>
                    <hr class="blue-border-color">
                    
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= 
                        $form->field($model, 'page_body')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'Standard'
                        ]) 
                    ?>  

                </div>
    		</div>
    		<div class="col-xs-4">
    			<div class="well shadow-0">
                    <h4>Публикация</h4>
                    <hr class="blue-border-color">

                    <div class="form-group">

                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-success']) ?>
                    
                    </div>

                    <?= $form->field($model, 'status')->checkBox(['label' => 'Опубликовать']) ?>

                </div>
                <div class="well shadow-0">
                    <h4>Стиль</h4>
                    <hr class="blue-border-color">

                    <?= $form->field($model, 'layout')->dropDownList($model->getLayouts()) ?>

                </div>
    		</div>
    	</div>

    <?php ActiveForm::end(); ?>

</div>
