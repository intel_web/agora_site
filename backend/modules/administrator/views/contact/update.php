<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\Contact */

$this->title = 'Обновить контакты: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
