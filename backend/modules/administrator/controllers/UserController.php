<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use common\models\AuthAssignment;
use common\models\Administrator;
use common\models\Author;
use common\models\Editor;
use common\models\Comment;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\models\SignupForm;
use app\modules\administrator\controllers\DefaultController;
use yii\data\SqlDataProvider;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends DefaultController
{
	 /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


	public function actionProfile($id)
    {
        $model = $this->findModel($id);
		$profile = AuthAssignment::find()->where(['user_id'=>$id])->one();
			
		switch($profile->item_name){
			case 'administrator':
				$this->redirect(['administrator/view', 'id' => $id]);
				break;
			case 'editor':
				$this->redirect(['editor/view', 'id' => $id]);
				break;
			case 'author':
				$this->redirect(['author/view', 'id' => $id]);
				break;
		}
    }
	
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	$model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model=$this->findModel($id);
        if($model->id==Yii::$app->user->identity->id){
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить свой профайл');
        }else{
            $model->delete();
        }

        if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$id))
        {
            $folders=glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$id."/*");
            if(!empty($folders)){
                foreach($folders as $folder){
                    $files=glob($folder."/*.*");
                    foreach ($files as $f) {
                        unlink($f);
                    }
                    rmdir($folder);
                }
            }
           rmdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$id);
        }
        //delete all user files and folders in contests folder
        $contest_folders=glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath'."/*"));
        $user_folders=array();
        $files=array();
        if(!empty($contest_folders)){
            foreach ($contest_folders as $folders){
                $tmp=glob($folders."/".$id);
                if(!empty($tmp)) 
                {
                    array_push($user_folders, $tmp[0]);
                }
            }
            if(!empty($user_folders))
            {
                foreach ($user_folders as $user_folder){
                    $user_files=glob($user_folder."/*");
                    if(!empty($user_files)){
                        foreach($user_files as $f){
                            unlink($f);
                        }
                    }
                    rmdir($user_folder);
                }
            }
        }
	   return $this->redirect(['index']); 
    } 


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionSignup()
    {
		$model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
				$userRole = Yii::$app->authManager->getRole($model->role_name);
				Yii::$app->authManager->assign($userRole, $user->getId());

				switch($model->role_name){
					case 'administrator':
						$this->redirect(['administrator/create']);
						break;
					case 'editor':
						$this->redirect(['editor/create']);
						break;
					case 'author':
                        mkdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$user->getId()."/");
                        $this->redirect(['author/create']);
						break;
					}
					return;
				}	
            }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	
    protected function getAdministratorById($id) 
    {
        if (($model = Administrator::findOne($id)) !== null){
            return $model;
        }else{
        	throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     

    protected function getEditorById($id) {
        if (($model = Editor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getAuthorById($id) 
    {
        if(($model = Author::findOne($id)) !== null) {
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
		
}
