<?php

namespace app\modules\administrator\controllers;

use Yii;

use common\models\Post;
use common\models\User;
use common\models\Page;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\modules\administrator\models\Message;
use yii\db\Query;

use yii\helpers\ArrayHelper;
use linslin\yii2\curl;

class DefaultController extends Controller {
	public function behaviors() {
		return [
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['administrator'],
                    ],
                ],
            ],
        ];
	}
    public function actionIndex() {
        $post = new Post();
        $latestPosts = Post::find()
            ->limit(3)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $users = User::find()->count();
        $posts = Post::find()->count();
        $pages = Page::find()->count();
        return $this->render('index', [
            'post' => $post,
            'latestPosts' => $latestPosts,
            'users' => $users,
            'posts' => $posts,
            'pages' => $pages,
        ]);
    }
}
