<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\Contest;
use common\models\ContestComment;
use common\models\ContestArticle;
use common\models\ContestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * ContestController implements the CRUD actions for Contest model.
 */
class ContestController extends DefaultController
{
    /**
     * Lists all Contest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(mkdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->id."/")){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionContestArticle($id)
    {
        return $this->redirect(Url::to(['contest-article/index','id'=>$id]));
    }

    /**
     * Deletes an existing Contest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$id)){

            $folders = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$id."/*");
            if(!empty($folders)){
                foreach ($folders as $folder) {
                    $files=glob($folder."/*");
                    if(!empty($files)){
                        foreach ($files as $file) {
                            unlink($file);
                        }
                    }
                    rmdir($folder);
                }
            }
            rmdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$id);
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Contest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findContestArticles($contest_id)
    {
        $contest_articles=ContestArticle::find()->where(['contest_id'=>$contest_id])->all();
        return $contest_articles;
    }
}
