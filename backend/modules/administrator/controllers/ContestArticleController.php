<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\ContestArticle;
use common\models\ContestArticleSearch;
use common\models\Contest;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;


/**
 * ContestArticleController implements the CRUD actions for ContestArticle model.
 */
class ContestArticleController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Lists all ContestArticle models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new ContestArticleSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => ContestArticle::find()->where(['contest_id'=> $id])
        ]);

        $model=$this->findContestById($id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single ContestArticle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionContestArticleComment($id)
    {
        return $this->redirect(Url::to(['contest-comment/index','id'=>$id]));
    }


    /**
     * Creates a new ContestArticle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ContestArticle();
        $model->contest_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$model->user_id))
            {
                mkdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$model->user_id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ContestArticle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContestArticle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);

         if(file_exists(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$model->user_id))
         {
            $files = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$model->user_id."/*.*");
            if(!empty($files)){
                foreach ($files as $f){
                    unlink($f);
                }
            }
            rmdir(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id."/".$model->user_id);
         }
         $model->delete();

        return $this->redirect(['index','id'=>$model->contest_id]);
    }

    /**
     * Finds the ContestArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContestArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContestArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findContestById($id)
    {
        return $model=Contest::findOne($id);
    }
}
