<?php

namespace backend\modules\editor\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class MailingForm extends Model
{
    public $subject;
    public $body;
    public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // subject and body are required
            [['subject', 'body', 'category'], 'required'],
            ['category', 'each', 'rule' => ['string']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject' => 'Тема',
            'body' => 'Текст',
            'category' => 'Категории',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['adminEmail'] => "IT&TRANSPORT"])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
}
