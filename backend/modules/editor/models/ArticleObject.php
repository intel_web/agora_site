<?php

namespace app\modules\editor\models;

use Yii;
use yii\base\Model;

/**
 * ArticleObject is the model behind the Article model (for json).
 */
class ArticleObject extends Model
{
    public $article;
    public $authors;
    public $comments;
    public $uploadedFiles;
}
