<?php

namespace app\modules\editor\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Inbox extends Model
{
    public $consideration;
    public $approved;
    public $published;
    public $digests;
    public $paided;

    public $contestArticleConsideration;
    public $contestArticleApproved;
    public $contests;
}
