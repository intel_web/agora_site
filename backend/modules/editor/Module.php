<?php

namespace app\modules\editor;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\editor\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
