<?php

namespace app\modules\editor\controllers;

use Yii;
use common\models\Article;
use common\models\ArticleAuthor;
use common\models\Comment;
use app\modules\editor\models\ArticleObject;

class ArticleController extends \app\modules\editor\controllers\DefaultController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    //template actions
    public function actionArticleList($status) {
    	$title = "";
    	switch ($status) {
    		case 2: {
    			$title = "Статьи на утверждении";
    		}break;
    		case 4: {
    			$title = "Статьи принятые к публикации";
    		}break;
    		case 5: {
    			$title = "Опубликованные статьи";
    		}break;
            case 'paided': {
                $title = "Оплаченные";
            }break;
    	}
    	return $this->renderPartial('articles', [
    		'title' => $title
    	]);
    }

    public function actionArticle($status, $id) {
        if (Article::find()->where('id = :id AND status = :status', [ ':id' => $id, ':status' => $status] )->exists()) {
            return $this->renderPartial('article');
        } else {
            return $this->renderPartial('error', ['message' => 'Статья не найдена']);
        }
    }

    //crud actions
    public function actionGetArticleList() {
    	$params = json_decode(file_get_contents('php://input'));
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$articles = [];
    	if ($params->data->articleStatus) {
            switch ($params->data->articleStatus) {
                case 'paided':{
                    $articles = Article::find()
                        ->where('article.status = :status AND payment_status = :payment_status', [':status' => 4, ':payment_status' => 1])
                        ->with('science')
                        ->with('author')
                        ->with('digest')
                        ->asArray()
                        ->all();
                }break;
                case Yii::$app->params['published']:{
                    $articles = Article::find()
                        //->where('article.status = :status AND payment_status = :payment_status', [':status' => $params->data->articleStatus, ':payment_status' => 1])
                        ->where('article.status = :status', [':status' => $params->data->articleStatus])
                        ->with('science')
                        ->with('author')
                        ->with('digest')
                        ->asArray()
                        ->all();
                }break;
                case Yii::$app->params['review']: {
                    $articles = Article::find()
                        ->where('article.status = :status', [':status' => $params->data->articleStatus])
                        ->with('science')
                        ->with('author')
                        ->with('digest')
                        ->asArray()
                        ->all();
                }break;
                default: {
                    $articles = Article::find()
                        ->where('article.status = :status AND payment_status = :payment_status', [':status' => $params->data->articleStatus, ':payment_status' => 0])
                        ->with('science')
                        ->with('author')
                        ->with('digest')
                        ->asArray()
                        ->all();
                }break;
            }
    	}

    	return $articles;
    }

    public function actionGetArticle() {
    	$params = json_decode(file_get_contents('php://input'));
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	if ($params->data->articleId && $params->data->articleStatus) {
    		$articleObject = new ArticleObject();
    		$article = Article::find()
    			->where('article.id = :articleId AND article.status = :status', [':articleId' => $params->data->articleId, ':status' => $params->data->articleStatus])
                ->with(['comments' => function($query) {
                    $query->with('user')
                        ->orderBy('created_at ASC');
                }])
                ->with('science')
                ->with('author')
                ->with(['articleAuthors' => function($query) {
                    $query->with('authorUser');
                }])
                ->asArray()
                ->one();
            if ($article) {
                $articleObject->article = $article;
                if (file_exists(Yii::getAlias('@uploads')."/articles/".$articleObject->article['author_id']."/")) {
                    if (file_exists(Yii::getAlias('@uploads')."/articles/".$articleObject->article['author_id']."/".$articleObject->article['id']."/")) {
                        $articleFile = glob(Yii::getAlias('@uploads')."/articles/".$articleObject->article['author_id']."/".$articleObject->article['id']."/".Yii::$app->params['articleFileName'].'.*');
                        if (!empty($articleFile)) {
                            $articleObject->uploadedFiles['article'] = $articleFile[0];
                        }
                        $firstReviewFile = glob(Yii::getAlias('@uploads')."/articles/".$articleObject->article['author_id']."/".$articleObject->article['id']."/".Yii::$app->params['firstReviewFileName'].'.*');
                        if (!empty($firstReviewFile)) {
                            $articleObject->uploadedFiles['firstReview'] = $firstReviewFile[0];
                        }
                        $secondReviewFile = glob(Yii::getAlias('@uploads')."/articles/".$articleObject->article['author_id']."/".$articleObject->article['id']."/".Yii::$app->params['secondReviewFileName'].'.*');
                        if (!empty($secondReviewFile)) {
                            $articleObject->uploadedFiles['secondReview'] = $secondReviewFile[0];
                        }
                        $expertOpinionFile = glob(Yii::getAlias('@uploads')."/articles/".$articleObject->article['author_id']."/".$articleObject->article['id']."/".Yii::$app->params['expertOpinionFileName'].'.*');
                        if (!empty($expertOpinionFile)) {
                            $articleObject->uploadedFiles['expertOpinion'] = $expertOpinionFile[0];
                        }
                    }
                }
                return $articleObject;
            }
        } else {
            return null;
        }
    }

    public function actionGetFreeArticles() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Article::find()
            ->where('digest_id IS NULL AND status = :status AND payment_status = 1', [':status' => Yii::$app->params['accepted']])
            ->all();
    }

    public function actionChangeArticleStatus() {
    	$params = json_decode(file_get_contents('php://input'));
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	if ($params->data->articleId && $params->data->articleStatus) {
    		$article = Article::findOne($params->data->articleId);
			$article->status = $params->data->articleStatus;
            if ($params->data->articleStatus == 4) {
                $article->payment_status = 1;
            } 
            //$article->payment_status = (($params->data->articleStatus == 4)) ? (1) : (0);//remove when payment will work
			$article->update();
    	}
        return $this->renderPartial('error', ['message' => 'Статья не найдена']);
    }

    public function actionCommentArticle() {
    	$params = json_decode(file_get_contents('php://input'));
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	if ($params->data->articleId && $params->data->comment) {
    		$comment = new Comment();
    		$comment->user_id = Yii::$app->user->identity->id;
    		$comment->comment = $params->data->comment;
    		$comment->article_id = $params->data->articleId;
            $comment->created_at = date("Y-m-d H:i:s");
    		$comment->save();
            //fuck
    		$comment = Comment::find()
    			->where('id = :comment_id', [':comment_id' => $comment->id])
    			->with('user')
    			->asArray()
    			->one();
    		return $comment;
    	}
    }

}
