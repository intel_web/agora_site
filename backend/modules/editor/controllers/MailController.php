<?php

namespace app\modules\editor\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use common\models\Author;
use common\models\MailList;
use common\models\MailHistory;
use backend\modules\editor\models\MailingForm;


class MailController extends Controller 
{
	public function behaviors() {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
	}
    
    public $layout = 'mail';

    public function actionIndex() 
    {
        $model = new MailingForm();

        /*Тут получать все email*/
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if(ArrayHelper::isIn(1, $model->category)) {
                $mailList = MailList::find()->all();
                foreach ($mailList as $iterator) {
                    (new MailHistory(['email' => $iterator->email, 
                        'mail_subject' => $model->subject,
                        'mail_body' => $model->body,
                    ]))->save();
                }
                Yii::$app->getSession()->setFlash('success', 'Очередь рассылки сформирована');
                return $this->redirect('index');
            } else {
                $mailList = [];
                //Перебираем массив category_id получнный с формы
                foreach ($model->category as $value) {
                    //Получаем массив всех адрессов принадлежащих категориям полученных с формы
                    $emailTmpArr = MailList::find()->select('email')->joinWith('mailListCategories')
                        ->where('mail_list_category.category_id =:category_id', [':category_id' => $value])->all();
                    //Если ответ на запрос не пуст то добавляем email адреса в массив для рассылки 
                    if(!empty($emailTmpArr)){
                        foreach ($emailTmpArr as $value) {
                            array_push($mailList, $value['email']);
                        }
                    }
                }
                //Перебираем подготовленный массив адресов для добавления в очередь отправки
                foreach ($mailList as $iterator) {
                    (new MailHistory(['email' => $iterator, 
                            'mail_subject' => $model->subject,
                            'mail_body' => $model->body,
                        ]))->save();
                }
                Yii::$app->getSession()->setFlash('success', 'Очередь рассылки сформирована');
            }
            return $this->redirect(Url::home());
        } else {
            return $this->render('mailing', [
                'model' => $model,
            ]);
        }
    }
}