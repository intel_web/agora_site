<?php

namespace app\modules\editor\controllers;

use Yii;
use common\models\ContestArticle;
use common\models\ContestComment;
use app\modules\editor\models\ArticleObject;

class ContestArticleController extends \app\modules\editor\controllers\DefaultController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    //template actions
    public function actionArticleList($status) {
    	$title = "";
    	switch ($status) {
    		case 2: {
    			$title = "Заявки на утверждении";
    		}break;
    		case 4: {
    			$title = "Заявки принятые";
    		}break;
    	}
    	return $this->renderPartial('articles', [
    		'title' => $title
    	]);
    }
    public function actionArticle($status, $id) {
        if (ContestArticle::find()->where('id = :id AND status = :status', [ ':id' => $id, ':status' => $status] )->exists()) {
            return $this->renderPartial('article');
        } else {
            return $this->renderPartial('error', ['message' => 'Заявка не найдена']);
        }
    }

    //crud actions
    public function actionGetContestArticleList() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $contestArticles = [];
        if ($params->data->articleStatus) {
            $contestArticles = ContestArticle::find()
                ->where('cn_article.status = :status', [':status' => $params->data->articleStatus])
                ->with('contest')
                ->with('contestNomination')
                ->with('user')
                ->with('userProfile')
                ->with('contestMasters')
                ->asArray()
                ->all();
        }

        return $contestArticles;
    }
    public function actionGetArticle() {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->articleId && $params->data->articleStatus) {
            $articleObject = new ArticleObject();
            $article = ContestArticle::find()
                ->where('cn_article.id = :articleId AND cn_article.status = :status', [':articleId' => $params->data->articleId, ':status' => $params->data->articleStatus])
                ->with('userProfile')
                ->with('contestNomination')
                ->with(['contestComments' => function($query) {
                    $query->with('user')->orderBy('created_at ASC');
                }])
                ->with('contestMasters')
                ->with('contest')
                ->asArray()
                ->one();
            if ($article) {
                $articleObject->article = $article;
                if (file_exists(Yii::getAlias('@uploads')."/contests/".$article['contest_id']."/")) {
                    $articleFile = glob(Yii::getAlias('@uploads')."/contests/".$article['contest_id']."/".$article['id'].'.*');
                    if (!empty($articleFile)) {
                        $articleObject->uploadedFiles['article'] = $articleFile[0];
                    }
                }
                return $articleObject;
            }
        } else {
            return null;
        }
    }
    public function actionChangeArticleStatus() {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->articleId && $params->data->articleStatus) {
            $article = ContestArticle::findOne($params->data->articleId);
            if (!empty($article)) {
                $article->status = $params->data->articleStatus;
                if (($article->status == Yii::$app->params['review']) || ($article->status == Yii::$app->params['returned'])) {
                    $article->score = NULL; 
                }
                $article->update();
            } else {
                throw new \Exception('Заявка не найдена!');
            }
        }
    }
    public function actionUpdateArticleScore() {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->articleId) {
            $article = ContestArticle::find()
                ->where('status = :status AND id = :id', [':status' => Yii::$app->params['accepted'], ':id' => $params->data->articleId])
                ->one();
            if (!empty($article)) {
                $article->score = ($params->data->articleScore) ? ($params->data->articleScore) : (NULL);
                if (!$article->update()) throw new \Exception('Не удалось сохранить место!');
            } else {
                throw new \Exception('Работа не найдена или она всё еще на рассмотрении!');
            }
        }
    }
    public function actionCommentArticle() {
        $params = json_decode(file_get_contents('php://input'));
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->articleId && $params->data->comment) {
            $comment = new ContestComment();
            $comment->user_id = Yii::$app->user->identity->id;
            $comment->comment = $params->data->comment;
            $comment->cn_article_id = $params->data->articleId;
            $comment->created_at = date("Y-m-d H:i:s");
            $comment->save();
            //fuck
            $comment = ContestComment::find()
                ->where('id = :comment_id', [':comment_id' => $comment->id])
                ->with('user')
                ->asArray()
                ->one();
            return $comment;
        }
    }

}
