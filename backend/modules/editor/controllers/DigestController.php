<?php

namespace app\modules\editor\controllers;

use Yii;
use common\models\Article;
use common\models\ArticleAuthor;
use common\models\Digest;
use app\modules\editor\models\ArticleObject;
use common\helpers\Translit;

class DigestController extends \app\modules\editor\controllers\DefaultController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    //template actions
    public function actionDigestList() {
    	return $this->renderPartial('digests');
    }

    public function actionDigestForm() {
        $digest = new Digest();
    	return $this->renderPartial('_digest', [
            'model' => $digest
        ]);
    }


    //CRUD actions
    public function actionGetDigestList() {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$digests = [];
    	$digests = Digest::find()->all();

    	return $digests;
    }
    public function actionGetDigest($id) {
        $digest = Digest::find()
            ->where('id = :id', [':id' => $id])
            ->with('articles')
            ->asArray()
            ->one();
        //if ($digest) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $digest;
        /*} else {
            return $this->renderPartial('error', ['message' => 'Сборник не найден']);
        }*/
    }
    public function actionCreateDigest() {
        $digest = new Digest();
        if (Yii::$app->request->post()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //load digest
            $digest->load(Yii::$app->request->post(), '');
            $digest->is_published = ($digest->is_published) ? (1) : (0);
            $digest->created_at = ($digest->is_published) ? (date("Y-m-d")) : (null);
            if ($digest->validate()) {
                if ($digest->save()) {
                    if (isset($_FILES) && array_key_exists('file', $_FILES)) {
                        //save digest file
                        $uploadfile = Yii::getAlias('@uploads').'/digests/'.basename(Translit::transliterationWithSpace($digest->name)).'.pdf';
                        move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
                    }
                }
            } else {
                throw new \Exception($this->getModelErrorsToString($digest->errors));
            }
        } else {
            return $this->renderPartial('create-digest', [
                'model' => $digest
            ]);
        }
    }
    public function actionUpdateDigest($id) {
        $digest = $this->findModel($id);
        if ($digest) {
            if (Yii::$app->request->post()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //load digest
                $digest->name = Yii::$app->request->post()['name'];
                $digest->is_published = (Yii::$app->request->post()['is_published'] == "true") ? (1) : (0);
                //if digest published -> all digest articles must be published to
                ($digest->is_published) ? (Article::updateAll(['status' => Yii::$app->params['published']], ['digest_id' => $digest->id])) : (Article::updateAll(['status' => Yii::$app->params['accepted']], ['digest_id' => $digest->id]));
                $digest->created_at = ($digest->is_published) ? (date("Y-m-d")) : (null);
                if ($digest->validate()) {
                    if ($digest->save()) {
                    	//if digest updated change its article status to published
                    	Article::updateAll(['status' => ($digest->is_published) ? (Yii::$app->params['published']) : (Yii::$app->params['accepted'])], ['digest_id' => $digest->id]);
                        if (isset($_FILES) && array_key_exists('file', $_FILES)) {
                            //save digest file
                            $uploadfile = Yii::getAlias('@uploads').'/digests/'.basename(Translit::transliterationWithSpace($digest->name)).'.pdf';
                            move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
                        }
                    }
                } else {
                    throw new \Exception($this->getModelErrorsToString($digest->errors));
                }
            } else {
                return $this->renderPartial('update-digest', [
                    'model' => $digest
                ]);
            }
        } else {
            return $this->renderPartial('error', ['message' => 'Сборник не найден']);
        }
    }
    public function actionUpdateDigestArticles($id) {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($params->data->freeArticles) && isset($params->data->digestArticles)) {
            $freeArticles = $params->data->freeArticles;
            $digestArticles = $params->data->digestArticles;
            $digest = $this->findModel($id);
            if ($digest) {
                Article::updateAll(['digest_id' => NULL, 'status' => Yii::$app->params['accepted']], ['id' => array_map(function($o) { return $o->id; }, $freeArticles)]);
                Article::updateAll(['digest_id' => $id, 'status' => ($digest->is_published) ? (Yii::$app->params['published']) : (Yii::$app->params['accepted'])], ['id' => array_map(function($o) { return $o->id; }, $digestArticles)]);
            } else {
                throw new \Exception('Сборник не найден!');
            }
        } else {
            throw new \Exception('Ошибка при передаче данных!');
        }
    }
    public function actionRemoveDigest() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->digestId) {
            $digest = $this->findModel($params->data->digestId);
            if (file_exists(Yii::getAlias('@uploads')."/digests/".Translit::transliterationWithSpace($digest->name).'.pdf')) {
                unlink(Yii::getAlias('@uploads')."/digests/".Translit::transliterationWithSpace($digest->name).'.pdf');
            }
            Article::updateAll(['digest_id' => NULL, 'status' => Yii::$app->params['accepted']], ['digest_id'=>[$params->data->digestId]]);
            Digest::deleteAll('id = :id', [':id' => $params->data->digestId]);
        }
    }

    public function actionGetDigestArticlesTemplate($id) {
        $digest = $this->findModel($id);
        if ($this->findModel($id)) {
            return $this->renderPartial('digest-articles-template');
        } else {
            return $this->renderPartial('error', ['message' => 'Сборник не найден']);
        }
    }

    protected function findModel($id) {
        $model = Digest::findOne($id);
        return $model;
    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

}
