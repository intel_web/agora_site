<?php

namespace app\modules\editor\controllers;

use Yii;
use common\models\Contest;
use common\models\ContestArticle;
use common\models\ContestComment;
use app\modules\editor\models\ArticleObject;
use yii\helpers\ArrayHelper;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Style\Alignment;

use common\models\Diplom;
use common\helpers\Translit;

class ContestController extends \app\modules\editor\controllers\DefaultController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    //template actions
    public function actionContestList() {
    	return $this->renderPartial('contests');
    }

    public function actionContestForm() {
        $contest = new Contest();
        return $this->renderPartial('_contest', [
            'model' => $contest
            ]);
    }


    //CRUD actions
    public function actionGetContestList() {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$contests = [];
    	$contests = Contest::find()->all();

    	return $contests;
    }
    public function actionGetContest($id) {
        $contest = Contest::find()
        ->where('id = :id', [':id' => $id])
        ->with('cnArticles')
        ->asArray()
        ->one();
        //if ($digest) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $contest;
        /*} else {
            return $this->renderPartial('error', ['message' => 'Сборник не найден']);
        }*/
    }
    public function actionCreateContest() {
        $contest = new Contest();
        if (Yii::$app->request->post()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //load contest
            $contest->load(Yii::$app->request->post(), '');
            if ($contest->validate()) {
                if ($contest->save()) {
                    //create folder
                    if (!mkdir(Yii::getAlias('@uploads').'/contests/'.$contest->id, 0777, true)) {
                        throw new \Exception('Не удалось создать директорию конкурса');
                    }
                    if (!mkdir(Yii::getAlias('@uploads').'/contests/'.$contest->id.'/diploma/', 0777, true)) {
                        throw new \Exception('Не удалось создать директорию для дипломов');
                    }
                }
            } else {
                throw new \Exception($this->getModelErrorsToString($contest->errors));
            }
        } else {
            return $this->renderPartial('create-contest', [
                'model' => $contest
                ]);
        }
    }
    public function actionUpdateContest($id) {
        $contest = $this->findModel($id);
        if ($contest) {
            if (Yii::$app->request->post()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //load digest
                $contest->name = Yii::$app->request->post()['name'];
                $contest->start_date = Yii::$app->request->post()['start_date'];
                $contest->end_date = Yii::$app->request->post()['end_date'];
                $contest->requirements = Yii::$app->request->post()['requirements'];
                if ($contest->validate()) {
                    $contest->save();
                } else {
                    throw new \Exception($this->getModelErrorsToString($contest->errors));
                }
            } else {
                return $this->renderPartial('update-contest', [
                    'model' => $contest
                    ]);
            }
        } else {
            return $this->renderPartial('error', ['message' => 'Конкурс не найден']);
        }
    }
    public function actionUpdateDigestArticles($id) {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($params->data->freeArticles) && isset($params->data->digestArticles)) {
            $freeArticles = $params->data->freeArticles;
            $digestArticles = $params->data->digestArticles;
            $digest = $this->findModel($id);
            if ($digest) {
                Article::updateAll(['digest_id' => NULL], ['id' => array_map(function($o) { return $o->id; }, $freeArticles)]);
                Article::updateAll(['digest_id' => $id, 'status' => ($digest->is_published) ? (Yii::$app->params['published']) : (Yii::$app->params['accepted'])], ['id' => array_map(function($o) { return $o->id; }, $digestArticles)]);
            } else {
                throw new \Exception('Сбоник не найден!');
            }
        } else {
            throw new \Exception('Ошибка при передаче данных!');
        }
    }
    public function actionRemoveContest() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->contestId) {
            //delete contest if exists
            $contest = Contest::findOne($params->data->contestId);
            if ($contest != null) {
                if ($contest->delete() != false) {
                    if (file_exists(Yii::getAlias('@uploads')."/contests/".$params->data->contestId)) {
                        //remove all contest docs
                        $this->removeDirectory(Yii::getAlias('@uploads')."/contests/".$params->data->contestId);
                    }
                }
            }
        }
    }

    public function actionGetParticipantsSheet() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = json_decode(file_get_contents('php://input'));
        if ($params->data->contestId) {
            $articles = ContestArticle::find()
            ->where('contest_id = :contest_id AND status = :status', [':contest_id' => $params->data->contestId, ':status' => Yii::$app->params['accepted']])
            ->with('contestNomination')
            ->with('userProfile')
            ->with('contestMasters')
            ->orderBy('score ASC, cn_nomination_id ASC')
            ->asArray()
            ->all();
            if (count($articles)) {
                //get contes info
                $contest = Contest::find()
                ->where('id = :id', [':id' => $params->data->contestId])
                ->one();

                $wrapper = [];
                foreach ($articles as $article) {
                    $wrapper[$article['contestNomination']['nomination_name']][] = $article;
                }
                $path = Yii::getAlias('@uploads').'/contests/'.$params->data->contestId.'/participants-sheet.docx';
                
                $phpWord = new PHPWord();
                $phpWord->setDefaultFontName('Times New Roman');
                $phpWord->setDefaultFontSize(12);

                $section = $phpWord->addSection([
                    'marginLeft' => floor(30 * 56.7),
                    'marginRight' => floor(15 * 56.7),
                    'marginTop' => floor(20 * 56.7),
                    'marginBottom' => floor(15 * 56.7),
                    ]);

                $header = array('size' => 16, 'bold' => true);
                $section->addText(htmlspecialchars('Участники конкурса «'.$contest->name.'»', ENT_COMPAT, 'UTF-8'), $header);
                $styleTable = array('borderSize' => 6, 'width' => 50 * 50, 'unit' => 'pct');
                $styleCell = array('valign' => 'center');
                $tableHeaderFontStyle = array('italic' => true);
                $fontStyle = array('bold' => true);
                $phpWord->addTableStyle('Fancy Table', $styleTable);
                
                $table = $section->addTable(array('width' => 5000, 'unit' => 'pct', 'borderSize' => 6));
                $table->addRow();

                //table header
                $table->addCell(250, $styleCell)->addText('ФИО участника', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                $table->addCell(2000, $styleCell)->addText('Тема научной работы', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                $table->addCell(10, $styleCell)->addText('Место', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                $table->addCell(250, $styleCell)->addText('ФИО научного руководителя', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));

                $nominationCounter = 1;
                foreach ($wrapper as $key => $nomination) {
                    $table->addRow();
                    $table->addCell(1750, array('gridSpan' => 4))->addText($nominationCounter.'. '.$key, $fontStyle, array('align' => 'center', 'spaceAfter' => 0));
                    foreach ($nomination as $article) {
                        $table->addRow();
                        $table->addCell(2000)->addText($article['author'], [], array('spaceAfter' => 0, 'marginLeft' => floor(5 * 56.7)));
                        //$table->addCell(2000)->addText($article['userProfile']['surname'].' '.$article['userProfile']['name'].' '.$article['userProfile']['lastname'], [], array('spaceAfter' => 0, 'marginLeft' => floor(5 * 56.7)));
                        $table->addCell(2000)->addText($article['title'], [], array('spaceAfter' => 0));
                        $table->addCell(2000)->addText(($article['score']) ? ($article['score']) : ('не присвоено'), $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                        $cn_master_str = '';
                        foreach($article['contestMasters'] as $cn_master) {
                            $cn_master_str .= $cn_master['surname'] ." ". $cn_master['name'] ." ". $cn_master['lastname'] ."; ";
                        }
                        $table->addCell(2000)->addText($cn_master_str, [], array('align' => 'center', 'spaceAfter' => 0));
                        
                    }
                    $nominationCounter++;
                }

                //save file
                $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
                $objWriter->save($path);
                if (file_exists($path)) {
                    return $path;
                } else {
                    throw new \Exception('Не удалось сохранить файл ведомости');
                }
            } else {
                throw new \Exception('Нет участников.');
            }
        } else {
            throw new \Exception('Не удалось передать данные!');
        }
    }

    public function actionGetSheet() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = json_decode(file_get_contents('php://input'));
        if ($params->data->contestId) {
            $articles = ContestArticle::find()
            ->where('contest_id = :contest_id AND score in (1,2,3) AND status = :status', [':contest_id' => $params->data->contestId, 'status' => Yii::$app->params['accepted']])
            ->with('contestNomination')
            ->with('userProfile')
            ->with('contestMasters')
            ->orderBy('score ASC, cn_nomination_id ASC')
            ->asArray()
            ->all();
            if (count($articles)) {
                //get contes info
                $contest = Contest::find()
                ->where('id = :id', [':id' => $params->data->contestId])
                ->one();

                $wrapper = [];
                foreach ($articles as $article) {
                    $wrapper[$article['contestNomination']['nomination_name']][] = $article;
                }
                $path = Yii::getAlias('@uploads').'/contests/'.$params->data->contestId.'/winners-sheet.docx';
                
                $phpWord = new PHPWord();
                $phpWord->setDefaultFontName('Times New Roman');
                $phpWord->setDefaultFontSize(12);

                $section = $phpWord->addSection([
                    'marginLeft' => floor(30 * 56.7),
                    'marginRight' => floor(15 * 56.7),
                    'marginTop' => floor(20 * 56.7),
                    'marginBottom' => floor(15 * 56.7),
                    ]);

                $header = array('size' => 16, 'bold' => true);
                $section->addText(htmlspecialchars('Победители конкурса «'.$contest->name.'»', ENT_COMPAT, 'UTF-8'), $header);
                $styleTable = array('borderSize' => 6, 'width' => 50 * 50, 'unit' => 'pct');
                $styleCell = array('valign' => 'center');
                $tableHeaderFontStyle = array('italic' => true);
                $fontStyle = array('bold' => true);
                $phpWord->addTableStyle('Fancy Table', $styleTable);
                
                $table = $section->addTable(array('width' => 5000, 'unit' => 'pct', 'borderSize' => 6));
                $table->addRow();

                //table header
                $table->addCell(250, $styleCell)->addText('ФИО участника', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                $table->addCell(2000, $styleCell)->addText('Тема научной работы', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                $table->addCell(10, $styleCell)->addText('Место', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                $table->addCell(250, $styleCell)->addText('ФИО научного руководителя', $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));

                $nominationCounter = 1;
                foreach ($wrapper as $key => $nomination) {
                    $table->addRow();
                    $table->addCell(1750, array('gridSpan' => 4))->addText($nominationCounter.'. '.$key, $fontStyle, array('align' => 'center', 'spaceAfter' => 0));
                    foreach ($nomination as $article) {
                        $table->addRow();
                        $table->addCell(2000)->addText($article['author'], [], array('spaceAfter' => 0, 'marginLeft' => floor(5 * 56.7)));
                        //$table->addCell(2000)->addText($article['userProfile']['surname'].' '.$article['userProfile']['name'].' '.$article['userProfile']['lastname'], [], array('spaceAfter' => 0, 'marginLeft' => floor(5 * 56.7)));
                        $table->addCell(2000)->addText($article['title'], [], array('spaceAfter' => 0));
                        $table->addCell(2000)->addText($article['score'], $tableHeaderFontStyle, array('align' => 'center', 'spaceAfter' => 0));
                        //$table->addCell(2000)->addText($article['master'], [], array('align' => 'center', 'spaceAfter' => 0));
                        $cn_master_str = '';
                        foreach($article['contestMasters'] as $cn_master) {
                            $cn_master_str .= $cn_master['surname'] ." ". $cn_master['name'] ." ". $cn_master['lastname'] ."; ";
                        }
                        $table->addCell(2000)->addText($cn_master_str, [], array('align' => 'center', 'spaceAfter' => 0));
                    }
                    $nominationCounter++;
                }

                //save file
                $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
                $objWriter->save($path);
                if (file_exists($path)) {
                    return $path;
                } else {
                    throw new \Exception('Не удалось сохранить файл ведомости');
                }
            } else {
                throw new \Exception('Нет победителей.');
            }
        } else {
            throw new \Exception('Не удалось передать данные!');
        }
    }

    public function actionGetDigestArticlesTemplate($id) {
        $digest = $this->findModel($id);
        if ($this->findModel($id)) {
            return $this->renderPartial('digest-articles-template');
        } else {
            return $this->renderPartial('error', ['message' => 'Сборник не найден']);
        }
    }

    protected function findModel($id) {
        $model = Contest::findOne($id);
        return $model;
    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    protected function removeDirectory($path) {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }

    public function actionGetDiplom() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = json_decode(file_get_contents('php://input'));
        
        if($params->data->contestId) {
            $cn_info_array = ContestArticle::find()
            ->where('contest_id = :contest_id AND score in (1,2,3) AND status = :status', 
                [':contest_id' => $params->data->contestId, 'status' => Yii::$app->params['accepted']])
            ->with('contestNomination')
            ->with('userProfile')
            ->with('contestMasters')
            ->with('contest')
            ->with('contestMasters.academicDegree')
            ->orderBy('score ASC, cn_nomination_id ASC')
            ->asArray()
            ->all();     

            if(empty($cn_info_array)) {
                Yii::$app->getSession()->setFlash('error', 'Список участников конкурса пуст');
                return;
            } else {
                $diplomPath = Yii::getAlias('@uploads').'/contests/'.$params->data->contestId.'/diploma/';
                if(!file_exists($diplomPath)) {
                    Yii::$app->getSession()->setFlash('error', 'Не создана папка для дипломов!');
                    return;
                }
                foreach ($cn_info_array as $cn_info) 
                {
                    $content = $this->prepareInfo($cn_info);
                    $author = Translit::transliterationWithSpace($content->author);

                    $templateProcessor = new TemplateProcessor(Yii::getAlias('@uploads').'/templates/diplomaTemplate.docx');
                    $templateProcessor->setValue(['studentName', 'masterName', 'organization', 'score', 'nomination', 'contestName'], 
                        [htmlspecialchars($content->author, ENT_COMPAT, 'UTF-8'),
                        htmlspecialchars($content->contestMasters, ENT_COMPAT, 'UTF-8'), 
                        htmlspecialchars($content->organization, ENT_COMPAT, 'UTF-8'),
                        htmlspecialchars($content->score, ENT_COMPAT, 'UTF-8'),
                        htmlspecialchars($content->contestNomination, ENT_COMPAT, 'UTF-8'),
                        htmlspecialchars($content->contestName, ENT_COMPAT, 'UTF-8')]);
                    //Сохраняем с уникальным именем файл диплома
                    $templateProcessor->saveAs($this->getDiplomFileName($diplomPath . $author));             
                }
            }
        }
    }

    protected function prepareInfo($cn_info) 
    {
        $model = new Diplom();
        $model->makeDiplom($cn_info);
        return $model;
    }

    /*Метод проверяет существует ли уже файл диплома с таким названием, если да то подставляет значение счетчика в конце названия файла и проверяет заново*/
    protected function getDiplomFileName($diplomFileName){
        $exists = true;
        $count = 0;
        $tmp = '';
        $tmp = $diplomFileName;
        /*Бесконечный(на самом деле нет) цикл подбора уникального имени файла*/
        while($exists){
            if(file_exists($tmp.'.docx')){
                $tmp = $diplomFileName.'_'.$count;
                $count++;
            }else{
                $exists = false;
            }
        }
        return $tmp.'.docx';
    }    
}
