<?php

namespace app\modules\editor\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\Article;
use common\models\Digest;
use common\models\Contest;
use common\models\ContestArticle;
use app\modules\editor\models\Inbox;
use backend\modules\editor\models\MassMailingForm;
use yii\helpers\Url;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Style\Alignment;

class DefaultController extends Controller {
	public function behaviors() {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['editor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
	}

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionInbox() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $inbox = new Inbox();
        $inbox->consideration = Article::find()
            ->where('status = :status', [':status' => Yii::$app->params['review']])
            ->count();
        $inbox->approved = Article::find()
            ->where('status = :status AND payment_status = 0', [':status' => Yii::$app->params['accepted']])
            ->count();
        $inbox->published = Article::find()
            ->where('status = :status', [':status' => Yii::$app->params['published']])
            ->count();
        $inbox->paided = Article::find()
            ->where('status = :status AND payment_status = 1', [':status' => Yii::$app->params['accepted']])
            ->count();
        $inbox->digests = Digest::find()
            ->count();
        $inbox->contestArticleConsideration = ContestArticle::find()
            ->where('status = :status', [':status' => Yii::$app->params['review']])
            ->count();
        $inbox->contestArticleApproved = ContestArticle::find()
            ->where('status = :status', [':status' => Yii::$app->params['accepted']])
            ->count();
        $inbox->contests = Contest::find()
            ->count();
        return $inbox;
    }
}