<div class="article">
	<h2>Профиль статьи</h2>
	<div class="well article-profile">
		<div class="row">
			<div class="col-md-12 text-left">
				<blockquote><h2>{{articleObject.article.title}}</h2></blockquote>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<label>
						Статус публикации
						<select class="form-control" ng-model="articleObject.article.status" convert-to-number ng-change="updateArticleStatus()">
					    	<option value="{{2}}">На рассмотрение</option>
					    	<option value="{{3}}">На доработку</option>
					    	<option value="{{4}}">Принять к публикации</option>
					    	<option value="{{5}}">Опубликовать</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<span class="label label-primary article-tag">{{"УДК: " + ((articleObject.article.udk_index) ? (articleObject.article.udk_index) : ("не задан"))}}</span>
					<span class="label label-primary article-tag">{{"Код ГРНТИ: " + ((articleObject.article.grnti) ? (articleObject.article.grnti) : ("не задан"))}}</span>
					<span class="label label-primary article-tag">{{"Количество страниц: " + ((articleObject.article.pages) ? (articleObject.article.pages) : ("не задано"))}}</span>
				</div>
			</div>
			<div class="row" style="margin:20px 0 20px 0">
				<div class="col-xs-12">
					<strong><span>Прикрепленные материалы: </span></strong>
					{{(articleObject.uploadedFiles) ? ("") : ("нет материалов")}}
					<a download target="_blank" class="btn btn-primary btn-xs" style="margin:0 5px 0 5px" ng-repeat="(key, value) in articleObject.uploadedFiles" href="{{value}}" role="button">
	  					{{(key == 'article') ? ("Статья") : ("")}}
						{{(key == 'firstReview') ? ("Рецензия #1") : ("")}}
						{{(key == 'secondReview') ? ("Рецензия #2") : ("")}}
						{{(key == 'expertOpinion') ? ("Экспертное заключение") : ("")}}
					</a>
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<strong><span>Научное направление: </span></strong>
					{{((articleObject.article.science.science_name) ? (articleObject.article.science.science_name) : ("не задано"))}}
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<strong><span>Автор: </span></strong>
					{{articleObject.article.author.surname + ' ' + articleObject.article.author.name + ' ' + articleObject.article.author.lastname}}
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<strong><span>Организации: </span></strong>
					<ul class="coauthors-list">
						<li ng-repeat="organization in articleObject.article.organization.split(';') track by $index">
							{{organization}}
						</li>
					</ul>
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<strong><span>Соавторы: </span></strong>
					<ul class="coauthors-list">
						<li ng-repeat="author in articleObject.article.coauthor.split(';') track by $index">
							{{author}}
						</li>
					</ul>
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<strong><span>Аннотация (рус): </span></strong>
					{{(articleObject.article.rus_annotation) ? (articleObject.article.rus_annotation) : ('не задана')}}
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-md-12">
					<strong><span>Аннотация (eng): </span></strong>
					{{(articleObject.article.eng_annotation) ? (articleObject.article.eng_annotation) : ('не задана')}}
				</div>
			</div>
		</div>
	</div>
	<!--Comments section-->
	<div class="well">
		<h3>Комментарии</h3>
		<div class="row">
			<div class="col-md-10">
				<textarea class="form-control comment-field" rows="2" ng-model="currentComment"></textarea>
			</div>
			<div class="col-md-2">
				<button type="button" class="btn btn-primary" ng-click="sendComment()" ng-disabled="(currentComment) ? (false) : (true)">Отправить</button>
			</div>
		</div>
	</div>
	<div class="article-list">
		<div class="panel panel-primary" ng-repeat="comment in articleObject.article.comments | orderBy:commentOrderByDate">
  			<div class="panel-heading">
  				<div class="row">
		    		<div class="col-md-6 text-left">
		    			<span>{{comment.user.username}}</span>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<span>{{comment.created_at}}</span>
		    		</div>
		    	</div>
  			</div>
			<div class="panel-body text-justify">
			    <p>
			    	{{comment.comment}}
			    </p>
			</div>
		</div>
	</div>
</div>
