<div class="articles">
	<!--<a href="#/article/2">dsfdsf</a>-->
	<!--filter-->
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<div class="row text-right">
				<div class="col-md-12">
					<a href="" ng-click="getConsiderationArticles()"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span></a>
				</div>
			</div>
	  		<h3><?= $title ?></h3>
	  	</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span></span>
			  			<select ng-model="orderList" class="form-control">
						    <option value="">Сортировка</option>
						    <option value="title">Название &uarr;</option>
						    <option value="-title">Название &darr;</option>
						    <option value="science">Научное направление &uarr;</option>
						    <option value="-science">Научное направление &darr;</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-header" aria-hidden="true"></span></span>
		  				<input ng-model="search.title" type="text" class="form-control" placeholder="Название статьи" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span></span>
						<select ng-model="search.science.science_name" class="form-control">
						    <option value="">Направление</option>
						    <option ng-repeat="science in sciences | orderBy: science" value="{{science}}">{{science}}</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row row-padding">
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
		  				<input ng-model="search.author.surname" type="text" class="form-control" placeholder="Фамилия автора" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-book" aria-hidden="true"></span></span>
						<select ng-model="search.digest.name" class="form-control">
						    <option value="">Сборник</option>
						    <option ng-repeat="digest in digests | orderBy: digest" value="{{digest}}">{{digest}}</option>
						</select>
					</div>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
	<!--articles-->
	<ul class="article-list">
    	<li ng-repeat="article in articles | orderBy: orderList | filter:search">
    		<div class="row">
    			<div class="col-xs-1">
    				{{$index + 1}}
    			</div>
    			<div class="col-xs-5">
    				<a href="#/article/{{article.status}}/{{article.id}}">{{article.title}}</a>
    				<div class="row row-margin">
						<div class="col-md-12">
							<strong><span>Сборник: </span></strong>
							{{(article.digest.name) ? (article.digest.name) : ('не прикреплен')}}
						</div>
					</div>
    				<div class="row row-margin">
						<div class="col-md-12">
							<strong><span>Автор: </span></strong>
							{{article.author.surname + ' ' + article.author.name + ' ' + article.author.lastname}}
						</div>
					</div>
    				<div class="row row-margin">
    					<div class="col-md-12">
    						<span class="label label-primary article-tag">{{"УДК: " + ((article.udk_index) ? (article.udk_index) : ("не задан"))}}</span>
							<span class="label label-primary article-tag">{{"Код ГРНТИ: " + ((article.grnti) ? (article.grnti) : ("не задан"))}}</span>
							<span class="label label-primary article-tag">{{"Количество страниц: " + ((article.pages) ? (article.pages) : ("не задано"))}}</span>
    					</div>
    				</div>
    			</div>
    			<div class="col-xs-4">
    				<span>{{article.science.science_name}}</span>
    			</div>
    			<div class="col-xs-2">
    				<a href="#/article/{{article.status}}/{{article.id}}"><span class="glyphicon glyphicon-eye-open"></span></a>
    			</div>
    		</div>
    	</li>
  	</ul>
</div>
