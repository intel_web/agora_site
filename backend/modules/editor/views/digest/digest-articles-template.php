<div class="digest-articles">
	<!--<a href="#/article/2">dsfdsf</a>-->
	<!--filter-->
	<div class="panel panel-primary">
		<div class="panel-heading">
	  		<h3>Статьи сборника "{{currentDigest.name}}"</h3>
	  	</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-5">
					<h4>Принятые к публикации статьи</h4>
					<ul class="article-list" dnd-list="freeArticles">
					    <!-- The dnd-draggable directive makes an element draggable and will
					         transfer the object that was assigned to it. If an element was
					         dragged away, you have to remove it from the original list
					         yourself using the dnd-moved attribute -->
					    <li ng-repeat="item in freeArticles"
					        dnd-draggable="item"
					        dnd-moved="freeArticles.splice($index, 1)"
					        dnd-effect-allowed="move"
					        dnd-selected="models.selected = item"
					        ng-class="{'selected': models.selected === item}"
					        >
					        {{item.title}}
					    </li>
					</ul>
				</div>
				<div class="col-xs-2 text-center">
					<div class="row">
						<div class="col-xs-12">
							
							<button type="submit" class="btn btn-success" ng-click="moveFreeArticlesToDigest()">&gt;&gt;</button>
						</div>
					</div>
					<div class="row row-margin">
						<div class="col-xs-12">
							
							<button type="submit" class="btn btn-success" ng-click="moveArticlesFromDigest()">&lt;&lt;</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							
							<button type="submit" class="btn btn-success" ng-click="updateDigestArticles()">Сохранить</button>
						</div>
					</div>
				</div>
				<div class="col-xs-5">
					<h4>Статьи сборника</h4>
					<ul class="article-list" dnd-list="currentDigest.articles">
					    <!-- The dnd-draggable directive makes an element draggable and will
					         transfer the object that was assigned to it. If an element was
					         dragged away, you have to remove it from the original list
					         yourself using the dnd-moved attribute -->
					    <li ng-repeat="item in currentDigest.articles"
					        dnd-draggable="item"
					        dnd-moved="currentDigest.articles.splice($index, 1)"
					        dnd-effect-allowed="move"
					        dnd-selected="models.selected = item"
					        ng-class="{'selected': models.selected === item}"
					        >
					        {{item.title}}
					    </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--articles-->
	<!--<ul class="article-list">
    	<li ng-repeat="article in articles | orderBy: orderList | filter:search:strict">
    		<div class="row">
    			<div class="col-xs-1">
    				{{$index + 1}}
    			</div>
    			<div class="col-xs-5">
    				<a href="#/article/{{article.status}}/{{article.id}}">{{article.title}}</a>
    			</div>
    			<div class="col-xs-4">
    				<span>{{article.science}}</span>
    			</div>
    			<div class="col-xs-2">
    				<a href="#/article/{{article.status}}/{{article.id}}"><span class="glyphicon glyphicon-eye-open"></span></a>
    			</div>
    		</div>
    	</li>
  	</ul>-->
</div>
