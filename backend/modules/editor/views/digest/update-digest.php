<?php 

use dosamigos\datepicker\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="digest-form">
	<!--<a href="#/article/2">dsfdsf</a>-->
	<!--filter-->
	<div class="panel panel-primary">
		<div class="panel-heading">
	  		<h3>Сборник: {{currentDigest.name}}</h3>
	  	</div>
		<div class="panel-body">

			<?php $form = ActiveForm::begin([
					'options' => [
						'ng-submit' => 'update(currentDigest)',
						'novalidate' => '', 
					],
					'method' => 'post',
	    			'action' => false,
				]) ?>

				<?= $form->field($model, 'name')->textInput(['ng-model' => 'currentDigest.name', 'initial-value' => '']) ?>

				<?= $form->field($model, 'is_published')->checkBox(['label' => 'Опубликовать', 'ng-model' => 'currentDigest.is_published', 'initial-value' => '']) ?>

				Файл сборника: <input type="file" id="digestInputFile" file-model="currentDigest.file" /><br />

				<div class="form-group">
        			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        			<a href="#/digest/<?php echo $model->id; ?>/articles" class="btn btn-success">Статьи сборника</a>
    			</div>

			<?php ActiveForm::end() ?>
			<form novalidate class="simple-form">
    			<!--Название: <input type="text" ng-model="currentDigest.name" name="Digest[name]" /><br />-->
    			<!--Статус: <input type="checkbox" ng-model="currentDigest.is_published" name="Digest[is_published]"/><br />
   				Файл сборника: <input type="file" id="digestInputFile" file-model="currentDigest.file" /><br />
   				Дата публикации: -->

   				<?php 
		  					/*DatePicker::widget([
							    'name' => 'Digest[created_at]',
							    'value' => '',
							    'template' => '{addon}{input}',
							    'clientOptions' => [
							        'autoclose' => true,
							        'format' => 'dd.mm.yyyy',
							        'placeholder' => 'Дата публикации'
							    ],
							    'options' => [
							    	'ng-model' => 'currentDigest.publication_date',
							    	'placeholder' => 'Дата обновления'
							   	],
							]);*/
						?>

    			<!--<input type="submit" value="Создать" ng-click="update(currentDigest)"/>-->
  			</form>
		</div>
	</div>
	<!--articles-->
	<!--<ul class="article-list">
    	<li ng-repeat="article in articles | orderBy: orderList | filter:search:strict">
    		<div class="row">
    			<div class="col-xs-1">
    				{{$index + 1}}
    			</div>
    			<div class="col-xs-5">
    				<a href="#/article/{{article.status}}/{{article.id}}">{{article.title}}</a>
    			</div>
    			<div class="col-xs-4">
    				<span>{{article.science}}</span>
    			</div>
    			<div class="col-xs-2">
    				<a href="#/article/{{article.status}}/{{article.id}}"><span class="glyphicon glyphicon-eye-open"></span></a>
    			</div>
    		</div>
    	</li>
  	</ul>-->
</div>
