<div class="digests">
	<!--<a href="#/article/2">dsfdsf</a>-->
	<!--filter-->
	<div class="panel panel-default">
		<div class="panel-heading text-center">
	  		<h3>Сборники</h3>
	  		<div class="row text-center">
	  			<div class="col-md-4">
	  			</div>
	  			<div class="col-md-4">
	  				<span class="spinner" style="display:none"></span>
	  			</div>
	  			<div class="col-md-4">
	  			</div>
	  		</div>
	  	</div>
		<div class="panel-body">
			<div class="row">
				<div class="row" style="margin: 10px 0 10px 0">
					<div class="col-md-12">
						<a href="#/create-digest" class="btn btn-primary">Создать сборник</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span></span>
			  			<select ng-model="orderList" class="form-control">
						    <option value="">Сортировка</option>
						    <option value="name">Название &uarr;</option>
						    <option value="-name">Название &darr;</option>
						    <option value="created_at">Дата публикации &uarr;</option>
						    <option value="-created_at">Дата публикации &darr;</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-header" aria-hidden="true"></span></span>
		  				<input ng-model="search.name" type="text" class="form-control" placeholder="Название сборника" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span></span>
		  				<select ng-model="search.is_published" class="form-control">
						    <option value="">Статус</option>
						    <option value="0">На утверждении</option>
						    <option value="1">Опубликован</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--articles-->
	<ul class="article-list">
    	<li ng-repeat="digest in digests | orderBy: orderList | filter:search:strict">
    		<div class="row">
    			<div class="col-xs-1">
    				{{$index + 1}}
    			</div>
    			<div class="col-xs-3">
    				<a href="#/update-digest/{{digest.id}}">{{digest.name}}</a>
    			</div>
    			<div class="col-xs-3">
    				<span>{{(digest.is_published) ? ('Опубликован') : ('На утверждении')}}</span>
    			</div>
    			<div class="col-xs-3">
    				<span>{{(digest.created_at) ? (digest.created_at) : ('-')}}</span>
    			</div>
    			<div class="col-xs-2">
    				<!--<a href="#/article/{{article.status}}/{{article.id}}"><span class="glyphicon glyphicon-eye-open"></span></a>-->
    				<a title="Статьи" href="#/digest/{{digest.id}}/articles"><span class="glyphicon glyphicon-folder-open"></span></a>
    				<a title="Удалить сборник" id="{{digest.id}}" ng-click="removeDigest($event)"><span class="glyphicon glyphicon-trash"></span></a>
    			</div>
    		</div>
    	</li>
  	</ul>
</div>
