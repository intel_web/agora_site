<div class="article">
	<h2>Профиль работы на конкурс</h2>
	<div class="well article-profile">
		<div class="row">
			<div class="col-md-12 text-left">
				<blockquote><h2>{{articleObject.article.title}}</h2></blockquote>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label>
					Статус заявки
					<select class="form-control" ng-model="articleObject.article.status" convert-to-number ng-change="updateArticleStatus()">
				    	<option value="{{2}}">На рассмотрение</option>
				    	<option value="{{3}}">На доработку</option>
				    	<option value="{{4}}">Принять к конкурсу</option>
					</select>
				</label>
			</div>
		</div>
		<div class="row row-margin" ng-if="articleObject.article.userProfile.surname + ' ' 
				+ articleObject.article.userProfile.name + ' ' + articleObject.article.userProfile.lastname !== articleObject.article.author">
		
			<div class="col-md-12">
				<strong><span>Создатель заявки: </span></strong>
				{{articleObject.article.userProfile.surname + ' ' + articleObject.article.userProfile.name + ' ' + articleObject.article.userProfile.lastname}}
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Автор: </span></strong>
				{{articleObject.article.author}}
			</div>
		</div>				
		<div class="row row-margin" ng-if="articleObject.article.coauthor">
			<div class="col-md-12">
				<strong><span>Соавторы: </span></strong>
				{{articleObject.article.coauthor}}
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Научный руководитель: </span></strong>
				<ul>
					<li ng-repeat="master in articleObject.article.contestMasters">
						{{master.surname + ' ' + master.name + ' ' + master.lastname}}
					</li>
				</ul>
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Прикрепленные материалы: </span></strong>
				{{(articleObject.uploadedFiles) ? ("") : ("нет материалов")}}
				<a download target="_blank" class="btn btn-primary btn-xs" style="margin:0 5px 0 5px" ng-repeat="(key, value) in articleObject.uploadedFiles" href="{{value}}" role="button">
  					{{(key == 'article') ? ("Работа") : ("")}}
					{{(key == 'firstReview') ? ("Рецензия #1") : ("")}}
					{{(key == 'secondReview') ? ("Рецензия #2") : ("")}}
					{{(key == 'expertOpinion') ? ("Экспертное заключение") : ("")}}
				</a>
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Аннотация: </span></strong>
				{{articleObject.article.annotation}}
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Конкурс: </span></strong>
				{{articleObject.article.contest.name}}
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Номинация: </span></strong>
				{{articleObject.article.contestNomination.nomination_name}}
			</div>
		</div>
		<div class="row row-margin">
			<div class="col-md-12">
				<strong><span>Призовое место: </span></strong>
				{{(articleObject.article.score) ? (articleObject.article.score) : ('не задано')}}
			</div>
		</div>
	</div>
	<div class="well">
		<h3>Комментарии</h3>
		<div class="row">
			<div class="col-md-10">
				<textarea class="form-control comment-field" rows="2" ng-model="currentComment"></textarea>
			</div>
			<div class="col-md-2">
				<button type="button" class="btn btn-primary" ng-click="sendComment()">Отправить</button>
			</div>
		</div>
	</div>
	<div class="article-list">
		<div class="panel panel-primary" ng-repeat="comment in articleObject.article.contestComments | orderBy:commentOrderByDate">
  			<div class="panel-heading">
  				<div class="row">
		    		<div class="col-md-6 text-left">
		    			<span>{{comment.user.username}}</span>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<span>{{comment.created_at}}</span>
		    		</div>
		    	</div>
  			</div>
			<div class="panel-body text-justify">
			    <p>
			    	{{comment.comment}}
			    </p>
			</div>
		</div>
	</div>
</div>
