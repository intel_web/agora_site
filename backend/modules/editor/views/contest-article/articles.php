<div class="articles">
	<!--<a href="#/article/2">dsfdsf</a>-->
	<!--filter-->
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<div class="row text-right">
				<div class="col-md-12">
					<a href="" ng-click="getArticles()"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span></a>
				</div>
			</div>
	  		<h3><?= $title ?></h3>
	  	</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span></span>
			  			<select ng-model="orderList" class="form-control">
						    <option value="">Сортировка</option>
						    <option value="title">Название &uarr;</option>
						    <option value="-title">Название &darr;</option>
						    <option value="cnNomination.nomination_name">Номинация &uarr;</option>
						    <option value="-cnNomination.nomination_name">Номинация &darr;</option>
						    <option value="contest.name">Конкурс &uarr;</option>
						    <option value="-contest.name">Конкурс &darr;</option>
						    <option value="score">Место &uarr;</option>
						    <option value="-score">Место &darr;</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-header" aria-hidden="true"></span></span>
		  				<input ng-model="search.title" type="text" class="form-control" placeholder="Название работы" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span></span>
						<select ng-model="search.contest.name" class="form-control">
						    <option value="">Конкурс</option>
						    <option ng-repeat="contest in contests | orderBy: contest" value="{{contest}}">{{contest}}</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row row-padding">
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-education" aria-hidden="true"></span></span>
						<select ng-model="search.contestNomination.nomination_name" class="form-control">
						    <option value="">Номинация</option>
						    <option ng-repeat="nomination in nominations | orderBy: nomination" value="{{nomination}}">{{nomination}}</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
		  				<input ng-model="search.author" type="text" class="form-control" placeholder="Фамилия автора работы" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-king" aria-hidden="true"></span></span>
		  				<input ng-model="search.master" type="text" class="form-control" placeholder="Фамилия научного руководителя" aria-describedby="basic-addon1">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--articles-->
	<!-- <table class="table table-hover articles-table table-striped"> -->
	<table class="table table-striped table-hover">
		<caption>Список работ на конкурс</caption>
		<thead>
			<th>#</th>
			<th>Название</th>
			<th>Номинация</th>
			<th>Автор</th>
			<th>Научный руководитель</th>
			<th ng-if="articles[0].status == 4">Место</th>
		</thead>
		<tbody>
			<tr ng-repeat="article in articles | orderBy: orderList | filter:search:strict">
				<td>{{$index + 1}}</td>
				<td class="col-md-3"><a href="#/contest-article/{{article.status}}/{{article.id}}">{{article.title}}</a></td>
				<td class="col-md-3">{{article.contestNomination.nomination_name}}</td>
				<td class="col-md-2">{{article.author}}</td>
				<!-- <td class="col-md-2">{{(article.master) ? (article.master) : ('-')}}</td> -->
				<td class="col-md-2">
					<ul>
						<li ng-repeat="master in article.contestMasters">
							{{master.surname + ' ' + master.name + ' ' + master.lastname}}
						</li>
					</ul>
				</td>
				<td class="col-md-2" ng-if='article.status == 4'>
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-education" aria-hidden="true"></span></span>
						<select ng-model="article.score" class="form-control" ng-change="updateContestArticleScore(article)">
						    <option value="">Место</option>
						    <option value="1">1</option>
						    <option value="2">2</option>
						    <option value="3">3</option>
						</select>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
