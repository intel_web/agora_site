<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\EditorAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\sidenav\SideNav;
use yii\helpers\Url;
use yii\bootstrap\Modal;

EditorAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body ng-app="editorConsiderationApp">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'IT&Транспорт',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Рассылка', 'url' => ['mail/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-3" ng-controller="EditorInbox">

                <?=
                    SideNav::widget([
                        'type' => SideNav::TYPE_PRIMARY,
                        'encodeLabels' => false,
                        'heading' => 'Публикации',
                        'items' => [
                            ['label' => '<span class="pull-right badge">{{(inbox.consideration)}}</span> На рассмотрении', 'icon' => 'envelope', 'url' => '#/articles/2'],
                            ['label' => '<span class="pull-right badge">{{(inbox.approved)}}</span> Одобренные', 'icon' => 'ok', 'url' => '#/articles/4'],
                            ['label' => '<span class="pull-right badge">{{(inbox.paided)}}</span> Оплаченные', 'icon' => 'rub', 'url' => '#/articles/paided'],
                            ['label' => '<span class="pull-right badge">{{(inbox.published)}}</span> Опубликованные', 'icon' => 'folder-open', 'url' => '#/articles/5'],
                            ['label' => '<span class="pull-right badge">{{(inbox.digests)}}</span> Сборники', 'icon' => 'book', 'url' => '#/digests'],
                        ],
                    ])
                ?>

                <?=
                    SideNav::widget([
                        'type' => SideNav::TYPE_PRIMARY,
                        'encodeLabels' => false,
                        'heading' => 'Конкурсы',
                        'items' => [
                            ['label' => '<span class="pull-right badge">{{(inbox.contestArticleConsideration)}}</span> Заявки', 'icon' => 'envelope', 'url' => '#/contest-articles/2'],
                            ['label' => '<span class="pull-right badge">{{(inbox.contestArticleApproved)}}</span> Принято к конкурсу', 'icon' => 'ok', 'url' => '#/contest-articles/4'],
                            ['label' => '<span class="pull-right badge">{{(inbox.contests)}}</span> Конкурсы', 'icon' => 'envelope', 'url' => '#/contests'],
                        ],
                    ])
                ?>
            </div>
            <div class="col-md-10 col-lg-10 col-sm-9">

                <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>

                <?= Alert::widget() ?>

                <div class="row" ng-controller="ErrorController">
                    <div class="col-md-12">
                        <div ng-repeat="(key,val) in alerts" class="alert {{key}}">
                            <div ng-repeat="msg in val">{{msg}}</div>
                        </div>
                    </div>
                </div>

                <div id="progress-bar" class="progress" style="display: none">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
						<span class="sr-only">45% Complete</span>
					</div>
				</div>

                <div ng-view></div>
                <?php //$content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; IT&Транспорт <?= date('Y') ?></p>

        <p class="pull-right"><a href="http://its-spc.ru/">ООО «НПЦ «ИТС»</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
