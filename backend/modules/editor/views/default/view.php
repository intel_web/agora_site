<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */

$this->title = 'История учащихся';
?>
<div class="parent-profile-view">

    <h1>Учащиеся</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'ФИО',
                'value' => function ($data) {
                    return $data->surname.' '.$data->name.' '.$data->lastname;
                },
            ],
            /*['class' => 'yii\grid\ActionColumn'],*/
        ],
    ]); ?>

</div>
