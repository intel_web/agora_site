<?php 

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
		'options' => [
			'ng-submit' => ($model->isNewRecord) ? ('create(currentContest)') : ('update(currentContest)'),
			'novalidate' => '', 
		],
		'method' => 'post',
		'action' => false,
	]) ?>

	<?= $form->field($model, 'name')->textInput(['ng-model' => 'currentContest.name', 'initial-value' => '', 'ng-required' => 'required']) ?>

	<?= $form->field($model, 'start_date')->textInput(['ng-model' => 'currentContest.start_date', 'initial-value' => '', 'jqdatepicker' => '', 'ng-required' => 'required']) ?>

	<?= $form->field($model, 'end_date')->textInput(['ng-model' => 'currentContest.end_date', 'initial-value' => '', 'jqdatepicker' => '']) ?>

	<?= $form->field($model, 'requirements')->textArea(['rows' => '6', 'ng-model' => 'currentContest.requirements', 'initial-value' => '']) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

<?php ActiveForm::end() ?>
