<div class="contests">
	<!--<a href="#/article/2">dsfdsf</a>-->
	<!--filter-->
	<div class="panel panel-default">
		<div class="panel-heading text-center">
	  		<h3>Конкурсы</h3>
	  		<div class="row text-center">
	  			<div class="col-md-4">
	  			</div>
	  			<div class="col-md-4">
	  				<span class="spinner" style="display:none"></span>
	  			</div>
	  			<div class="col-md-4">
	  			</div>
	  		</div>
	  	</div>
		<div class="panel-body">
			<div class="row">
				<div class="row" style="margin: 10px 0 10px 0">
					<div class="col-md-12">
						<a href="#/create-contest" class="btn btn-primary">Создать конкурс</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
			  			<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span></span>
			  			<select ng-model="orderList" class="form-control">
						    <option value="">Сортировка</option>
						    <option value="name">Название &uarr;</option>
						    <option value="-name">Название &darr;</option>
						    <option value="created_at">Дата публикации &uarr;</option>
						    <option value="-created_at">Дата публикации &darr;</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
		  				<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-header" aria-hidden="true"></span></span>
		  				<input ng-model="search.name" type="text" class="form-control" placeholder="Название конкурса" aria-describedby="basic-addon1">
					</div>
				</div>
			</div>
			<div class="row row-padding">
				<div class="col-md-6">
					<div class="input-group">
	  					<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
	  					<input ng-model="search.start_date" type="text" class="form-control" placeholder="Дата начала" aria-describedby="basic-addon1" jqdatepicker>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
	  					<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
	  					<input ng-model="search.end_date" type="text" class="form-control" placeholder="Дата окончания" aria-describedby="basic-addon1" jqdatepicker>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--articles-->
	<ul class="article-list">
    	<li ng-repeat="contest in contests | orderBy: orderList | filter:search:strict">
    		<div class="row">
    			<div class="col-xs-1">
    				{{$index + 1}}
    			</div>
    			<div class="col-xs-4">
    				<a href="#/update-contest/{{contest.id}}">{{contest.name}}</a>
    			</div>
    			<div class="col-xs-5">
    				<p>Сроки проведения:</p>
    				<span>с {{contest.start_date}} до {{contest.end_date}}</span>
    			</div>
    			<div class="col-xs-2">
    				<a title="Удалить конкурс" id="{{contest.id}}" ng-click="removeContest($event)"><span class="glyphicon glyphicon-trash"></span></a>
    				<a title="Ведомость победителей" ng-click="getSheet(contest.id)"><span class="glyphicon glyphicon-file"></span></a>
    				<a title="Ведомость участников" ng-click="getParticipantsSheet(contest.id)"><span class="glyphicon glyphicon-user"></span></a>
    				<a title="Сформировать дипломы" ng-click="getDiplom(contest.id)"><span class="glyphicon glyphicon-book"></span></a>
    			</div>
    		</div>
    	</li>
  	</ul>
</div>
