<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use common\models\Category;


$this->title = 'Рассылка писем пользователям';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <div class="row text-center">
                <div class="col-md-12">
                    <h3><?= Html::encode($this->title) ?></h3>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?php $form = ActiveForm::begin(['id' => 'mail-form']); ?>

                        <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'category')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Category::find()->all(),'id',
                                function($model){
                                    return $model->name;
                                }),
                            'toggleAllSettings' => [
                                'selectLabel' => '<i></i>',
                            ],
                            'options' => ['placeholder' => 'Выберите категории...', 'multiple' => true],
                            'pluginOptions' => [
                                'tags' => true,
                                'tokenSeparators' => [',', ' '],
                                'maximumInputLength' => 10,
                            ],
                        ])->label('Категории рассылки'); ?>

                        <?= $form->field($model, 'body')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'Standard',
                        ]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Разослать', ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
