<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class ScoreTableWidget extends Widget {

	public $model;
	public $scoreTable;

	public function init() {
		if (($this->model === null) || (!$this->model->calendar) || (!$this->model->student_marks)) {
			$this->scoreTable = Html::tag('div', 'Нет данных', ['class' => 'row']);
		} else {
			$this->scoreTable = $this->formateData($this->model);
		}
	}

	private function formateData($model) {
		$resultTable .= "<div class='row'>";
		$resultTable .= $this->getLeftTable($model->student_marks);
		$resultTable .= $this->getRightTable($model);
		$resultTable .= "</div>";

		return $resultTable;
	}

	private function getLeftTable($subjects) {
		$resultTable .= "<div class='col-xs-4' style='padding: 0'>";
		$resultTable .= "<table class='table table-striped table-bordered detail-view'>";
		$resultTable .= "<thead>";
		$resultTable .= "<tr><th>Предмет&nbsp;/&nbsp;Дата</th></tr>";
		$resultTable .= "</thead>";
		foreach ($subjects as $subjects_data) {
			$resultTable .= '<tr>';
			$resultTable .= "<td>".Html::encode($subjects_data[0]->subject->name)."</td>";
			$resultTable .= "</tr>";
        }
		$resultTable .= "</table>";
		$resultTable .= "</div>";

		return $resultTable;
	}

	private function getRightTable($model) {
		$resultTable .= "<div class='col-xs-8' style='padding: 0'>";
        $resultTable .= "<div style='width:100%; overflow-x: scroll'>";
        $resultTable .= "<table class='table table-striped table-bordered detail-view'>";
        $resultTable .= "<thead>";
        $resultTable .= "<tr>";
        foreach ($model->calendar as $elem) {
            $resultTable .= "<th style='text-align:center'>";
            $elem = \DateTime::createFromFormat("Y-m-d", $elem);
            $resultTable .= Html::encode($elem->format("d"))."&nbsp;-&nbsp;".$elem->format("m");
            $resultTable .= '</th>';
        }
        $resultTable .= "</tr>";
        $resultTable .= "</thead>";
        foreach ($model->student_marks as $subjects_data) {
        	$resultTable .= '<tr>';
        	foreach ($model->calendar as $calendar_elem) {
            	//$resultTable .= "<td style='background-color: whitesmoke'>";
            	$resultTable .= $this->getScoresInDate($subjects_data, $calendar_elem, $model->student_subscription);
            	/*foreach ($subjects_data as $subject_data) {
                	if ($calendar_elem == $subject_data->mark_date) {
                    	$resultTable .= $subject_data->mark."&nbsp;";
                	} else {
                    	$resultTable .= "";
                	}
            	}*/
        		//$resultTable .= "</td>";
        	}
        	$resultTable .= "</tr>";
        }
        $resultTable .= "</table>";
        $resultTable .= "</div>";
        $resultTable .= "</div>";

        return $resultTable;
	}

	//returns cell with marks by date
	private function getScoresInDate($subject_marks, $date, $student_subscriptions) {
		$dateScores = array();
		if (!$this->checkSubscription($date, $student_subscriptions)) {
			return "<td title='не оплачено' style='background-color: rgb(166, 165, 165); text-align:center'>&nbsp;</td>";
		} else {
			foreach ($subject_marks as $mark) {
            	if ($date == $mark->mark_date) {
            		$dateScores[] = $mark->mark;
            	}
        	}
        	if (count($dateScores)) {
        		return "<td style='text-align:center'>".implode(',&nbsp;', $dateScores)."</td>";
        	} else {
        		return "<td style='text-align:center'>-</td>";
        	}
		}
	}

	private function checkSubscription($date, $student_subscriptions) {
		$allowed = false;
		foreach ($student_subscriptions as $student_subscription) {
			if (($date >= $student_subscription->subscription_start) && ($date <= $student_subscription->subscription_end)) {
				$allowed = true;
			}
		}
		return $allowed;
	}

	public function run() {
		return $this->scoreTable;
	}
}