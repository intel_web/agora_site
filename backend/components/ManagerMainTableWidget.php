<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
					   
class ManagerMainTableWidget extends Widget {
	
	public $model;
	public $managerTable;
 	public $school_name_arr;
	public $school_id_arr;
	public $result;
	public $total;
	public $school_year;
	
	public $arr=array('09'=>'Сентябрь', 
					   '10'=>'Октябрь', 
					   '11'=>'Ноябрь', 
					   '12'=>'Декабрь', 
					   '01'=>'Январь', 
					   '02'=>'Февраль', 
					   '03'=>'Март', 
					   '04'=>'Апрель', 
					   '05'=>'Май');
	
	public function init() {
		if(($this->model === null)||(count($this->model)==0)){
			$this->managerTable = Html::tag('div', 'Нет данных за текущий период', ['class' => 'row']);
		}else{
			$this->managerTable= $this->formateData();
		}
	}
	private function formateData() {
		$resultTable = "";
		$resultTable .= "<div class='row'>";
		$resultTable .= $this->getLeftTable();
		$resultTable .= $this->getRightTable();
		$resultTable .= "</div>";

		return $resultTable;
	}
	
	private function getLeftTable(){
		$tmp=array();
		$tmp1=array();
		
		//return all unique school_id from SQL
		foreach($this->model as $school){
			array_push($tmp,$school[id]);
		}
		$this->school_id_arr = array_values(array_unique($tmp));
		
		//return all unique school_name from SQL
		foreach($this->model as $school){
			array_push($tmp1,$school[School]);
		} 
		$this->school_name_arr = array_values(array_unique($tmp1));
		
		//return school_id+school_name array		
		for($i = 0, $max = count($this->school_name_arr);$i < $max;$i++){
			$this->result[$i]=["id"=>$this->school_id_arr[$i],"school"=>$this->school_name_arr[$i]];
		}	
				
		$resultTable .= "<div class='col-xs-2' style='padding: 0'>";
		$resultTable .= "<table class='table table-striped table-bordered detail-view'>";
		$resultTable .= "<thead>";
		$resultTable .= "<tr><th>Школа&nbsp;/&nbsp;Месяц</th></tr>";
		$resultTable .= "</thead>";
		for($i=0; $i<count($this->result);$i++){
			$resultTable .= '<tr>';
			$resultTable .= "<td>".Html::a($this->result[$i][school],/*['school','id'=>$this->result[$i][id]]*/"#", 
				['id' => $this->result[$i][id],'data-year'=> $this->school_year->id, 'class' => 'school-link'])."</td>";
			$resultTable .= "</tr>";
		}
		$resultTable .= "</table>";
		$resultTable .= "</div>";
				
		return $resultTable;
	}
	
	private function getRightTable() {
		$resultTable = "";
		$resultTable .= "<div class='col-xs-10' style='padding: 0'>";
        $resultTable .= "<div style='width:100%; overflow-x: scroll'>";
        $resultTable .= "<table class='table table-striped table-bordered detail-view' align='center'>";
        $resultTable .= "<thead>";
        $resultTable .= "<tr>";
        foreach ($this->arr as $elem) {
            $resultTable .= "<th colspan='2' style='text-align:center'>";
            $resultTable .= Html::encode($elem);
            $resultTable .= '</th>';
        }
		$resultTable .= "<th colspan='2' style='text-align:center'>";
		$resultTable .= "Итого";
		$resultTable .= "</th>";
        $resultTable .= "</tr>";
        $resultTable .= "</thead>";

		for($i=0;$i<count($this->school_id_arr);$i++){
			$resultTable .="<tr align='center'>";
			$global_count=0;
			for($k=0;$k<9;$k++){
				
				$count=0;
				$rate=0;
				for($j = 0; $j < count($this->model); $j++){

					if($this->model[$j][id]==$this->school_id_arr[$i]){
						$rate=$this->model[$j][rate];
 						$tmp=array_keys($this->arr);
						/*if($tmp[$k]>=9 && date('m')<9){
							$year=(date("Y")-1);	
						}
						if($tmp[$k]>=9 && date('m')>9){
							$year=(date("Y"));
						}
						if($tmp[$k]<9 && date('m')<9){
							$year=(date("Y"));
						}
						if($tmp[$k]<9 && date('m')>9){
							$year=(date("Y")+1);
						}*/
						if($tmp[$k]>=9){
							$year=$this->school_year->start_year;
						}else{
							$year=$this->school_year->end_year;
						}
						if($this->model[$j][BEGIN]< ''.$year.'-'.$tmp[$k].'-15' and ''.$year.'-'.$tmp[$k].'-15' < $this->model[$j][END]){
							$count++;
						}
					}
				}
				$global_count+=$count;
				$resultTable .= "<td>";
				$resultTable .= Html::encode($count);
				$resultTable .= "&nbsp;шт";
				$resultTable .= "</td>";
				$resultTable .= "<td>";
				$resultTable .= Html::encode($count*$rate);
				$resultTable .= "&#8381";
				$resultTable .= "</td>";
			}
			$resultTable .= "<td>";
			$resultTable .= Html::encode($global_count*$rate);
			$resultTable .= "&#8381";
			$resultTable .= "</td>";
			$resultTable .="</tr>";
			$this->total+=$global_count*$rate;
		}
		$resultTable .='<td colspan="19" align="right">';
		$resultTable .= Html::encode($this->total);	
		$resultTable .= "&#8381";
		$resultTable .="</td>";		
        $resultTable .= "</table>";
        $resultTable .= "</div>";
        $resultTable .= "</div>";
		

        return $resultTable;
	}
	
	public function run() {
		return $this->managerTable;
	}
}