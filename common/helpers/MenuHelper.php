<?php

namespace common\helpers;

use common\models\SiteMenu;

class MenuHelper {

	public static function getMenuItems() {
		$tree = [];
		$prepared_rows = array();
		$menuItems = SiteMenu::find()->asArray()->all();
		
		foreach ($menuItems as $menuItem) {
			$menuItem['items'] = array();
			$prepared_rows[$menuItem['id']] = $menuItem;
		}
		foreach( $prepared_rows as $id=>&$item ){
			if( array_key_exists( $item['parent_id'], $prepared_rows )){
				$prepared_rows[ $item['parent_id'] ]['items'][ $id ] =& $item;
				$prepared_rows[ $item['parent_id'] ]['items'][ $id ]['url'] = /*$prepared_rows[ $item['parent_id'] ]['items'][ $id ]['page_id']*/['site/page', 'id' => $prepared_rows[ $item['parent_id'] ]['items'][ $id ]['page_id']];
			}  // если есть родительская вершина в дереве
			else {
				$tree[$id] =& $item;
				$tree[$id]['url'] = /*$tree[$id]['page_id']*/['site/page', 'id' => $tree[$id]['page_id']];
			} // иначе - это вершина верхнего уровня
		}
		return MenuHelper::removeEmptyChildItems($tree);
	}

	public static function removeEmptyChildItems(&$tree) {
  		foreach( $tree as &$item ){
			if (!empty($item['items'])) {
				MenuHelper::removeEmptyChildItems( $item['items']);
			} else {
				unset($item['items']);
			}
    	}
    	return $tree;
	}

}