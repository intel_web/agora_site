<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="new-application-mail">
    <p>Ассалам алейкум, уважаемый модератор ресурса IT&Транспорт!</p>

    <p>Сообщаем Вам, что на ресурсе IT&Транспорт появилась новая заявка на конкурс <?= Html::encode($cn_article->contest->name) ?>,</p>
    <p>требующая Вашего рассмотрения</p>
    <p>Cоздатель заявки: <?= Html::encode($cn_article->userProfile->surname) . ' ' 
    	. Html::encode($cn_article->userProfile->name) . ' ' 
    	. Html::encode($cn_article->userProfile->lastname)?></p>
    <p>Автор: <?= Html::encode($cn_article->author) ?></p>
    <p>Организация: <?= Html::encode($cn_article->organization) ?></p>
    <p>Статья: <?= Html::encode($cn_article->title) ?></p>



    <p>С уважением,<br> email-бот ресурса IT&Транспорт</p>


</div>
