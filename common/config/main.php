<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'aliases' => [
        @uploads => '../uploads',
        @agora => 'http://ittransport.ru/',
        @digestsPath => '/digests',
        @contestsPath => 'contests',
        @articlesPath => 'articles',
    ],
];
