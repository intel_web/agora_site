<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "digest".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property integer $is_published
 *
 * @property Article[] $articles
 */
class Digest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'digest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['created_at'], 'safe'],
            [['is_published'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название сборника',
            'created_at' => 'Дата создания',
            'is_published' => 'Опубликован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['digest_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
           if(Article::find()->where(['digest_id'=>$this->id])->one()!=null){
            Yii::$app->getSession()->setFlash('error', 'Сборник имеет связанные записи в таблице article');
            return false;
           }else{
            return true;
           }
        } else {
            return false;
        }
    }
}
