<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'pages', 'status', 'payment_status', 'digest_id', 'science_id'], 'integer'],
            [['title', 'udk_index', 'created_at', 'updated_at', 'tags', 'rus_annotation', 'eng_annotation', 'coauthor', 'organization', 'grnti'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
            'pages' => $this->pages,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
            'payment_status' => $this->payment_status,
            'digest_id' => $this->digest_id,
            'science_id' => $this->science_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'udk_index', $this->udk_index])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'rus_annotation', $this->rus_annotation])
            ->andFilterWhere(['like', 'eng_annotation', $this->eng_annotation])
            ->andFilterWhere(['like', 'coauthor', $this->coauthor])
            ->andFilterWhere(['like', 'organization', $this->organization])
            ->andFilterWhere(['like', 'grnti', $this->grnti]);

        return $dataProvider;
    }
}
