<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $preview
 * @property string $publish_date
 * @property integer $status
 * @property integer $is_presentation
 * @property string $thumbnail
 * @property string $username
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'preview'], 'required'],
            [['content', 'preview'], 'string'],
            [['publish_date'], 'safe'],
            [['status', 'is_presentation'], 'integer'],
            [['title', 'thumbnail', 'username'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Контент',
            'preview' => 'Превью записи',
            'publish_date' => 'Дата публикации',
            'status' => 'Статус',
            'is_presentation' => 'Поместить в слайдер на главную',
            'thumbnail' => 'Превью',
            'username' => 'Автор',
        ];
    }

    /**
    * Before save function
    */
    public function beforeSave($insert) {
        if (!$this->publish_date) {
            $this->publish_date = date('Y-m-d');
        }
        $this->username = Yii::$app->user->identity->username;

        return parent::beforeSave($insert);
    }

    /**
    * Truncate content for preview
    */
    public function truncateContent($numb) {
        $preview = $this->content;
        if (strlen($this->content) > $numb) {
            $preview = strip_tags($preview, '<p><a>');
            $preview = substr($preview, 0, $numb); 
            $preview = substr($preview, 0, strrpos($preview, " ")); 
            $etc = " ...";  
            $preview = $preview.$etc; 
        }
        return $preview;
    }

    public function getStatusAlias() {
        return ($this->status) ? ('Опубликована') : ('В ожидании');
    }

    public function getPresentationsAlias() {
        return ($this->is_presentation) ? ('Да') : ('Нет');
    }
}
