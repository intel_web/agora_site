<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cn_nomination".
 *
 * @property integer $id
 * @property string $nomination_name
 *
 * @property CnArticle[] $cnArticles
 */
class ContestNomination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cn_nomination';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomination_name'], 'required'],
            [['nomination_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomination_name' => 'Название номинации',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnArticles()
    {
        return $this->hasMany(CnArticle::className(), ['cn_nomination_id' => 'id']);
    }

    public function beforeDelete() 
    {
        if (parent::beforeDelete()) 
        {
            $model=ContestArticle::find()->where('cn_nomination_id = :cn_nomination_id',[':cn_nomination_id'=>$this->id])->all();
            if(!empty($model))
            {
                Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей cn_article');
                return false;
            }else{
                return true;
            }    
        } else {
            return false;
        }
    }
}
