<?php

namespace common\models;

use Yii;
use common\models\Author;
use common\models\Digest;
use common\models\ArticleAuthor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use common\helpers\Translit;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $title
 * @property string $udk_index
 * @property integer $pages
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property integer $payment_status
 * @property string $tags
 * @property integer $digest_id
 * @property string $rus_annotation
 * @property string $eng_annotation
 * @property string $coauthor
 * @property integer $science_id
 * @property string $organization
 * @property string $grnti
 *
 * @property Science $science
 * @property Digest $digest
 * @property ArticleAuthor[] $articleAuthors
 * @property Comment[] $comments
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'created_at', 'updated_at', 'status', 'rus_annotation', 'eng_annotation', 'science_id', 'organization','author_id'], 'required'],
            [['pages', 'status', 'payment_status', 'digest_id', 'science_id','author_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['rus_annotation', 'eng_annotation'], 'string'],
            [['title', 'udk_index', 'tags', 'coauthor', 'organization', 'grnti'], 'string', 'max' => 255],
            [['rus_annotation'], 'match', 'pattern' => "/[^a-zA-Z]+$/u", 'message' => 'Разрешены только символы кириллицы'],
            [['eng_annotation'], 'match', 'pattern' => "/[^а-яёА-ЯЁ]+$/u", 'message' => 'Разрешены только символы латиницы'],
            [['udk_index', 'grnti'], 'match', 'pattern' => "/^[0-9\.]+$/u", 'message' => 'Не верное значение'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'author_id'=>'Автор',
            'udk_index' => 'Индекс УДК',
            'pages' => 'Количество страниц',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'status' => 'Статус',
            'payment_status' => 'Оплачено',
            'tags' => 'Тэги',
            'digest_id' => 'Журнал',
            'rus_annotation' => 'Аннотация на русском языке',
            'eng_annotation' => 'Аннотация на английском языке',
            'coauthor' => 'Соавторы',
            'science_id' => 'Научное направление',
            'organization' => 'Организация',
            'grnti' => 'ГРНТИ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScience()
    {
        return $this->hasOne(Science::className(), ['id' => 'science_id']);
    }

    public function getArticleAuthor()
    {
        return $this->hasOne(Author::className(), ['user_id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDigest()
    {
        return $this->hasOne(Digest::className(), ['id' => 'digest_id']);
    }

    public function getCoauthorRequest()
    {
        return $this->hasMany(CoauthorRequest::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleAuthors()
    {
        return $this->hasMany(ArticleAuthor::className(), ['article_id' => 'id']);
    }

    public function getAuthor() {
        return $this->hasOne(Author::className(), ['user_id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['article_id' => 'id']);
    }

    public function getStatus($model)
    {
        switch($model->status){
            case Yii::$app->params['draft']:
                return 'Черновик';
                break;
            case Yii::$app->params['review']:
                return 'На рассмотрении';
                break;
            case Yii::$app->params['returned']:
                return 'На доработку';
                break;
            case Yii::$app->params['accepted']:
                return 'Одобрена';
                break;
            case Yii::$app->params['published']:
                return 'Опубликована';
                break;        
        }
    }

    public function getArticleAuthorToString($author_id)
    {
        $author_fio='';
        if(($model=Author::findOne($author_id))!==null){
            $author_fio=$model->surname." ".$model->name." ".$model->lastname;
        }
        return $author_fio;
    }

    public function getAuthors()
    {
        $authors = [];
        if(isset($this->id))
        {
            array_push($authors, Author::find()
                ->join('JOIN','article','author.user_id = article.author_id')
                ->where('article.id = :articleId', ['articleId' => $this->id])
                ->one());
            $coauthors = Author::find()
                ->join('JOIN','article_author','author.user_id = article_author.author_user_id')
                ->where('article_author.article_id = :articleId', ['articleId' => $this->id])
                ->all();

            if(!empty($coauthors))
            {
                foreach($coauthors as $coauthor)
                {
                    array_push($authors, $coauthor);
                }
            }
        }

        return $authors;
    }

    public function getAuthorsToString($model)
    {
        $article_authors='';
        $authors=Author::find()->joinWith(['articleAuthors'])->where(['article_id'=>$model->id])->all();
            foreach ($authors as $author) {
                $article_authors.=Html::encode($author->surname." ");
                $article_authors.=Html::encode($author->name." ");
                $article_authors.=Html::encode($author->lastname);
                $article_authors.='; ';
            }
        
        $article_authors.=$model->coauthor;

        return $article_authors;    
    }

    public function getArticleDigest($digest_id)
    {
        if($digest_id != null) {
            $digest = Digest::findOne($digest_id);
            $article_digest = glob(Yii::getAlias('@uploads').Yii::getAlias('@digestsPath')."/".trim(Translit::transliterationWithSpace($digest->name)).".*");
        }
        if(!empty($article_digest[0])){
            $article_digest=$article_digest[0];
            $article_digest=substr($article_digest,2);
            return Html::a($digest->name, $article_digest,['target'=>'_blank']);
        }else{
            return '';
        }
    }

    public function getAttachedFiles($model)
    {
        $file_path='';
        $articleFilesArray = $this->getAttachedFilesArray($model);
        
        if($articleFilesArray['article']!==null){
            $article=$articleFilesArray['article'];
            $article=substr($article,2);
            $file_path.=Html::a('Статья', $article, ['target'=>'_blank']);
            $file_path.='<br>';
        }
        if($articleFilesArray['firstReview']!==null){
            $first_review=$articleFilesArray['firstReview'];
            $first_review=substr($first_review,2);
            $file_path.=Html::a('Рецензия № 1', $first_review, ['target'=>'_blank']);
            $file_path.='<br>';        
        } 
        if($articleFilesArray['secondReview']!==null){
            $second_review=$articleFilesArray['secondReview'];
            $second_review=substr($second_review,2);
            $file_path.=Html::a('Рецензия № 2', $second_review, ['target'=>'_blank']);
            $file_path.='<br>';
        }
        if($articleFilesArray['expertOpinion']!==null){
            $expert_opinion=$articleFilesArray['expertOpinion'];
            $expert_opinion=substr($expert_opinion,2);
            $file_path.=Html::a('Экспертное заключение', $expert_opinion, ['target'=>'_blank']);
            $file_path.='<br>';
        }

        if($file_path==''){
            return Html::encode('Нет прикрепленых файлов');
        }else{
            return $file_path;
        }
    }

    public function getAttachedFilesArray($model) 
    {        
        $article = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$model->author_id."/"
            .$model->id."/".Yii::$app->params['articleFileName'].".*");
        $first_review = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$model->author_id."/"
            .$model->id."/".Yii::$app->params['firstReviewFileName'].".*");
        $second_review = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$model->author_id."/"
            .$model->id."/".Yii::$app->params['secondReviewFileName'].".*");
        $expert_opinion = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$model->author_id."/"
            .$model->id."/".Yii::$app->params['expertOpinionFileName'].".*");
        
        $articleFilesArray['article'] = empty($article[0]) ? null : $article[0];
        $articleFilesArray['firstReview'] = empty($first_review[0]) ? null : $first_review[0];
        $articleFilesArray['secondReview'] = empty($second_review[0]) ? null : $second_review[0];
        $articleFilesArray['expertOpinion'] = empty($expert_opinion[0]) ? null : $expert_opinion[0];

        return $articleFilesArray;
    }

    public function getArticleScience($science_id)
    {
        $article_science='';
        if(($model=Science::findOne($science_id))!=null)
        {
            $article_science=$model->science_name;
        }
        return $article_science;
    }

    /**
    * Delete all related records before contest deleted
    */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            $article_authors = ArticleAuthor::find()
                ->where('article_id = :article_id', [':article_id' => $this->id])
                ->all();
            foreach ($article_authors as $article_authors) {
                $article_authors->delete();
            }
            $article_comments=Comment::find()
                ->where('article_id = :article_id', [':article_id' => $this->id])
                ->all();
            if(!empty($article_comments)){
                foreach($article_comments as $coments){
                    $coments->delete();
                }
            }
            return true;
        } else {
            return false;
        }
    }

}
