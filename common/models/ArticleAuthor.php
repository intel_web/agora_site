<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_author".
 *
 * @property integer $id
 * @property integer $author_user_id
 * @property integer $article_id
 * @property integer $owner
 *
 * @property Article $article
 * @property Author $authorUser
 */
class ArticleAuthor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_author';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_user_id', 'article_id'], 'required'],
            [['author_user_id', 'article_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_user_id' => 'Автор',
            'article_id' => 'Статья',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorUser()
    {
        return $this->hasOne(Author::className(), ['user_id' => 'author_user_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) 
        {
            if(ArticleAuthor::find()->where(['article_id'=>$this->article_id])
                ->andWhere(['author_user_id'=>$this->author_user_id])->exists() || 
                Article::find()->where(['author_id'=> $this->author_user_id])->exists())
            {
                Yii::$app->getSession()->setFlash('error', 'Невозможно добавить этого пользователя в число соавторов');
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}
