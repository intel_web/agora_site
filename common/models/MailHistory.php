<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "mail_history".
 *
 * @property integer $id
 * @property string $email
 * @property string $mail_subject
 * @property string $mail_body
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class MailHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    public static function tableName()
    {
        return 'mail_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'mail_subject', 'mail_body'], 'required'],
            [['status'], 'integer'],
            [ ['status'], 'default', 'value' => 0],
            [['email'], 'email'],
            [['mail_subject', 'mail_body'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Эл.Почта',
            'mail_subject' => 'Тема письма',
            'mail_body' => 'Текст письма',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
}
