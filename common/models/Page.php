<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $page_body
 * @property integer $status
 * @property string $layout
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['page_body'], 'string'],
            [['status'], 'integer'],
            [['title', 'layout'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок страницы',
            'page_body' => 'Контент',
            'status' => 'Статус',
            'layout' => 'Шаблон',
        ];
    }

    public function getLayouts() {
        $files = array_diff(scandir(Yii::getAlias('@frontend').'/views/layouts/page layouts'), array('..', '.'));
        $layouts = array();
        foreach ($files as $file) {
            $layouts[Yii::getAlias('@frontend').'/views/layouts/page layouts/'.$file] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file);
        }
        return $layouts;
    }

    public function getStatusAlias() {
        return ($this->status) ? ('Опубликована') : ('В ожидании');
    }
}
