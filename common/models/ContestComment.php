<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cn_comment".
 *
 * @property integer $id
 * @property string $comment
 * @property string $created_at
 * @property integer $user_id
 * @property integer $cn_article_id
 *
 * @property CnArticle $cnArticle
 * @property User $user
 */
class ContestComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cn_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'created_at', 'user_id', 'cn_article_id'], 'required'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['user_id', 'cn_article_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => 'Комментарий',
            'created_at' => 'Дата',
            'user_id' => 'Пользователь',
            'cn_article_id' => 'Работа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestArticle()
    {
        return $this->hasOne(ContestArticle::className(), ['id' => 'cn_article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
