<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "academic_degree".
 *
 * @property integer $id
 * @property string $academic_degree
 *
 * @property Author[] $authors
 */
class AcademicDegree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academic_degree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_degree'], 'required'],
            [['academic_degree'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academic_degree' => 'Ученая степень',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['academic_degree_id' => 'id']);
    }

    public function beforeDelete() 
    {
        if (parent::beforeDelete()) 
        {
            $model=Author::find()->where('academic_degree_id = :academic_degree_id',[':academic_degree_id'=>$this->id])->all();
            if(!empty($model))
            {
                Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей Author');
                return false;
            }else{
                return true;
            }    
        } else {
            return false;
        }
    }
}
