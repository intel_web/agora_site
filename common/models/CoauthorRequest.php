<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coauthor_request".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $article_author_id
 * @property integer $requested_user_id
 *
 * @property Author $requestedUser
 */
class CoauthorRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coauthor_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'article_author_id', 'requested_user_id'], 'required'],
            [['article_id', 'article_author_id', 'requested_user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Статья',
            'article_author_id' => 'Автор статьи',
            'requested_user_id' => 'Запросивший пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestedUser()
    {
        return $this->hasOne(Author::className(), ['user_id' => 'requested_user_id']);
    }

    public function getArticleTitle($article_id)
    {
        $article_title='';
        if(($model=Article::findOne($article_id))!==null){
            $article_title=$model->title;
        }
        return $article_title;
    }
}
