<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContestArticle;

/**
 * ContestArticleSearch represents the model behind the search form about `common\models\ContestArticle`.
 */
class ContestArticleSearch extends ContestArticle
{
    public $master;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'cn_nomination_id', 'contest_id', 'user_id','score'], 'integer'],
            [['title', 'annotation', 'master'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContestArticle::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'cn_nomination_id' => $this->cn_nomination_id,
            'contest_id' => $this->contest_id,
            'user_id' => $this->user_id,
            'master'=>$this->master,
        ]);


        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'annotation', $this->annotation]);

        return $dataProvider;
    }
}
