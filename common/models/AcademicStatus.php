<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "academic_status".
 *
 * @property integer $id
 * @property string $academic_status
 *
 * @property Author[] $authors
 */
class AcademicStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academic_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_status'], 'required'],
            [['academic_status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'academic_status' => 'Ученое звание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['academic_status_id' => 'id']);
    }

    public function beforeDelete() 
    {
        if (parent::beforeDelete()) 
        {
            $model=Author::find()->where('academic_status_id = :academic_status_id',[':academic_status_id'=>$this->id])->all();
            if(!empty($model))
            {
                Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей Author');
                return false;
            }else{
                return true;
            }    
        } else {
            return false;
        }
    }
}
