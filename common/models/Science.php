<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "science".
 *
 * @property integer $id
 * @property string $science_name
 *
 * @property Article[] $articles
 */
class Science extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'science';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['science_name'], 'required'],
            [['science_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'science_name' => 'Научное направление',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['science_id' => 'id']);
    }

    public function beforeDelete() 
    {
        if (parent::beforeDelete()) 
        {
            $model=Article::find()->where('science_id = :scienceId',[':scienceId'=>$this->id])->all();
            if(!empty($model))
            {
                Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей Article');
                return false;
            }else{
                return true;
            }    
            //return true;
        } else {
            return false;
        }
    }
}
