<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "cn_article".
 *
 * @property integer $id
 * @property string $title
 * @property string $annotation
 * @property integer $status
 * @property integer $cn_nomination_id
 * @property integer $contest_id
 *
 * @property CnNomination $cnNomination
 * @property Contest $contest
 * @property CnComment[] $cnComments
 */
class ContestArticle extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cn_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'annotation', 'cn_nomination_id', 'contest_id','user_id', 'author', 'organization'], 'required'],
            [['annotation'], 'string'],
            [['status', 'cn_nomination_id', 'contest_id','score'], 'integer'],
            [['title','coauthor'], 'string', 'max' => 255],
            [['organization', 'author'], 'string', 'min' => 3, 'max' => 255],

            [['author', 'organization'], 'match', 'pattern' => "/^[a-zA-Zа-яёА-ЯЁ\s\-\`\.]+$/u", 'message' => 'Разрешены только символы латиницы, кириллицы, пробел, знаки дефис и апостроф'],
            [['author'], 'filter', 'filter' => function ($value) {
                $newValue = '';
                $tmp = mb_split("\s", $value);
                foreach ($tmp as $key => $val) {
                    $newValue .= mb_strtoupper(mb_substr($val, 0, 1, 'UTF-8'), 'UTF-8');
                    $newValue .= mb_substr($val, 1, null, 'UTF-8');
                    $newValue .= " ";
                }
                return $newValue;
            }],
            [['organization','author'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название работы',
            'annotation' => 'Аннотация',
            'status' => 'Статус',
            'cn_nomination_id' => 'Номинация',
            'contest_id' => 'Конкурс',
            'user_id' => 'Создатель заявки',
            'author'=>'Автор',
            'organization' => 'Организация',
            'coauthor'=>'Соавторы',
            'score' => 'Занятое место',
            'master' => 'Руководитель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestNomination()
    {
        return $this->hasOne(ContestNomination::className(), ['id' => 'cn_nomination_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(Author::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContest()
    {
        return $this->hasOne(Contest::className(), ['id' => 'contest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestComments()
    {
        return $this->hasMany(ContestComment::className(), ['cn_article_id' => 'id']);
    }    

    public function getContestMasters()
    {
        return $this->hasMany(Master::className(), ['id' => 'master_id'])
            ->viaTable('cn_article_master', ['cn_article_id' => 'id']);
    }

    public function getStatus($model)
    {
        switch($model->status){
            case Yii::$app->params['draft']:
                return 'Черновик';
                break;
            case Yii::$app->params['review']:
                return 'На рассмотрении';
                break;
            case Yii::$app->params['returned']:
                return 'На доработку';
                break;
            case Yii::$app->params['accepted']:
                return 'Принята';
                break;      
        }
    }

    public function getNomination($nomination_id)
    {
        $nomination_name='';
         if (($model = ContestNomination::findOne($nomination_id)) !== null) {
             $nomination_name=$model->nomination_name;
        } 
        return $nomination_name;
    }

    public function getContestName($contest_id)
    {
        $contest_name='';
        if (($model = Contest::findOne($contest_id)) !== null) {
             $contest_name=$model->name;
        } 
        return $contest_name;
    }
        
    public function getAttachedFiles($model)
    {
        $file_path='';
        
        $contest_article = glob(Yii::getAlias('@uploads')."/".Yii::getAlias('@contestsPath')."/".$model->contest_id. "/" . $model->id .".*");
        
        if(!empty($contest_article))
        {
            $contest_article = $contest_article[0];
            $contest_article = substr($contest_article,2);
            $file_path.=Html::a('Файл', $contest_article,['target'=>'_blank']);
        }

        if($file_path==''){
            return Html::encode('Нет прикрепленых файлов');
        }else{
            return $file_path;
        }
    }

    public function getContestArticleAuthor($user_id)
    {
        return $username = User::findOne($user_id)->username;
    }

    public function getAuthor() {
        return Author::find()
            ->where('user_id = :user_id', [':user_id' => $this->user_id])
            ->one();
    }

    public function getMaster() 
    {
        $master = '';
        $master_array = Master::find()->where('cn_article_master.cn_article_id = :cn_article_id', 
            [':cn_article_id' => $this->id])->leftJoin('cn_article_master', '`cn_article_master`.`master_id` = `master`.`id`')->all();
        if(!empty($master_array)){
            foreach ($master_array as $key => $value) {
                $master .= $value->surname ." ". $value->name ." ". $value->lastname . " - " . $value->organization . "; \n";
            }
        }
        return $master;
    }

    /**
    * Delete all related records before contest article deleted
    */
    public function beforeDelete() 
    {
        if (parent::beforeDelete()) {
            ContestComment::deleteAll('cn_article_id = :cn_article_id', [':cn_article_id' => $this->id]);
            ContestArticleMaster::deleteAll('cn_article_id = :cn_article_id', [':cn_article_id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }
}
