<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $comment
 * @property string $created_at
 * @property integer $user_id
 * @property integer $article_id
 *
 * @property Article $article
 * @property User $user
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'created_at', 'user_id', 'article_id'], 'required'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['user_id', 'article_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => 'Комментарий',
            'created_at' => 'Дата создания комментария',
            'user_id' => 'Пользователь',
            'article_id' => 'Статья',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName($user_id)
    {
        return User::findOne($user_id)->username;
    }
}
