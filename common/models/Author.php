<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "author".
 *
 * @property integer $user_id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property string $birth_date
 * @property string $sex
 * @property string $science
 * @property string $organization
 * @property string $ofiice_position
 * @property string $address
 * @property string $phone_number
 * @property integer $academic_degree_id
 * @property integer $teaching_staff_id
 *
 * @property ArticleAuthor[] $articleAuthors
 * @property TeachingStaff $teachingStaff
 * @property AcademicDegree $academicDegree
 * @property User $user
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'author';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'surname', 'name', 'lastname', 'organization', 'office_position',
                'eng_surname', 'eng_name', 'eng_lastname'], 'required'],
            [['user_id', 'academic_degree_id', 'academic_status_id'], 'integer'],
            [['birth_date'], 'safe'],
            [['surname', 'name', 'lastname', 'eng_surname', 'eng_name', 'eng_lastname', 
                'sex', 'organization', 'office_position', 'address', 'phone_number'], 'string', 'max' => 255],
            [['surname', 'name', 'lastname', 'organization', 'office_position'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'lastname'], 'filter', 'filter' => function ($value) {
                // нормализация значения происходит тут
                $newValue = mb_strtoupper(mb_substr($value, 0, 1, 'UTF-8'), 'UTF-8');
                $newValue .= mb_substr($value, 1, null, 'UTF-8');
                return $newValue;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'birth_date' => 'Дата рождения',
            'sex' => 'Пол',
            'organization' => 'Учреждение',
            'office_position' => 'Должность',
            'address' => 'Адрес',
            'phone_number' => 'Телефон',
            'academic_degree_id' => 'Ученая степень',
            'academic_status_id' => 'Ученое звание',
            'eng_surname' => 'Фамилия на английском',
            'eng_name' => 'Имя на английском',
            'eng_lastname' => 'Отчество на английском',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleAuthors()
    {
        return $this->hasMany(ArticleAuthor::className(), ['author_user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicStatus()
    {
        return $this->hasOne(AcademicStatus::className(), ['id' => 'academic_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicDegree()
    {
        return $this->hasOne(AcademicDegree::className(), ['id' => 'academic_degree_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getAcademicStatusToString($id)
    {
        $academic_status='';

        if(($model=AcademicStatus::find()->where(['id'=>$id])->one())!==null) {
            $academic_status=$model->academic_status;
        }
            
        return $academic_status;
    }

    public function getAcademicDegreeToString($id) 
    {
        $academic_degree='';
        
        if(($model=AcademicDegree::find()->where(['id'=>$id])->one())!==null) {
            $academic_degree=$model->academic_degree;
        }

        return $academic_degree;
    }

    public static function getAuthorFIO($user_id) {
        $authorFIO = '';
        $author = Author::findOne($user_id);
        $authorFIO .= $author->surname . " ";
        $authorFIO .= mb_substr($author->name, 0, 1, 'UTF-8').".";
        $authorFIO .= mb_substr($author->lastname, 0, 1, 'UTF-8').".";
        return $authorFIO;
    }    

    public static function getAuthorOrganization($user_id) {
        $authorOrganization = '';
        $author = Author::findOne($user_id);
        $authorOrganization .= $author->organization;
        return $authorOrganization;
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            ArticleAuthor::deleteAll('author_user_id = :author_user_id', [':author_user_id' => $this->user_id]);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes) 
    {
        //если это новая запись, а не обновление
        if($insert) {
            $email = User::findOne($this->user_id)->email;
            $mailList = new MailList();
            $mailList->email = $email;
            $mailList->name = $this->name . " " . $this->lastname;
            $mailList->status = 1;
            $mailList->save(false);

            /*$allCategories = Category::find()->where('id != 1')->all();
            if(!empty($allCategories)){
                foreach ($allCategories as $value) {
                    (new MailListCategory(['mail_list_id' => $mailList->id, 'category_id' => $value->id]))->save();
                }
            }*/
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
