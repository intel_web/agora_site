<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "site_menu".
 *
 * @property integer $id
 * @property string $label
 * @property string $url
 * @property integer $page_id
 * @property integer $parent_id
 */
class SiteMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string'],
            [['page_id', 'parent_id'], 'integer'],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Название',
            'url' => 'Ссылка',
            'page_id' => 'Страница',
            'parent_id' => 'Родительский пункт',
        ];
    }

    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {
            if (!$this->page_id) $this->page_id = 0;
            if (!$this->parent_id) $this->parent_id = 0;
            return true;
        } else {
            return false;
        }
    }

    public function getPagesForMenuItem() {
        return Page::find()->all();
    }

    public function getAllMenuItems() {
        return SiteMenu::find()->all();
    }
}
