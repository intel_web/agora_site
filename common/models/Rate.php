<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "rate".
 *
 * @property integer $id
 * @property integer $minimum
 * @property integer $maximum
 * @property double $value
 */
class Rate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['minimum', 'maximum'], 'integer'],
            [['value'], 'required'],
            [['value'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'minimum' => 'Минимум',
            'maximum' => 'Максимум',
            'value' => 'Тариф',
        ];
    }
}
