<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contest".
 *
 * @property integer $id
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 * @property string $requirements
 *
 * @property CnArticle[] $cnArticles
 */
class Contest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_date', 'end_date'], 'required'],
            //[['start_date', 'end_date'], 'date', 'format' => 'yyyy-m-d'],
            [['requirements'], 'string'],
            [['name'], 'string', 'max' => 255],
            ['start_date', 'validateDateRange'],
        ];
    }

    //validators
    public function validateDateRange() {
        $this->start_date = preg_replace('/\s+/', ' ', trim($this->start_date));
        $this->end_date = preg_replace('/\s+/', ' ', trim($this->end_date));
        if (date_create($this->start_date) >= date_create($this->end_date)) {
            $this->addError('start_date', 'Недопустимый диапазон дат');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название конкурса',
            'start_date' => 'Дата начала',
            'end_date' => 'Дата конца',
            'requirements' => 'Требования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnArticles()
    {
        return $this->hasMany(ContestArticle::className(), ['contest_id' => 'id']);
    }

    /**
    * Delete all related records before contest deleted
    */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            $articles = ContestArticle::find()
                ->where('contest_id = :contest_id', [':contest_id' => $this->id])
                ->all();
            foreach ($articles as $article) {
                $article->delete();
            }
            return true;
        } else {
            return false;
        }
    }
}
