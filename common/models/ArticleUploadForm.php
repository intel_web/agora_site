<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ArticleUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $fileName;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, rtf, docx',  'maxSize' => 4*1024*1024],
        ];
    }

    public function upload($user_id, $article_id)
    {
        if(($this->validate())&&($this->file!=null)){
            $this->file->saveAs(Yii::getAlias('@uploads')."/".Yii::getAlias('@articlesPath')."/".$user_id."/".$article_id."/" . $this->fileName . '.' . $this->file->extension);
            return true;
        }else{
            return false;
        }
    }
}

?>