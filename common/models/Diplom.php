<?php

namespace common\models;

use yii\base\Model;
use Yii;
use common\models\ContestArticle;
use yii\data\ActiveDataProvider;

/**
 * SystemStatistic
 */
class Diplom extends Model {

	public $author;
	public $coauthor;
	public $contestMasters;
	public $organization;
	public $score;
	public $contestNomination;
	public $contestName;

	public function makeDiplom($cn_info) 
	{
		if(!empty($cn_info)) {
			$this->author = $cn_info['author'];
			if($cn_info['coauthor'] !== null) {
                //разбиваем строку соавторов из базы по дилиметру ;
                $couthorArr = explode(";", $cn_info['coauthor']);
                for ($i=0; $i < count($couthorArr)-1; $i++) {
                    $this->author .= ', ';
                    $this->author .= $couthorArr[$i];
                }
			}
			if(!empty($cn_info['contestMasters'])){
				for ($i=0; $i < count($cn_info['contestMasters']); $i++) { 
                    $this->contestMasters .= $cn_info['contestMasters'][$i]['office_position'] . ', ';
                    if($cn_info['contestMasters'][$i]['academicDegree'] !== null) {
                        $this->contestMasters .= $cn_info['contestMasters'][$i]['academicDegree']['academic_degree'];
                        $this->contestMasters .= ' ';
                    }
                    $this->contestMasters .= $cn_info['contestMasters'][$i]['surname'].' '. 
                        mb_substr($cn_info['contestMasters'][$i]['name'], 0, 1, 'UTF-8') .'.'.
                        mb_substr($cn_info['contestMasters'][$i]['lastname'], 0, 1, 'UTF-8').'.';
                    if($cn_info['contestMasters'][$i] !== $cn_info['contestMasters'][count($cn_info['contestMasters'])-1]){
                        $this->contestMasters .= ', ';
                    }  
                }
			}

			$this->organization = $cn_info['organization'];

			$this->score = $cn_info['score'];

			$this->contestNomination = $cn_info['contestNomination']['nomination_name'];
			$this->contestName = $cn_info['contest']['name'];
		}
	}
}