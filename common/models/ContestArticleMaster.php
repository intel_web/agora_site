<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cn_article_master".
 *
 * @property integer $id
 * @property integer $master_id
 * @property integer $cn_article_id
 *
 * @property CnArticle $cnArticle
 * @property Master $master
 */
class ContestArticleMaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cn_article_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['master_id', 'cn_article_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'master_id' => 'Руководитель',
            'cn_article_id' => 'Работа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestArticle()
    {
        return $this->hasOne(ContestArticle::className(), ['id' => 'cn_article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaster()
    {
        return $this->hasOne(Master::className(), ['id' => 'master_id']);
    }
}
