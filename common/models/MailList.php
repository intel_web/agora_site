<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mail_list".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property integer $status
 *
 * @property MailListCategory[] $mailListCategories
 */
class MailList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mail_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'status'], 'required'],
            [['status'], 'integer'],
            ['name', 'string', 'max' => 255],

            ['email', 'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\MailList', 'message' => 'E-mail уже занят.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Эл.Почта',
            'name' => 'Имя адресата',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailListCategories()
    {
        return $this->hasMany(MailListCategory::className(), ['mail_list_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            
            $mailCategory = MailListCategory::find()->where('mail_list_id = :mail_list_id', [':mail_list_id' => $this->id])->all();
            if(!empty($mailCategory)){
                foreach ($mailCategory as $v) {
                    $v->delete();
                }
            }

            $mailHistory = MailHistory::find()->where('email = :email', [':email' => $this->email])->all();
            if(!empty($mailHistory)){
                foreach ($mailHistory as $v) {
                    $v->delete();
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
