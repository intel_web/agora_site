<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mail_list_category".
 *
 * @property integer $id
 * @property integer $mail_list_id
 * @property integer $category_id
 *
 * @property Category $category
 * @property MailList $mailList
 */
class MailListCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mail_list_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail_list_id', 'category_id'], 'required'],
            [['mail_list_id', 'category_id'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['mail_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailList::className(), 'targetAttribute' => ['mail_list_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mail_list_id' => 'Эл.Почта',
            'category_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailList()
    {
        return $this->hasOne(MailList::className(), ['id' => 'mail_list_id']);
    }
}
