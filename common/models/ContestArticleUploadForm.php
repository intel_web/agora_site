<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ContestArticleUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $fileName;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'maxSize' => 4*1024*1024],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
            'fileName' => 'Название файла',
        ];
    }

    public function upload($contest_id)
    {
        if(($this->validate())&&($this->file!=null)){
            $this->file->saveAs(Yii::getAlias('@uploads') ."/". Yii::getAlias('@contestsPath') ."/". $contest_id ."/". $this->fileName . '.' . $this->file->extension);
            return true;
        }else{
            return false;
        }
    }
}

?>