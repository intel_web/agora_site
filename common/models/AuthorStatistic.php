<?php

namespace common\models;

use yii\base\Model;
use Yii;
use common\models\Article;
use yii\data\ActiveDataProvider;

/**
 * SystemStatistic
 */
class AuthorStatistic extends Model {
    public $articleDraft;
    public $articleReview;
    public $articleReturned;
    public $articleAccepted;
    public $articlePublished;
    public $articleCoAuthors;
    public $coAuthorRequest;
    public $probableArticles;
    public $articleTotal;

    public $contestDraft;
    public $contestReview;
    public $contestReturned;
    public $contestAccepted;
    public $contestTotal;

    public function attributeLabels() {
        return [
            'articleDraft' => 'Черновик',
            'articleReview' => 'На рецензии',
            'articleReturned' => 'На доработке', 
            'articleAccepted' => 'Принято к печати',
            'articlePublished' => 'Опубликовано',
            'articleCoAuthors' => 'Совместные работы',
            'coAuthorRequest' => 'Запросы на соавторство',
            'probableArticles' => 'Возможное соавторство',
            'articleTotal' => 'Всего',            

            'contestDraft' => 'Черновик',
            'contestReview' => 'На рецензии',
            'contestReturned' => 'На доработке', 
            'contestAccepted' => 'Принято',
            'contestTotal' => 'Всего',
        ];
    }

    public function getAuthorStatistic() 
    {
        $user_id = Yii::$app->user->identity->id;

        $curent_user_surname = Author::findOne(Yii::$app->user->identity->id)->surname;

        $this->articleDraft = Article::find()
            ->where(['author_id'=> $user_id])
            ->andWhere(['status' => Yii::$app->params['draft']])
            ->count();
        $this->articleReview = Article::find()
            ->where(['author_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['review']])
            ->count();
        $this->articleReturned = Article::find()
            ->where(['author_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['returned']])
            ->count();
        $this->articleAccepted = Article::find()
            ->where(['author_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['accepted']])
            ->count();
        $this->articlePublished = Article::find()
            ->where(['author_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['published']])
            ->count();
        $this->articleCoAuthors = ArticleAuthor::find()
            ->joinWith(['article'])
            ->where(['status' => Yii::$app->params['published']])
            ->andWhere('author_user_id = :current_user', 
                [':current_user' => $user_id])
            ->count();       
        $this->coAuthorRequest = CoauthorRequest::find()
            ->where(['article_author_id' => $user_id])
            ->count();        
        $this->probableArticles = Article::find()
            ->where('coauthor LIKE :curent_user_surname AND status = :status 
                AND article.id NOT IN (SELECT coauthor_request.article_id from coauthor_request where coauthor_request.requested_user_id=:current_user) 
                AND article.id NOT IN (SELECT article_author.article_id from article_author where article_author.author_user_id=:current_user)', 
                [':curent_user_surname' => '%'.$curent_user_surname.'%', 
                ':status' => Yii::$app->params['published'],
                ':current_user'=>Yii::$app->user->identity->id])
            ->count();
        $this->articleTotal = Article::find()
            ->where(['author_id' => $user_id])
            ->count() + $this->articleCoAuthors;



        $this->contestDraft = ContestArticle::find()
            ->where(['user_id'=> $user_id])
            ->andWhere(['status' => Yii::$app->params['draft']])
            ->count();
        $this->contestReview = ContestArticle::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['review']])
            ->count();
        $this->contestReturned = ContestArticle::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['returned']])
            ->count();
        $this->contestAccepted = ContestArticle::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['status' => Yii::$app->params['accepted']])
            ->count();

        $this->contestTotal = ContestArticle::find()
            ->where(['user_id' => $user_id])
            ->count();
    }
}
