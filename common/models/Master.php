<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property string $sex
 * @property string $organization
 * @property string $office_position
 * @property integer $academic_degree
 *
 * @property CnArticleMaster[] $cnArticleMasters
 * @property AcademicDegree $academicDegree
 */ 
class Master extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'name', 'lastname', 'sex', 'organization', 'office_position'], 'required'],
            [['surname', 'name', 'lastname', 'sex', 'organization', 'office_position'], 'string', 'max' => 255],
            [['surname', 'name', 'lastname'], 'match', 'pattern' => "/^[a-zA-Zа-яёА-ЯЁ\-\`]+$/u", 
                'message' => 'Разрешены только символы латиницы и кириллицы, знаки дефис и апостроф'],            
            [['academic_degree_id'], 'integer'],
            [['surname', 'name', 'lastname', 'organization', 'office_position'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'lastname'], 'filter', 'filter' => function ($value) {
                $newValue = mb_strtoupper(mb_substr($value, 0, 1, 'UTF-8'), 'UTF-8');
                $newValue .= mb_substr($value, 1, null, 'UTF-8');
                return $newValue;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'sex' => 'Пол',
            'organization' => 'Организация',
            'office_position' => 'Должность',
            'academic_degree_id' => 'Ученая степень',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestArticleMasters()
    {
        return $this->hasMany(ContestArticleMaster::className(), ['master_id' => 'id']);
    }

   public function getAcademicDegree()
   {
       return $this->hasOne(AcademicDegree::className(), ['id' => 'academic_degree_id']);
   }
}
