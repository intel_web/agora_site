<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "editor".
 *
 * @property integer $user_id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 *
 * @property User $user
 */
class Editor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'editor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'surname', 'name', 'lastname'], 'required'],
            [['user_id'], 'integer'],
            [['surname', 'name', 'lastname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
